const redis = require('redis');
require('dotenv').config();
const logger = require('../config/logger');
const errorLogger = logger.errorLogger;

var commonDb = redis.createClient({
    url: process.env.REDIS_URL + '/0'
});
commonDb.on('connect', function () {
	console.log('Redis commonDb connected');
});
commonDb.on('error', function (err) {
    errorLogger.info('Redis commonDb error: ' + err);
});

module.exports = {
    commonDb
}