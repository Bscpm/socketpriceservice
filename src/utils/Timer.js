var hour = 0, minute = 0, second = 0;
var timerInterval = undefined;

function setTime(timeString) {
    hour = parseInt(timeString.substring(0, timeString.length - 4));
    minute = parseInt(timeString.substring(timeString.length - 4, timeString.length - 2));
    second = parseInt(timeString.substring(timeString.length - 2, timeString.length));

    timerInterval = setInterval(() => {
        second++;
        if (second === 60) {
            minute++;
            if (minute === 60) {
                hour++;
                if (hour === 24)
                    hour = 0;
                minute = 0;
            }
            second = 0;
        }
    }, 980);
}

function getH(){
    return hour;
}

function getM(){
    return minute;
}

function getS(){
    return second;
}

function getTime() {
    if(hour === 0 && minute === 0 && second === 0) return '';
    var tmpH = hour < 10 ? ('0' + hour) : hour,
        tmpM = minute < 10 ? ('0' + minute) : minute,
        tmpS = second < 10 ? ('0' + second) : second;
    return tmpH + ':' + tmpM + ':' + tmpS;
}

function stopTimer() {
    if (timerInterval !== undefined)
        clearInterval(timerInterval);
}

module.exports = {
    setTime,
    getH,
    getM,
    getS,
    getTime,
    stopTimer
}