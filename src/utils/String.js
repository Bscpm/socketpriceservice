const dotenv = require('dotenv');
dotenv.config();

function isEmpty(str) {
    return (!str || 0 === str.trim().length);
}
function curDate2String(separator = '-'){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
  
    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    return (dd + separator + mm + separator + yyyy);
}

// date1 < date2 return true
function compareDateString(date1, date2, separator = '-'){
    var tmp1 = date1.split(separator);
    var tmp2 = date2.split(separator);
    if(Number(tmp1[2]) < Number(tmp2[2])) return true;
    else if(Number(tmp1[2]) == Number(tmp2[2])){
        if(Number(tmp1[1]) < Number(tmp2[1])) return true;
        else if(Number(tmp1[1]) == Number(tmp2[1])){
            if(Number(tmp1[0]) < Number(tmp2[0])) return true;
        }
    }
    return false;
}

function getDateByDateTime(date, separator = '-') {
    var dd = date.getDate();
    var mm = date.getMonth() + 1; //January is 0!
  
    var yyyy = date.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    return (dd + separator + mm + separator + yyyy);
}

function getTimeByDateTime(date) {
    var h = date.getHours();
    var m = date.getMinutes();
    var s = date.getSeconds();
    if(h < 10) h = '0' + h
    if(m < 10) m = '0' + m
    if(s < 10) s = '0' + s
    return (h+':'+m+':'+s)
}

function convertDate(dateString, format = null, separator = '-'){
  if(dateString.trim() === '') return '';
  switch(format){
    case 'yyyyMMdd':
      return dateString.substring(6, 8) + separator + dateString.substring(4, 6) + separator + dateString.substring(0, 4);
    default:
      return dateString.substring(0, 2) + separator + dateString.substring(2, 4) + separator + dateString.substring(4, 8);
  }
}

function validateOriginString(origin){
  if(process.env.WS_ORIGIN.split(';')[0] === "any"){
    return true;
  }
  return process.env.WS_ORIGIN.split(';').includes(origin);
}

module.exports = {
    isEmpty,
    curDate2String,
    compareDateString,
    getDateByDateTime,
    getTimeByDateTime,
    convertDate,
    validateOriginString
}