const redis = require('../database/redis');
const logger = require('../config/logger');
const errorLogger = logger.errorLogger;
const commonLogger = logger.commonLogger;
const dotenv = require('dotenv');
dotenv.config();
const bondCalculator = require('bond-calculator');
const bond_5Y = bondCalculator({
  settlement: process.env.SETTLEMENT || '1900-01-01',
  maturity: process.env.MATURITY_5Y || '1905-01-01',
  rate: process.env.RATE || 0.05, // 5%
  redemption: process.env.REDEMPTION || 100,
  frequency: process.env.FREQUENCY || 1, // 1=Annual, 2=Semiannual, 4=Quarterly
  convention: process.env.BASIS || 'ACTUAL/ACTUAL',
});
const bond_10Y = bondCalculator({
  settlement: process.env.SETTLEMENT || '1900-01-01',
  maturity: process.env.MATURITY_10Y || '1910-01-01',
  rate: process.env.RATE || 0.05, // 5%
  redemption: process.env.REDEMPTION || 100,
  frequency: process.env.FREQUENCY || 1, // 1=Annual, 2=Semiannual, 4=Quarterly
  convention: process.env.BASIS || 'ACTUAL/ACTUAL',
});

function bondYield(underlying, price) {
  if(underlying === "VGB5"){
    return (bond_5Y.yield(price / 1000) * 100).toFixed(4);
  } else if(underlying === "VGB10") {
    return (bond_10Y.yield(price / 1000) * 100).toFixed(4);
  } else {
    return 0;
  }
}

var lastPriceStock = { basis: {}, future: {} };
var firstLoad = true;

function writeLastPriceStock(type, symbol, price) {
  if (redis.client.connected) {
    if (firstLoad) {
      firstLoad = false;
      redis.client.get("lastPriceStock", function (err, reply) {
        if (err) {
          errorLogger.info('[Redis - LastPriceStock] ' + ex.message);
          return;
        }
        else if (reply !== null) {
          lastPriceStock = JSON.parse(reply);
        }
      });
    } else {
      lastPriceStock[type][symbol] = { symbol: symbol, type: type, price: price };
      redis.client.set('lastPriceStock', JSON.stringify(lastPriceStock));
    }
  } else {
    errorLogger.info('[Redis - LastPriceStock] disconnect');
  }
}

module.exports = {
  bondYield,
  writeLastPriceStock
}