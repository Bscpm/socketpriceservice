const logger = require('../config/logger');
const errorLogger = logger.errorLogger;
const commonLogger = logger.commonLogger;
const redis = require('../database/redis');
const stringHelper = require('../utils/String');
const rp = require('request-promise').defaults({ json: true });
var JsonOptimize = require('json-optimize'), jo = new JsonOptimize();

var lastDataHose = {}, lastDataHnx = {}, matchHistData = {};
var isNeedResetData = false; // Init day
var tradingSessionHOSE = '';
var lastPriceStock = {}, lastDataHoseIntraday = [];

function resetSIDataChange(item) {
    item.total_vol.change = 0;
    item.total_value.change = 0;
    item.match_price.change = 0;
    item.match_qtty.change = 0;
    item.low_price.change = 0;
    item.high_price.change = 0;
    item.nn_buy.change = 0;
    item.nn_sell.change = 0;
    item.nn_room.change = 0;
}

function resetTPDataChange(item) {
    item.buy_1.change = 0;
    item.buy_2.change = 0;
    item.buy_3.change = 0;
    item.buy_qtty_1.change = 0;
    item.buy_qtty_2.change = 0;
    item.buy_qtty_3.change = 0;
    item.sell_1.change = 0;
    item.sell_2.change = 0;
    item.sell_3.change = 0;
    item.sell_qtty_1.change = 0;
    item.sell_qtty_2.change = 0;
    item.sell_qtty_3.change = 0;
}

function resetDataChange() {
    Object.values(lastDataHose).forEach(function (item) {
        resetTPDataChange(item);
        resetSIDataChange(item);
        item.hasChange = 1;
        lastDataHose[item.id] = item;
        if (!stringHelper.isEmpty(item.last_trading_date) && stringHelper.compareDateString(item.last_trading_date, stringHelper.curDate2String('/'), '/')) {
            delete lastDataHose[item.id];
        }
    });
    Object.values(lastDataHnx).forEach(function (item) {
        resetTPDataChange(item);
        resetSIDataChange(item);
        item.hasChange = 1;
        lastDataHnx[item.id] = item;
    });
}

function resetData() {
    matchHistData = {};
    Object.values(lastDataHose).forEach(function (item) {
        if (!stringHelper.isEmpty(item.last_trading_date) && stringHelper.compareDateString(item.last_trading_date, stringHelper.curDate2String('/'), '/')) {
            delete lastDataHose[item.id];
        } else {
            resetTPData(item);
            resetSIData(item);
            lastDataHose[item.id] = item;
        }
    });
    Object.values(lastDataHnx).forEach(function (item) {
        resetTPData(item);
        resetSIData(item);
        lastDataHnx[item.id] = item;
    });
}

function resetSIData(item) {
    item.match_price.val = '';
    item.match_qtty.val = '';
    item.match_qtty.intVal = 0;
    item.difference = '';
    item.total_vol.val = '';
    item.total_vol.intVal = 0;
    item.total_value.val = '';
    item.total_value.intVal = 0;
    item.open_price = '';
    item.high_price.val = '';
    item.low_price.val = '';
    item.nn_buy.val = '';
    item.nn_sell.val = '';
    if (item.stock_type === "W") {
        item.underlying.price = { val: '', change: 0, time: 0 };
        item.underlying.color = 0;
        item.underlying.buy_1 = 0;
        item.underlying.buy_2 = 0;
        item.underlying.buy_3 = 0;
        item.underlying.sell_1 = 0;
        item.underlying.sell_2 = 0;
        item.underlying.sell_3 = 0;
    }
}

function resetTPData(item) {
    item.buy_3.val = ''; item.buy_2.val = ''; item.buy_1.val = '';
    item.buy_qtty_3.val = ''; item.buy_qtty_2.val = ''; item.buy_qtty_1.val = '';
    item.buy_qtty_3.intVal = 0; item.buy_qtty_2.intVal = 0; item.buy_qtty_1.intVal = 0;
    item.sell_1.val = ''; item.sell_2.val = ''; item.sell_3.val = '';
    item.sell_qtty_1.val = ''; item.sell_qtty_2.val = ''; item.sell_qtty_3.val = '';
    item.sell_qtty_1.intVal = 0; item.sell_qtty_2.intVal = 0; item.sell_qtty_3.intVal = 0;
}

function loadHistoryData(historyDataHose, historyDataHnx, historyLastPriceStock) {
    lastDataHose = historyDataHose;
    lastDataHnx = historyDataHnx;
    lastPriceStock = historyLastPriceStock;
}

function loadMatchHistData(_matchHistData) {
    matchHistData = _matchHistData;
}

function saveLastPriceStock() {
    if (process.env.WRITE_LAST_PRICE_STOCK === 'Y') {
        redis.commonDb.set('StockBoard:LastPriceStock', JSON.stringify(lastPriceStock));
        if (lastDataHoseIntraday.length) {
            redis.commonDb.set('StockBoard:lastDataHoseIntraday', JSON.stringify(lastDataHoseIntraday));
        }
    }
}

function pushMakerStockChangeEvent(data) {
    if (process.env.MARKET_MAKER_CW === 'Y') {
        if (data === null) data = Object.values(lastDataHose).filter(el => el.stock_type === 'W'
            && el.issuer_name === process.env.MARKET_MAKER_ISSUER_NAME);
        var postData = [];
        data.forEach(cw => {
            if (cw.issuer_name === process.env.MARKET_MAKER_ISSUER_NAME) {
                var tmpMakerStock = {
                    symbol: cw.symbol,
                    tradingSession: cw.trading_session,
                    bid1: {
                        price: cw.buy_1.val === 'ATO' || cw.buy_1.val === 'ATC'
                            ? cw.buy_1.val : cw.buy_1.val * 1000, qty: cw.buy_qtty_1.intVal, qtyChange: false
                    },
                    bid2: { price: cw.buy_2.val * 1000, qty: cw.buy_qtty_2.intVal, qtyChange: false },
                    bid3: { price: cw.buy_3.val * 1000, qty: cw.buy_qtty_3.intVal, qtyChange: false },
                    ask1: {
                        price: cw.sell_1.val === 'ATO' || cw.sell_1.val === 'ATC'
                            ? cw.sell_1.val : cw.sell_1.val * 1000, qty: cw.sell_qtty_1.intVal, qtyChange: false
                    },
                    ask2: { price: cw.sell_2.val * 1000, qty: cw.sell_qtty_2.intVal, qtyChange: false },
                    ask3: { price: cw.sell_3.val * 1000, qty: cw.sell_qtty_3.intVal, qtyChange: false }
                };
                postData.push(tmpMakerStock);
            }
        });

        if (postData.length) {
            process.env.MARKET_MAKER_SERVER.split(";").forEach(host => {
                rp({
                    method: 'POST',
                    url: host + '/marketmaker/OnChangeMakerStock',
                    form: {
                        data: postData
                    }
                }).catch(err => {
                    errorLogger.info('[pushMakerStockChangeEvent] ' + err.message);
                });
            });
        }
    }
}

function initDataForOnline(io, clientId) {
    resetDataChange();
    io.to(`${clientId}`).emit('initialStocksForOnline', jo.pack({ stockTableId: 'basis', data: Object.values(Object.assign({}, lastDataHose, lastDataHnx)) }));
}

function hoseOnChangeHandler(io, string) {
    // console.log(string);
    if (!string.includes("HeartBeat")) {
        var jsonArr = JSON.parse(string);
        var tmpDataHOSE = [], tmpVariable = 0;
        var last10MatchHistHOSE = {};
        for (var jsonObj of jsonArr) {
            if (jsonObj['MsgType'] === 'TS') {
                return;
            }
            // Init day Msg
            if (jsonObj['MsgType'] === 'INIT_DAY') {
                isNeedResetData = true;
                return;
            }
            // System Control Msg
            if (jsonObj['MsgType'] === 'SC') {
                tradingSessionHOSE = jsonObj['system_control_code'];
                // console.log(tradingSessionHOSE);
                commonLogger.info("[HOSE Session] " + tradingSessionHOSE);
                if (jsonObj['system_control_code'] === 'P')
                    tradingSessionHOSE = 'ATO';
                else if (jsonObj['system_control_code'] === 'A')
                    tradingSessionHOSE = 'ATC';
                tmpDataHOSE = Object.values(lastDataHose).map(itm => {
                    itm.trading_session = tradingSessionHOSE;
                    lastDataHose[itm.id] = itm;
                    return itm;
                });
            }
            var stock = lastDataHose[jsonObj['security_number']];
            if (stock !== undefined) {
                stock.hasChange = 0;
                // stock.trading_session = tradingSessionHOSE;
                // Security Status Change Msg
                if (jsonObj['MsgType'] === 'SS') {
                    // if(stock['symbol'] === 'CDPM2002'){
                    //     console.log(JSON.stringify(jsonObj));
                    // }
                    stock.ceiling = jsonObj['ceiling'] / 1000;
                    stock.floor = jsonObj['floor_price'] / 1000;
                    stock.reference = jsonObj['prior_close_price'] / 1000;
                    // if (stock.reference === 0) stock.reference = ((stock.ceiling + stock.floor) / 2).toFixed(2);
                    stock.maturity_date = stringHelper.convertDate(jsonObj['maturity_date'], 'yyyyMMdd', '/');
                    stock.last_trading_date = stringHelper.convertDate(jsonObj['last_trading_date'], 'yyyyMMdd', '/');
                    if (stock.stock_type === 'W') {
                        stock.issuer_name = jsonObj['issuer_name'];
                        if (jsonObj['exercise_price'] > 0) {
                            stock.exercise_price = (jsonObj['exercise_price'] / 10000).toFixed(2);
                        }
                        stock.exercise_ratio = jsonObj['exercise_ratio'];
                        stock.underlying.symbol = jsonObj['underlying_symbol'];
                        var tmpStock = Object.values(lastDataHose).find(el => el.symbol === jsonObj['underlying_symbol']);
                        if (tmpStock !== undefined) {
                            stock.underlying.id = tmpStock.id;
                            stock.underlying.buy_1 = lastDataHose[tmpStock.id].buy_1.val;
                            stock.underlying.buy_2 = lastDataHose[tmpStock.id].buy_2.val;
                            stock.underlying.buy_3 = lastDataHose[tmpStock.id].buy_3.val;
                            stock.underlying.sell_1 = lastDataHose[tmpStock.id].sell_1.val;
                            stock.underlying.sell_2 = lastDataHose[tmpStock.id].sell_2.val;
                            stock.underlying.sell_3 = lastDataHose[tmpStock.id].sell_3.val;
                            stock.underlying.price = lastDataHose[tmpStock.id].match_price;
                            if (!lastDataHose[tmpStock.id].covered_warrant_id.includes(jsonObj['security_number'])) {
                                lastDataHose[tmpStock.id].covered_warrant_id.push(jsonObj['security_number']);
                            }
                        }
                    }
                    stock.stock_type = jsonObj['security_type'];
                }
                // Projected Open Msg
                else if (jsonObj['MsgType'] === 'PO') {
                    stock.match_qtty.val = '';
                    if (jsonObj['projected_open_price'] !== undefined) {
                        tmpVariable = jsonObj['projected_open_price'] / 1000;
                        if (tmpVariable === stock.match_price.val)
                            stock.match_price.change = 0;
                        else {
                            stock.match_price.change = 1;
                            stock.hasChange = 1;
                            stock.match_price.time = stock.match_price.time === 1 ? 2 : 1;
                            // stockHelper.writeLastPriceStock('basis', stock.symbol, tmpVariable);
                        }
                        stock.match_price.val = tmpVariable;
                        if (jsonObj['projected_open_price'] > 0) {
                            lastPriceStock[stock.symbol] = {
                                symbol: stock.symbol,
                                floor: stock.market.toUpperCase(),
                                stocktype: stock.stock_type,
                                basicPrice: Math.round(stock.reference * 1000),
                                price: jsonObj['projected_open_price']
                            };
                        }
                        stock.covered_warrant_id.forEach(function (itm) {
                            if (lastDataHose[itm] !== undefined) {
                                lastDataHose[itm].underlying.price = stock.match_price;
                                lastDataHose[itm].underlying.color = -1;
                                if (stock.reference === stock.match_price.val)
                                    lastDataHose[itm].underlying.color = 0;
                                else if (stock.ceiling === stock.match_price.val)
                                    lastDataHose[itm].underlying.color = 2;
                                else if (stock.floor === stock.match_price.val)
                                    lastDataHose[itm].underlying.color = -2;
                                else if (stock.reference < stock.match_price.val)
                                    lastDataHose[itm].underlying.color = 1;
                                if (stock.match_price.change) {
                                    lastDataHose[itm].underlying.price.change = 1;
                                    lastDataHose[itm].underlying.price.time
                                        = lastDataHose[itm].underlying.price.time === 1 ? 2 : 1;
                                    lastDataHose[itm].hasChange = 1;
                                    tmpDataHOSE.push(lastDataHose[itm]);
                                }
                            }
                        });
                    }
                    if (stock.match_price.val === 0 || stock.match_price.val === stock.reference) {
                        stock.difference = '';
                    } else {
                        stock.difference = (stock.match_price.val - stock.reference).toFixed(2);
                    }
                }
                // Market Open Last Sale Msg
                else if (jsonObj['MsgType'] === 'OS' && stock.open_price === '' && jsonObj['price'] > 0) {
                    stock.open_price = jsonObj['price'] / 1000;
                    stock.high_price.val = stock.open_price;
                    stock.low_price.val = stock.open_price;
                }
                // Foreign Room Msg 
                else if (jsonObj['MsgType'] === 'TR') {
                    if (jsonObj['buy_volume'] !== undefined) {
                        if (jsonObj['buy_volume'] % 10 > 4) jsonObj['buy_volume'] += 10;
                        // tmpVariable = jsonObj['buy_volume'].toLocaleString('en')
                        //     .substring(0, jsonObj['buy_volume'].toLocaleString('en').length - 1);
                        // tmpVariable = (jsonObj['buy_volume'] / 10).toLocaleString('en');
                        tmpVariable = (Number((jsonObj['buy_volume'] / 1000).toFixed(1))).toLocaleString('en');
                        if (tmpVariable === stock.nn_buy.val)
                            stock.nn_buy.change = 0;
                        else {
                            stock.nn_buy.change = 1;
                            stock.hasChange = 1;
                            stock.nn_buy.time = stock.nn_buy.time === 1 ? 2 : 1;
                        }
                        stock.nn_buy.val = tmpVariable;
                    }
                    if (jsonObj['sell_volume'] !== undefined) {
                        if (jsonObj['sell_volume'] % 10 > 4) jsonObj['sell_volume'] += 10;
                        // tmpVariable = jsonObj['sell_volume'].toLocaleString('en')
                        //     .substring(0, jsonObj['sell_volume'].toLocaleString('en').length - 1);
                        // tmpVariable = (jsonObj['sell_volume'] / 10).toLocaleString('en');
                        tmpVariable = (Number((jsonObj['sell_volume'] / 1000).toFixed(1))).toLocaleString('en');
                        if (tmpVariable === stock.nn_sell.val)
                            stock.nn_sell.change = 0;
                        else {
                            stock.nn_sell.change = 1;
                            stock.hasChange = 1;
                            stock.nn_sell.time = stock.nn_sell.time === 1 ? 2 : 1;
                        }
                        stock.nn_sell.val = tmpVariable;
                    }
                    if (jsonObj['current_room'] !== undefined) {
                        if (jsonObj['current_room'] % 10 > 4) jsonObj['current_room'] += 10;
                        // tmpVariable = jsonObj['current_room'].toLocaleString('en')
                        //     .substring(0, jsonObj['current_room'].toLocaleString('en').length - 1);
                        // tmpVariable = Math.round(jsonObj['current_room'] / 10).toLocaleString('en');
                        tmpVariable = (Number((jsonObj['current_room'] / 1000).toFixed(1))).toLocaleString('en');
                        if (tmpVariable === stock.nn_room.val)
                            stock.nn_room.change = 0;
                        else {
                            stock.nn_room.change = 1;
                            stock.hasChange = 1;
                            stock.nn_room.time = stock.nn_room.time === 1 ? 2 : 1;
                        }
                        stock.nn_room.val = tmpVariable;
                    }
                }
                // Last Sale Msg
                else if (jsonObj['MsgType'] === 'LS') {
                    if (jsonObj['price'] !== undefined && jsonObj['price'] > 0) {
                        tmpVariable = jsonObj['price'] / 1000;
                        if (tmpVariable === stock.match_price.val)
                            stock.match_price.change = 0;
                        else {
                            stock.match_price.change = 1;
                            stock.hasChange = 1;
                            stock.match_price.time = stock.match_price.time === 1 ? 2 : 1;
                            // stockHelper.writeLastPriceStock('basis', stock.symbol, tmpVariable);
                        }
                        stock.match_price.val = tmpVariable;
                        lastPriceStock[stock.symbol] = {
                            symbol: stock.symbol,
                            floor: stock.market.toUpperCase(),
                            stocktype: stock.stock_type,
                            basicPrice: Math.round(stock.reference * 1000),
                            price: jsonObj['price']
                        };
                        stock.covered_warrant_id.forEach(function (itm) {
                            if (lastDataHose[itm] !== undefined) {
                                lastDataHose[itm].underlying.price = stock.match_price;
                                lastDataHose[itm].underlying.color = -1;
                                if (stock.reference === stock.match_price.val)
                                    lastDataHose[itm].underlying.color = 0;
                                else if (stock.ceiling === stock.match_price.val)
                                    lastDataHose[itm].underlying.color = 2;
                                else if (stock.floor === stock.match_price.val)
                                    lastDataHose[itm].underlying.color = -2;
                                else if (stock.reference < stock.match_price.val)
                                    lastDataHose[itm].underlying.color = 1;
                                if (stock.match_price.change) {
                                    lastDataHose[itm].underlying.price.change = 1;
                                    lastDataHose[itm].underlying.price.time
                                        = lastDataHose[itm].underlying.price.time === 1 ? 2 : 1;
                                    lastDataHose[itm].hasChange = 1;
                                    tmpDataHOSE.push(lastDataHose[itm]);
                                }
                            }
                        });
                        var tmpHighPrice = jsonObj['high_price'] / 1000;
                        var tmpLowPrice = jsonObj['low_price'] / 1000;
                        if (tmpHighPrice > stock.high_price.val) {
                            stock.high_price.val = tmpHighPrice;
                            stock.high_price.change = 1;
                            stock.hasChange = 1;
                            stock.high_price.time = stock.high_price.time === 1 ? 2 : 1;
                        }
                        else if (tmpLowPrice < stock.low_price.val) {
                            stock.low_price.val = tmpLowPrice;
                            stock.low_price.change = 1;
                            stock.hasChange = 1;
                            stock.low_price.time = stock.low_price.time === 1 ? 2 : 1;
                        }
                    } else {
                        stock.match_price.val = '';
                    }
                    if (jsonObj['lot_volume'] !== undefined && jsonObj['lot_volume'] > 0) {
                        // tmpVariable = jsonObj['lot_volume'].toLocaleString('en')
                        //     .substring(0, jsonObj['lot_volume'].toLocaleString('en').length - 1);
                        if (jsonObj['lot_volume'] === stock.match_qtty.intVal)
                            stock.match_qtty.change = 0;
                        else {
                            stock.match_qtty.change = 1;
                            stock.hasChange = 1;
                            stock.match_qtty.time = stock.match_qtty.time === 1 ? 2 : 1;
                        }
                        stock.match_qtty.val = (Number((jsonObj['lot_volume'] / 1000).toFixed(1))).toLocaleString('en');
                        stock.match_qtty.intVal = jsonObj['lot_volume'];
                    } else {
                        stock.match_qtty.val = '';
                        stock.match_qtty.intVal = 0;
                    }
                    if (stock.match_price.val === 0 || stock.match_price.val === stock.reference) {
                        stock.difference = '';
                    } else {
                        stock.difference = (stock.match_price.val - stock.reference).toFixed(2);
                    }

                    if (jsonObj['match_volume'] !== undefined && jsonObj['match_volume'] > 0) {
                        stock.total_vol.change = 1;
                        stock.hasChange = 1;
                        stock.total_vol.time = stock.total_vol.time === 1 ? 2 : 1;
                        stock.total_vol.intVal += jsonObj['match_volume'];
                        // stock.total_vol.val = stock.total_vol.intVal.toLocaleString('en')
                        //     .substring(0, stock.total_vol.intVal.toLocaleString('en').length - 1);
                        // stock.total_vol.val = (stock.total_vol.intVal / 10).toLocaleString('en');
                        stock.total_vol.val = (Number((stock.total_vol.intVal / 1000).toFixed(1))).toLocaleString('en');
                        if (matchHistData[stock.symbol] === undefined) {
                            matchHistData[stock.symbol] = [{
                                time: jsonObj['time'],
                                price: stock.match_price.val,
                                volume: jsonObj['match_volume'],
                                change_value: Number((stock.match_price.val - stock.reference).toFixed(2)),
                                total_vol: stock.total_vol.intVal
                            }];
                            last10MatchHistHOSE[stock.symbol] = matchHistData[stock.symbol];
                        } else {
                            matchHistData[stock.symbol].push({
                                time: jsonObj['time'],
                                price: stock.match_price.val,
                                volume: jsonObj['match_volume'],
                                change_value: Number((stock.match_price.val - stock.reference).toFixed(2)),
                                total_vol: stock.total_vol.intVal
                            });
                            last10MatchHistHOSE[stock.symbol] = matchHistData[stock.symbol].slice(matchHistData[stock.symbol].length - 10);
                        }
                    }
                    if (jsonObj['match_value'] !== undefined && jsonObj['match_value'] > 0) {
                        stock.total_value.change = 1;
                        stock.hasChange = 1;
                        stock.total_value.time = stock.total_value.time === 1 ? 2 : 1;
                        stock.total_value.intVal += jsonObj['match_value'];
                        stock.total_value.val = Math.round(stock.total_value.intVal / 1000000).toLocaleString('en');
                    }
                }
                // Top Prices Msg
                else if (jsonObj['MsgType'] === 'TP') {
                    if (jsonObj['side'] === 'B') {
                        if (jsonObj['price_3_best'] !== undefined && jsonObj['price_3_best'] > 0) {
                            tmpVariable = jsonObj['price_3_best'] / 1000;
                            stock.buy_3.change = -1;
                            if (tmpVariable === stock.buy_3.val)
                                stock.buy_3.change = 0;
                            else if (tmpVariable > stock.buy_3.val)
                                stock.buy_3.change = 1;
                            stock.buy_3.val = tmpVariable;
                            if (stock.buy_3.change !== 0) {
                                stock.hasChange = 1;
                                stock.buy_3.time = stock.buy_3.time === 1 ? 2 : 1;
                                stock.covered_warrant_id.forEach(function (itm) {
                                    if (lastDataHose[itm] !== undefined) {
                                        lastDataHose[itm].underlying.buy_3 = stock.buy_3.val;
                                        lastDataHose[itm].hasChange = 1;
                                        tmpDataHOSE.push(lastDataHose[itm]);
                                    }
                                });
                            }
                        } else {
                            stock.buy_3.val = '';
                        }
                        if (jsonObj['lot_volume_3'] !== undefined && jsonObj['lot_volume_3'] > 0) {
                            // tmpVariable = jsonObj['lot_volume_3'].toLocaleString('en')
                            //     .substring(0, jsonObj['lot_volume_3'].toLocaleString('en').length - 1);
                            stock.buy_qtty_3.change = -1;
                            if (jsonObj['lot_volume_3'] === stock.buy_qtty_3.intVal)
                                stock.buy_qtty_3.change = 0;
                            else if (jsonObj['lot_volume_3'] > stock.buy_qtty_3.intVal)
                                stock.buy_qtty_3.change = 1;
                            // stock.buy_qtty_3.val = (jsonObj['lot_volume_3'] / 10).toLocaleString('en');
                            stock.buy_qtty_3.val = (Number((jsonObj['lot_volume_3'] / 1000).toFixed(1))).toLocaleString('en');
                            stock.buy_qtty_3.intVal = jsonObj['lot_volume_3'];
                            if (stock.buy_qtty_3.change !== 0) {
                                stock.hasChange = 1;
                                stock.buy_qtty_3.time = stock.buy_qtty_3.time === 1 ? 2 : 1;
                            }
                        } else {
                            stock.buy_qtty_3.val = '';
                            stock.buy_qtty_3.intVal = 0;
                        }
                        if (jsonObj['price_2_best'] !== undefined && jsonObj['price_2_best'] > 0) {
                            tmpVariable = jsonObj['price_2_best'] / 1000;
                            stock.buy_2.change = -1;
                            if (tmpVariable === stock.buy_2.val)
                                stock.buy_2.change = 0;
                            else if (tmpVariable > stock.buy_2.val)
                                stock.buy_2.change = 1;
                            stock.buy_2.val = tmpVariable;
                            if (stock.buy_2.change !== 0) {
                                stock.hasChange = 1;
                                stock.buy_2.time = stock.buy_2.time === 1 ? 2 : 1;
                                stock.covered_warrant_id.forEach(function (itm) {
                                    if (lastDataHose[itm] !== undefined) {
                                        lastDataHose[itm].underlying.buy_2 = stock.buy_2.val;
                                        lastDataHose[itm].hasChange = 1;
                                        tmpDataHOSE.push(lastDataHose[itm]);
                                    }
                                });
                            }
                        } else {
                            stock.buy_2.val = '';
                        }
                        if (jsonObj['lot_volume_2'] !== undefined && jsonObj['lot_volume_2'] > 0) {
                            // tmpVariable = jsonObj['lot_volume_2'].toLocaleString('en')
                            //     .substring(0, jsonObj['lot_volume_2'].toLocaleString('en').length - 1);
                            stock.buy_qtty_2.change = -1;
                            if (jsonObj['lot_volume_2'] === stock.buy_qtty_2.intVal)
                                stock.buy_qtty_2.change = 0;
                            else if (jsonObj['lot_volume_2'] > stock.buy_qtty_2.intVal)
                                stock.buy_qtty_2.change = 1;
                            // stock.buy_qtty_2.val = (jsonObj['lot_volume_2'] / 10).toLocaleString('en');
                            stock.buy_qtty_2.val = (Number((jsonObj['lot_volume_2'] / 1000).toFixed(1))).toLocaleString('en');
                            stock.buy_qtty_2.intVal = jsonObj['lot_volume_2'];
                            if (stock.buy_qtty_2.change !== 0) {
                                stock.hasChange = 1;
                                stock.buy_qtty_2.time = stock.buy_qtty_2.time === 1 ? 2 : 1;
                            }
                        } else {
                            stock.buy_qtty_2.val = '';
                            stock.buy_qtty_2.intVal = 0;
                        }
                        if (jsonObj['price_1_best'] !== undefined && jsonObj['price_1_best'] > 0) {
                            tmpVariable = jsonObj['price_1_best'] / 1000;
                            stock.buy_1.change = -1;
                            if (tmpVariable === stock.buy_1.val)
                                stock.buy_1.change = 0;
                            else if (tmpVariable > stock.buy_1.val)
                                stock.buy_1.change = 1;
                            stock.buy_1.val = tmpVariable;
                            if (stock.buy_1.change !== 0) {
                                stock.hasChange = 1;
                                stock.buy_1.time = stock.buy_1.time === 1 ? 2 : 1;
                                stock.covered_warrant_id.forEach(function (itm) {
                                    if (lastDataHose[itm] !== undefined) {
                                        lastDataHose[itm].underlying.buy_1 = stock.buy_1.val;
                                        lastDataHose[itm].hasChange = 1;
                                        tmpDataHOSE.push(lastDataHose[itm]);
                                    }
                                });
                            }
                        } else {
                            stock.buy_1.val = '';
                        }
                        if (jsonObj['lot_volume_1'] !== undefined && jsonObj['lot_volume_1'] > 0) {
                            // tmpVariable = jsonObj['lot_volume_1'].toLocaleString('en')
                            //     .substring(0, jsonObj['lot_volume_1'].toLocaleString('en').length - 1);
                            stock.buy_qtty_1.change = -1;
                            if (jsonObj['lot_volume_1'] === stock.buy_qtty_1.intVal)
                                stock.buy_qtty_1.change = 0;
                            else if (jsonObj['lot_volume_1'] > stock.buy_qtty_1.intVal)
                                stock.buy_qtty_1.change = 1;
                            // stock.buy_qtty_1.val = (jsonObj['lot_volume_1'] / 10).toLocaleString('en');
                            stock.buy_qtty_1.val = (Number((jsonObj['lot_volume_1'] / 1000).toFixed(1))).toLocaleString('en');
                            stock.buy_qtty_1.intVal = jsonObj['lot_volume_1'];
                            if (stock.buy_qtty_1.change !== 0) {
                                stock.hasChange = 1;
                                stock.buy_qtty_1.time = stock.buy_qtty_1.time === 1 ? 2 : 1;
                            }
                        } else {
                            stock.buy_qtty_1.val = '';
                            stock.buy_qtty_1.intVal = 0;
                        }
                        if (stock.buy_qtty_1.val !== '' && stock.buy_1.val === ''
                            && (stock.trading_session === 'ATO' || stock.trading_session === 'O')) {
                            stock.buy_1.val = 'ATO';
                        } else if (stock.buy_qtty_1.val !== '' && stock.buy_1.val === ''
                            && (stock.trading_session === 'ATC' || stock.trading_session === 'C')) {
                            stock.buy_1.val = 'ATC';
                        }
                    }
                    else if (jsonObj['side'] === 'S') {
                        if (jsonObj['price_3_best'] !== undefined && jsonObj['price_3_best'] > 0) {
                            tmpVariable = jsonObj['price_3_best'] / 1000;
                            stock.sell_3.change = -1;
                            if (tmpVariable === stock.sell_3.val)
                                stock.sell_3.change = 0;
                            else if (tmpVariable > stock.sell_3.val)
                                stock.sell_3.change = 1;
                            stock.sell_3.val = tmpVariable;
                            if (stock.sell_3.change !== 0) {
                                stock.hasChange = 1;
                                stock.sell_3.time = stock.sell_3.time === 1 ? 2 : 1;
                                stock.covered_warrant_id.forEach(function (itm) {
                                    if (lastDataHose[itm] !== undefined) {
                                        lastDataHose[itm].underlying.sell_3 = stock.sell_3.val;
                                        lastDataHose[itm].hasChange = 1;
                                        tmpDataHOSE.push(lastDataHose[itm]);
                                    }
                                });
                            }
                        } else {
                            stock.sell_3.val = '';
                        }
                        if (jsonObj['lot_volume_3'] !== undefined && jsonObj['lot_volume_3'] > 0) {
                            // tmpVariable = jsonObj['lot_volume_3'].toLocaleString('en')
                            //     .substring(0, jsonObj['lot_volume_3'].toLocaleString('en').length - 1);
                            stock.sell_qtty_3.change = -1;
                            if (jsonObj['lot_volume_3'] === stock.sell_qtty_3.intVal)
                                stock.sell_qtty_3.change = 0;
                            else if (jsonObj['lot_volume_3'] > stock.sell_qtty_3.intVal)
                                stock.sell_qtty_3.change = 1;
                            // stock.sell_qtty_3.val = (jsonObj['lot_volume_3'] / 10).toLocaleString('en');
                            stock.sell_qtty_3.val = (Number((jsonObj['lot_volume_3'] / 1000).toFixed(1))).toLocaleString('en');
                            stock.sell_qtty_3.intVal = jsonObj['lot_volume_3'];
                            if (stock.sell_qtty_3.change !== 0) {
                                stock.hasChange = 1;
                                stock.sell_qtty_3.time = stock.sell_qtty_3.time === 1 ? 2 : 1;
                            }
                        } else {
                            stock.sell_qtty_3.val = '';
                            stock.sell_qtty_3.intVal = 0;
                        }
                        if (jsonObj['price_2_best'] !== undefined && jsonObj['price_2_best'] > 0) {
                            tmpVariable = jsonObj['price_2_best'] / 1000;
                            stock.sell_2.change = -1;
                            if (tmpVariable === stock.sell_2.val)
                                stock.sell_2.change = 0;
                            else if (tmpVariable > stock.sell_2.val)
                                stock.sell_2.change = 1;
                            stock.sell_2.val = tmpVariable;
                            if (stock.sell_2.change !== 0) {
                                stock.hasChange = 1;
                                stock.sell_2.time = stock.sell_2.time === 1 ? 2 : 1;
                                stock.covered_warrant_id.forEach(function (itm) {
                                    if (lastDataHose[itm] !== undefined) {
                                        lastDataHose[itm].underlying.sell_2 = stock.sell_2.val;
                                        lastDataHose[itm].hasChange = 1;
                                        tmpDataHOSE.push(lastDataHose[itm]);
                                    }
                                });
                            }
                        } else {
                            stock.sell_2.val = '';
                        }
                        if (jsonObj['lot_volume_2'] !== undefined && jsonObj['lot_volume_2'] > 0) {
                            // tmpVariable = jsonObj['lot_volume_2'].toLocaleString('en')
                            //     .substring(0, jsonObj['lot_volume_2'].toLocaleString('en').length - 1);
                            stock.sell_qtty_2.change = -1;
                            if (jsonObj['lot_volume_2'] === stock.sell_qtty_2.intVal)
                                stock.sell_qtty_2.change = 0;
                            else if (jsonObj['lot_volume_2'] > stock.sell_qtty_2.intVal)
                                stock.sell_qtty_2.change = 1;
                            // stock.sell_qtty_2.val = (jsonObj['lot_volume_2'] / 10).toLocaleString('en');
                            stock.sell_qtty_2.val = (Number((jsonObj['lot_volume_2'] / 1000).toFixed(1))).toLocaleString('en');
                            stock.sell_qtty_2.intVal = jsonObj['lot_volume_2'];
                            if (stock.sell_qtty_2.change !== 0) {
                                stock.hasChange = 1;
                                stock.sell_qtty_2.time = stock.sell_qtty_2.time === 1 ? 2 : 1;
                            }
                        } else {
                            stock.sell_qtty_2.val = '';
                            stock.sell_qtty_2.intVal = 0;
                        }
                        if (jsonObj['price_1_best'] !== undefined && jsonObj['price_1_best'] > 0) {
                            tmpVariable = jsonObj['price_1_best'] / 1000;
                            stock.sell_1.change = -1;
                            if (tmpVariable === stock.sell_1.val)
                                stock.sell_1.change = 0;
                            else if (tmpVariable > stock.sell_1.val)
                                stock.sell_1.change = 1;
                            stock.sell_1.val = tmpVariable;
                            if (stock.sell_1.change !== 0) {
                                stock.hasChange = 1;
                                stock.sell_1.time = stock.sell_1.time === 1 ? 2 : 1;
                                stock.covered_warrant_id.forEach(function (itm) {
                                    if (lastDataHose[itm] !== undefined) {
                                        lastDataHose[itm].underlying.sell_1 = stock.sell_1.val;
                                        lastDataHose[itm].hasChange = 1;
                                        tmpDataHOSE.push(lastDataHose[itm]);
                                    }
                                });
                            }
                        } else {
                            stock.sell_1.val = '';
                        }
                        if (jsonObj['lot_volume_1'] !== undefined && jsonObj['lot_volume_1'] > 0) {
                            // tmpVariable = jsonObj['lot_volume_1'].toLocaleString('en')
                            //     .substring(0, jsonObj['lot_volume_1'].toLocaleString('en').length - 1);
                            stock.sell_qtty_1.change = -1;
                            if (jsonObj['lot_volume_1'] === stock.sell_qtty_1.intVal)
                                stock.sell_qtty_1.change = 0;
                            else if (jsonObj['lot_volume_1'] > stock.sell_qtty_1.intVal)
                                stock.sell_qtty_1.change = 1;
                            // stock.sell_qtty_1.val = (jsonObj['lot_volume_1'] / 10).toLocaleString('en');
                            stock.sell_qtty_1.val = (Number((jsonObj['lot_volume_1'] / 1000).toFixed(1))).toLocaleString('en');
                            stock.sell_qtty_1.intVal = jsonObj['lot_volume_1'];
                            if (stock.sell_qtty_1.change !== 0) {
                                stock.hasChange = 1;
                                stock.sell_qtty_1.time = stock.sell_qtty_1.time === 1 ? 2 : 1;
                            }
                        } else {
                            stock.sell_qtty_1.val = '';
                            stock.sell_qtty_1.intVal = 0;
                        }
                        if (stock.sell_qtty_1.val !== '' && stock.sell_1.val === ''
                            && (stock.trading_session === 'ATO' || stock.trading_session === 'O')) {
                            stock.sell_1.val = 'ATO';
                        } else if (stock.sell_qtty_1.val !== '' && stock.sell_1.val === ''
                            && (stock.trading_session === 'ATC' || stock.trading_session === 'C')) {
                            stock.sell_1.val = 'ATC';
                        }
                    }
                }
                // SU Msg
                else if (jsonObj['MsgType'] === 'SU') {
                    lastDataHoseIntraday.push({
                        id: jsonObj['security_number_old'],
                        newId: jsonObj['security_number_new'],
                        symbol: jsonObj['security_symbol'],
                        name: jsonObj['security_name'],
                        security_type: jsonObj['security_type'],
                        ceiling: jsonObj['ceiling_price'],
                        floor: jsonObj['floor_price'],
                        reference: jsonObj['last_sale_price'],
                        underlying_symbol: jsonObj['underlying_symbol'],
                        issuer_name: jsonObj['issuer_name'],
                        maturity_date: jsonObj['maturity_date'],
                        last_trading_date: jsonObj['last_trading_date'],
                        exercise_price: jsonObj['exercise_price'],
                        exercise_ratio: jsonObj['exercise_ratio']
                    });
                }
                if (stock.hasChange) tmpDataHOSE.push(stock);
                lastDataHose[jsonObj['security_number']] = stock;
            }
        }

        //save lastPrice stock
        saveLastPriceStock();

        var data = tmpDataHOSE;
        if (isNeedResetData) {
            isNeedResetData = false;
            var count = 5;
            var resetDataInterval = setInterval(() => {
                count--;
                if (count <= 0) {
                    clearInterval(resetDataInterval);
                }
                redis.commonDb.del("OnlineTrader:NotifyLogsTrader");
                makerSimulator = {};
                redis.commonDb.get("StockBoard:lastDataHoseIntraday", function (err, reply) {
                    if (err || reply === null) {
                        errorLogger.info("Redis error or lastDataHoseIntraday null");
                        return;
                    }
                    if (reply !== null) {
                        lastDataHoseIntraday = JSON.parse(reply);
                    }
                    lastDataHoseIntraday.forEach(el => {
                        if (el.id !== el.newId) delete lastDataHose[el.id];
                        var stockTmp = null;
                        if (el.security_type === 'W') {
                            stockTmp = Object.values(lastDataHose).find(ele => ele.symbol === el.underlying_symbol);
                            if (!stockTmp.covered_warrant_id.includes(el.newId))
                                stockTmp.covered_warrant_id.push(el.newId);
                        }
                        if (lastDataHose[el.newId] !== undefined) {
                            lastDataHose[el.newId].ceiling = el.ceiling / 1000;
                            lastDataHose[el.newId].floor = el.floor / 1000;
                            lastDataHose[el.newId].reference = el.reference / 1000;
                            lastDataHose[el.newId].issuer_name = el.issuer_name;
                            lastDataHose[el.newId].maturity_date = stringHelper.convertDate(el.maturity_date, 'yyyyMMdd', '/');
                            lastDataHose[el.newId].last_trading_date = stringHelper.convertDate(el.last_trading_date, 'yyyyMMdd', '/');
                            lastDataHose[el.newId].exercise_price = (el.exercise_price / 10000).toFixed(2);
                            lastDataHose[el.newId].exercise_ratio = el.exercise_ratio;
                            lastDataHose[el.newId].underlying = {
                                id: stockTmp === null ? 0 : stockTmp.id,
                                symbol: el.underlying_symbol, price: { val: '', change: 0, time: 0 },
                                color: 0, buy_1: 0, sell_1: 0
                            };
                        } else {
                            commonLogger.info("New symbol " + el.symbol);
                            delete lastDataHnx[el.symbol];
                            lastDataHose[el.newId] = {
                                id: el.newId, symbol: el.symbol, company: el.name, ceiling: el.ceiling / 1000,
                                floor: el.floor / 1000, reference: el.reference === 0 ? (el.ceiling + el.floor) / 2000 : el.reference / 1000,
                                buy_10: { val: '', change: 0, time: 0 }, buy_qtty_10: { val: '', intVal: 0, change: 0, time: 0 },
                                buy_9: { val: '', change: 0, time: 0 }, buy_qtty_9: { val: '', intVal: 0, change: 0, time: 0 },
                                buy_8: { val: '', change: 0, time: 0 }, buy_qtty_8: { val: '', intVal: 0, change: 0, time: 0 },
                                buy_7: { val: '', change: 0, time: 0 }, buy_qtty_7: { val: '', intVal: 0, change: 0, time: 0 },
                                buy_6: { val: '', change: 0, time: 0 }, buy_qtty_6: { val: '', intVal: 0, change: 0, time: 0 },
                                buy_5: { val: '', change: 0, time: 0 }, buy_qtty_5: { val: '', intVal: 0, change: 0, time: 0 },
                                buy_4: { val: '', change: 0, time: 0 }, buy_qtty_4: { val: '', intVal: 0, change: 0, time: 0 },
                                buy_3: { val: '', change: 0, time: 0 }, buy_qtty_3: { val: '', intVal: 0, change: 0, time: 0 },
                                buy_2: { val: '', change: 0, time: 0 }, buy_qtty_2: { val: '', intVal: 0, change: 0, time: 0 },
                                buy_1: { val: '', change: 0, time: 0 }, buy_qtty_1: { val: '', intVal: 0, change: 0, time: 0 },
                                match_price: { val: '', change: 0, time: 0 }, match_qtty: { val: '', intVal: 0, change: 0, time: 0 }, difference: 0,
                                sell_1: { val: '', change: 0, time: 0 }, sell_qtty_1: { val: '', intVal: 0, change: 0, time: 0 },
                                sell_2: { val: '', change: 0, time: 0 }, sell_qtty_2: { val: '', intVal: 0, change: 0, time: 0 },
                                sell_3: { val: '', change: 0, time: 0 }, sell_qtty_3: { val: '', intVal: 0, change: 0, time: 0 },
                                sell_4: { val: '', change: 0, time: 0 }, sell_qtty_4: { val: '', intVal: 0, change: 0, time: 0 },
                                sell_5: { val: '', change: 0, time: 0 }, sell_qtty_5: { val: '', intVal: 0, change: 0, time: 0 },
                                sell_6: { val: '', change: 0, time: 0 }, sell_qtty_6: { val: '', intVal: 0, change: 0, time: 0 },
                                sell_7: { val: '', change: 0, time: 0 }, sell_qtty_7: { val: '', intVal: 0, change: 0, time: 0 },
                                sell_8: { val: '', change: 0, time: 0 }, sell_qtty_8: { val: '', intVal: 0, change: 0, time: 0 },
                                sell_9: { val: '', change: 0, time: 0 }, sell_qtty_9: { val: '', intVal: 0, change: 0, time: 0 },
                                sell_10: { val: '', change: 0, time: 0 }, sell_qtty_10: { val: '', intVal: 0, change: 0, time: 0 },
                                total_vol: { val: '', intVal: 0, change: 0, time: 0 }, total_value: { val: '', intVal: 0, change: 0, time: 0 },
                                open_price: '', high_price: { val: '', change: 0, time: 0 }, low_price: { val: '', change: 0, time: 0 },
                                nn_buy: { val: '', change: 0, time: 0 },
                                nn_sell: { val: '', change: 0, time: 0 },
                                nn_room: { val: '', change: 0, time: 0 },
                                trading_session: tradingSessionHOSE, stock_type: el.security_type,
                                issuer_name: el.issuer_name,
                                maturity_date: stringHelper.convertDate(el.maturity_date, 'yyyyMMdd', '/'),
                                last_trading_date: stringHelper.convertDate(el.last_trading_date, 'yyyyMMdd', '/'),
                                exercise_price: (el.exercise_price / 10000).toFixed(2),
                                exercise_ratio: el.exercise_ratio,
                                underlying: {
                                    id: stockTmp === null ? 0 : stockTmp.id,
                                    symbol: el.underlying_symbol, price: { val: '', change: 0, time: 0 },
                                    color: 0, buy_1: 0, sell_1: 0
                                },
                                covered_warrant_id: [],
                                market: 'hose', hasChange: 0
                            };
                            if (process.env.WRITE_LAST_PRICE_STOCK === 'Y') {
                                redis.commonDb.get("StockBoard:LastPriceStock", function (err, reply) {
                                    if (err) {
                                        errorLogger.info('read [LastPriceStock] error: ' + err);
                                        return;
                                    }
                                    if (reply !== null) {
                                        var tmpLastPriceStock = JSON.parse(reply);
                                        tmpLastPriceStock[el.symbol] = {
                                            "symbol": el.symbol,
                                            "floor": "HOSE",
                                            "price": lastDataHose[el.newId].reference,
                                            "basicPrice": lastDataHose[el.newId].reference,
                                            "stocktype": "W"
                                        }
                                        redis.commonDb.set('StockBoard:LastPriceStock', JSON.stringify(tmpLastPriceStock));
                                    }
                                });
                            }
                        }
                    });
                    lastDataHoseIntraday = []; // clear data
                    resetData();
                    commonLogger.info("Reset data success");
                    data = Object.values(Object.assign({}, lastDataHose, lastDataHnx));
                    io.emit('onChangeStockForOnline', jo.pack(data));
                    pushMakerStockChangeEvent(data);
                });
            }, 5000);
        } else if (data.length) {
            io.emit('onChangeStockForOnline', jo.pack(data));
            pushMakerStockChangeEvent(data);
            if (Object.values(last10MatchHistHOSE).length) {
                io.emit('onChangeMatchHist', jo.pack(last10MatchHistHOSE));
            }
        }
    }
}

function hnxOnChangeHandler(io, string) {
    if (!string.includes("HeartBeat")) {
        var jsonArr = JSON.parse(string);
        var tmpDataHNX = [], tmpVariable = 0;
        var last10MatchHistHNX = {};
        jsonArr.forEach(function (jsonObj) {
            if (jsonObj['MsgType'] === 'TP' && lastDataHnx[jsonObj['Symbol']] === undefined) return;
            if (jsonObj['MsgType'] === 'SI' || jsonObj['MsgType'] === 'TP') {
                var stock = lastDataHnx[jsonObj['Symbol']];
                if (stock === undefined) {
                    lastDataHnx[jsonObj['Symbol']] = {
                        id: jsonObj['Symbol'], symbol: jsonObj['Symbol'], ceiling: '', floor: '', reference: '',
                        buy_10: { val: '', change: 0, time: 0 }, buy_qtty_10: { val: '', intVal: 0, change: 0, time: 0 },
                        buy_9: { val: '', change: 0, time: 0 }, buy_qtty_9: { val: '', intVal: 0, change: 0, time: 0 },
                        buy_8: { val: '', change: 0, time: 0 }, buy_qtty_8: { val: '', intVal: 0, change: 0, time: 0 },
                        buy_7: { val: '', change: 0, time: 0 }, buy_qtty_7: { val: '', intVal: 0, change: 0, time: 0 },
                        buy_6: { val: '', change: 0, time: 0 }, buy_qtty_6: { val: '', intVal: 0, change: 0, time: 0 },
                        buy_5: { val: '', change: 0, time: 0 }, buy_qtty_5: { val: '', intVal: 0, change: 0, time: 0 },
                        buy_4: { val: '', change: 0, time: 0 }, buy_qtty_4: { val: '', intVal: 0, change: 0, time: 0 },
                        buy_3: { val: '', change: 0, time: 0 }, buy_qtty_3: { val: '', intVal: 0, change: 0, time: 0 },
                        buy_2: { val: '', change: 0, time: 0 }, buy_qtty_2: { val: '', intVal: 0, change: 0, time: 0 },
                        buy_1: { val: '', change: 0, time: 0 }, buy_qtty_1: { val: '', intVal: 0, change: 0, time: 0 },
                        match_price: { val: '', change: 0, time: 0 }, match_qtty: { val: '', intVal: 0, change: 0, time: 0 }, difference: 0,
                        sell_1: { val: '', change: 0, time: 0 }, sell_qtty_1: { val: '', intVal: 0, change: 0, time: 0 },
                        sell_2: { val: '', change: 0, time: 0 }, sell_qtty_2: { val: '', intVal: 0, change: 0, time: 0 },
                        sell_3: { val: '', change: 0, time: 0 }, sell_qtty_3: { val: '', intVal: 0, change: 0, time: 0 },
                        sell_4: { val: '', change: 0, time: 0 }, sell_qtty_4: { val: '', intVal: 0, change: 0, time: 0 },
                        sell_5: { val: '', change: 0, time: 0 }, sell_qtty_5: { val: '', intVal: 0, change: 0, time: 0 },
                        sell_6: { val: '', change: 0, time: 0 }, sell_qtty_6: { val: '', intVal: 0, change: 0, time: 0 },
                        sell_7: { val: '', change: 0, time: 0 }, sell_qtty_7: { val: '', intVal: 0, change: 0, time: 0 },
                        sell_8: { val: '', change: 0, time: 0 }, sell_qtty_8: { val: '', intVal: 0, change: 0, time: 0 },
                        sell_9: { val: '', change: 0, time: 0 }, sell_qtty_9: { val: '', intVal: 0, change: 0, time: 0 },
                        sell_10: { val: '', change: 0, time: 0 }, sell_qtty_10: { val: '', intVal: 0, change: 0, time: 0 },
                        total_vol: { val: '', intVal: 0, change: 0, time: 0 }, total_value: { val: '', intVal: 0, change: 0, time: 0 },
                        open_price: '', high_price: { val: '', change: 0, time: 0 },
                        low_price: { val: '', change: 0, time: 0 }, nn_buy: { val: '', change: 0, time: 0 },
                        nn_sell: { val: '', change: 0, time: 0 }, nn_room: { val: '', change: 0, time: 0 }, hasChange: 0,
                        trading_session: '', market: jsonObj['TradingSessionCode'] === 'LIS_BRD_01' ? 'hnx'
                            : (jsonObj['TradingSessionCode'] === 'UPC_BRD_01' ? 'upcom' : '')
                    };
                    stock = lastDataHnx[jsonObj['Symbol']];
                    var tmpStock = Object.values(lastDataHose).find(el => el.symbol === jsonObj['Symbol']);
                    if (tmpStock !== undefined) {
                        delete lastDataHose[tmpStock.id];
                    }
                }

                if (jsonObj['MsgType'] === 'SI') {
                    resetSIDataChange(stock);
                    stock.symbol = jsonObj['Symbol'];
                    stock.trading_session = '';
                    stock.stock_type = jsonObj['StockType'];
                    if (jsonObj['TradingSeStatus'] === '1') {
                        if (jsonObj['TradingSessionID'] === 'LIS_AUC_C_NML')
                            stock.trading_session = 'ATC';
                        else if (jsonObj['TradingSessionID'] === 'LIS_PTH_P_NML') {
                            stock.trading_session = 'PLO';
                            isNeedResetTPHNX = true;
                        } else {
                            stock.trading_session = 'OPEN';
                        }
                    } else if (jsonObj['TradingSeStatus'] === '2') {
                        stock.trading_session = 'BREAK';
                    } else {
                        // if (jsonObj['TradingSeStatus'] === '97' && jsonObj['MidPX'] > 0) {
                        //     stockHelper.writeLastPriceStock('basis', stock.symbol, jsonObj['MidPX'] / 1000);
                        // }
                        stock.trading_session = 'CLOSE';
                    }
                    stock.ceiling = jsonObj['CeilingPrice'] / 1000;
                    stock.floor = jsonObj['FloorPrice'] / 1000;
                    stock.reference = jsonObj['BasicPrice'] / 1000;
                    stock.open_price = jsonObj['OpenPrice'] === undefined || jsonObj['OpenPrice'] === 0 ? stock.open_price : jsonObj['OpenPrice'] / 1000;

                    if (jsonObj['HighestPrice'] !== undefined && jsonObj['HighestPrice'] > 0) {
                        tmpVariable = jsonObj['HighestPrice'] / 1000;
                        if (tmpVariable === stock.high_price.val)
                            stock.high_price.change = 0;
                        else {
                            stock.high_price.change = 1;
                            stock.hasChange = 1;
                            stock.high_price.time = stock.high_price.time === 1 ? 2 : 1;
                        }
                        stock.high_price.val = tmpVariable;
                    }
                    if (jsonObj['LowestPrice'] !== undefined && jsonObj['LowestPrice'] > 0) {
                        tmpVariable = jsonObj['LowestPrice'] / 1000;
                        if (tmpVariable === stock.low_price.val)
                            stock.low_price.change = 0;
                        else {
                            stock.low_price.change = 1;
                            stock.hasChange = 1;
                            stock.low_price.time = stock.low_price.time === 1 ? 2 : 1;
                        }
                        stock.low_price.val = tmpVariable;
                    }
                    if (jsonObj['BuyForeignQtty'] !== undefined) {
                        if (jsonObj['BuyForeignQtty'] % 10 > 4) jsonObj['BuyForeignQtty'] += 10;
                        // tmpVariable = jsonObj['BuyForeignQtty'].toLocaleString('en')
                        //     .substring(0, jsonObj['BuyForeignQtty'].toLocaleString('en').length - 1);
                        // tmpVariable = (jsonObj['BuyForeignQtty'] / 10).toLocaleString('en');
                        tmpVariable = (Number((jsonObj['BuyForeignQtty'] / 1000).toFixed(1))).toLocaleString('en');
                        if (tmpVariable === stock.nn_buy.val)
                            stock.nn_buy.change = 0;
                        else {
                            stock.nn_buy.change = 1;
                            stock.hasChange = 1;
                            stock.nn_buy.time = stock.nn_buy.time === 1 ? 2 : 1;
                        }
                        stock.nn_buy.val = tmpVariable;
                    }
                    if (jsonObj['SellForeignQtty'] !== undefined) {
                        if (jsonObj['SellForeignQtty'] % 10 > 4) jsonObj['SellForeignQtty'] += 10;
                        // tmpVariable = jsonObj['SellForeignQtty'].toLocaleString('en')
                        //     .substring(0, jsonObj['SellForeignQtty'].toLocaleString('en').length - 1);
                        // tmpVariable = (jsonObj['SellForeignQtty'] / 10).toLocaleString('en');
                        tmpVariable = (Number((jsonObj['SellForeignQtty'] / 1000).toFixed(1))).toLocaleString('en');
                        if (tmpVariable === stock.nn_sell.val)
                            stock.nn_sell.change = 0;
                        else {
                            stock.nn_sell.change = 1;
                            stock.hasChange = 1;
                            stock.nn_sell.time = stock.nn_sell.time === 1 ? 2 : 1;
                        }
                        stock.nn_sell.val = tmpVariable;
                    }
                    if (jsonObj['RemainForeignQtty'] !== undefined) {
                        if (jsonObj['RemainForeignQtty'] % 10 > 4) jsonObj['RemainForeignQtty'] += 10;
                        // tmpVariable = jsonObj['RemainForeignQtty'].toLocaleString('en')
                        //     .substring(0, jsonObj['RemainForeignQtty'].toLocaleString('en').length - 1);
                        // tmpVariable = Math.round(jsonObj['RemainForeignQtty'] / 10).toLocaleString('en');
                        tmpVariable = (Number((jsonObj['RemainForeignQtty'] / 1000).toFixed(1))).toLocaleString('en');
                        if (tmpVariable === stock.nn_room.val)
                            stock.nn_room.change = 0;
                        else {
                            stock.nn_room.change = 1;
                            stock.hasChange = 1;
                            stock.nn_room.time = stock.nn_room.time === 1 ? 2 : 1;
                        }
                        stock.nn_room.val = tmpVariable;
                    }

                    if (jsonObj['NM_TotalTradedQtty'] !== undefined) {
                        // tmpVariable = jsonObj['NM_TotalTradedQtty'].toLocaleString('en')
                        //     .substring(0, jsonObj['NM_TotalTradedQtty'].toLocaleString('en').length - 1);
                        // tmpVariable = (jsonObj['NM_TotalTradedQtty'] / 10).toLocaleString('en');
                        tmpVariable = (Number((jsonObj['NM_TotalTradedQtty'] / 1000).toFixed(1))).toLocaleString('en');
                        if (tmpVariable === stock.total_vol.val)
                            stock.total_vol.change = 0;
                        else {
                            stock.total_vol.change = 1;
                            stock.hasChange = 1;
                            stock.total_vol.time = stock.total_vol.time === 1 ? 2 : 1;
                            var volume = jsonObj['NM_TotalTradedQtty'] - stock.total_vol.intVal;
                            if (matchHistData[stock.symbol] === undefined) {
                                matchHistData[stock.symbol] = [{
                                    time: jsonObj['Time'],
                                    price: stock.match_price.val,
                                    volume: volume,
                                    change_value: Number((stock.match_price.val - stock.reference).toFixed(2)),
                                    total_vol: jsonObj['NM_TotalTradedQtty']
                                }];
                                last10MatchHistHNX[stock.symbol] = matchHistData[stock.symbol];
                            } else {
                                matchHistData[stock.symbol].push({
                                    time: jsonObj['Time'],
                                    price: stock.match_price.val,
                                    volume: volume,
                                    change_value: Number((stock.match_price.val - stock.reference).toFixed(2)),
                                    total_vol: jsonObj['NM_TotalTradedQtty']
                                });
                                last10MatchHistHNX[stock.symbol] = matchHistData[stock.symbol].slice(matchHistData[stock.symbol].length - 10);
                            }
                        }
                        stock.total_vol.val = tmpVariable;
                        stock.total_vol.intVal = jsonObj['NM_TotalTradedQtty'];
                    }

                    if (jsonObj['NM_TotalTradedValue'] !== undefined) {
                        if (jsonObj['NM_TotalTradedValue'] === stock.total_value.intVal)
                            stock.total_value.change = 0;
                        else {
                            stock.total_value.change = 1;
                            stock.hasChange = 1;
                            stock.total_value.time = stock.total_value.time === 1 ? 2 : 1;
                        }
                        stock.total_value.val = Math.round(jsonObj['NM_TotalTradedValue'] / 1000000).toLocaleString('en');
                        stock.total_value.intVal = jsonObj['NM_TotalTradedValue'];
                    }

                    if (jsonObj['TradingSeStatus'] === '90') return;

                    if ((stock.trading_session === 'ATC' || stock.trading_session === 'PLO') && jsonObj['CurrentQtty'] != 0) {
                        if (jsonObj['CurrentPrice'] !== undefined && jsonObj['CurrentPrice'] > 0) {
                            tmpVariable = jsonObj['CurrentPrice'] / 1000;
                            if (tmpVariable === stock.match_price.val)
                                stock.match_price.change = 0;
                            else {
                                stock.match_price.change = 1;
                                stock.hasChange = 1;
                                stock.match_price.time = stock.match_price.time === 1 ? 2 : 1;
                                // stockHelper.writeLastPriceStock('basis', stock.symbol, tmpVariable);
                            }
                            stock.match_price.val = tmpVariable;
                            lastPriceStock[stock.symbol] = {
                                symbol: stock.symbol,
                                floor: stock.market.toUpperCase(),
                                basicPrice: Math.round(stock.reference * 1000),
                                price: jsonObj['CurrentPrice']
                            };
                            if (stock.stock_type !== '') lastPriceStock[stock.symbol]['stocktype'] = stock.stock_type;
                        } else {
                            stock.match_price.val = '';
                        }
                        if (jsonObj['CurrentQtty'] !== undefined) {
                            tmpVariable = jsonObj['CurrentQtty'].toLocaleString('en')
                                .substring(0, jsonObj['CurrentQtty'].toLocaleString('en').length - 1);
                            if (jsonObj['CurrentQtty'] === stock.match_qtty.intVal)
                                stock.match_qtty.change = 0;
                            else {
                                stock.match_qtty.change = 1;
                                stock.hasChange = 1;
                                stock.match_qtty.time = stock.match_qtty.time === 1 ? 2 : 1;
                            }
                            stock.match_qtty.val = (Number((jsonObj['CurrentQtty'] / 1000).toFixed(1))).toLocaleString('en');
                            stock.match_qtty.intVal = jsonObj['CurrentQtty'];
                        }
                    } else {
                        if (jsonObj['MatchPrice'] !== undefined && jsonObj['MatchPrice'] > 0) {
                            tmpVariable = jsonObj['MatchPrice'] / 1000;
                            if (tmpVariable === stock.match_price.val)
                                stock.match_price.change = 0;
                            else {
                                stock.match_price.change = 1;
                                stock.hasChange = 1;
                                stock.match_price.time = stock.match_price.time === 1 ? 2 : 1;
                                // stockHelper.writeLastPriceStock('basis', stock.symbol, tmpVariable);
                            }
                            stock.match_price.val = tmpVariable;
                            lastPriceStock[stock.symbol] = {
                                symbol: stock.symbol,
                                floor: stock.market.toUpperCase(),
                                basicPrice: Math.round(stock.reference * 1000),
                                price: jsonObj['MatchPrice']
                            };
                            if (stock.stock_type !== '') lastPriceStock[stock.symbol]['stocktype'] = stock.stock_type;
                        } else {
                            stock.match_price.val = '';
                        }
                        if (jsonObj['MatchQtty'] !== undefined) {
                            // tmpVariable = jsonObj['MatchQtty'].toLocaleString('en')
                            //     .substring(0, jsonObj['MatchQtty'].toLocaleString('en').length - 1);
                            if (jsonObj['MatchQtty'] === stock.match_qtty.intVal)
                                stock.match_qtty.change = 0;
                            else {
                                stock.match_qtty.change = 1;
                                stock.hasChange = 1;
                                stock.match_qtty.time = stock.match_qtty.time === 1 ? 2 : 1;
                            }
                            stock.match_qtty.val = (Number((jsonObj['MatchQtty'] / 1000).toFixed(1))).toLocaleString('en');
                            stock.match_qtty.intVal = jsonObj['MatchQtty'];
                        }
                    }

                    if (stock.match_price.val === '' || stock.match_price.val === stock.reference) {
                        stock.difference = '';
                    } else {
                        stock.difference = (stock.match_price.val - stock.reference).toFixed(2);
                    }
                }
                if (jsonObj['MsgType'] === 'TP') {
                    if (jsonObj['BestBidPrice0'] !== undefined) {
                        resetTPData(stock);
                        stock.hasChange = 1;
                    }
                    else {
                        resetTPDataChange(stock);
                        stock.symbol = jsonObj['Symbol'];
                        for (var top = 1; top <= 10; top++) {
                            if (jsonObj['BestBidPrice' + top] !== undefined && jsonObj['BestBidPrice' + top] > 0) {
                                tmpVariable = jsonObj['BestBidPrice' + top] / 1000;
                                stock['buy_' + top].change = -1;
                                if (tmpVariable === stock['buy_' + top].val)
                                    stock['buy_' + top].change = 0;
                                else if (tmpVariable > stock['buy_' + top].val)
                                    stock['buy_' + top].change = 1;
                                stock['buy_' + top].val = tmpVariable;
                                if (stock['buy_' + top].change !== 0) {
                                    stock.hasChange = 1;
                                    stock['buy_' + top].time = stock['buy_' + top].time === 1 ? 2 : 1;
                                }
                            } else {
                                stock['buy_' + top].val = '';
                            }
                            if (jsonObj['BestBidQtty' + top] !== undefined && jsonObj['BestBidQtty' + top] > 0) {
                                // tmpVariable = jsonObj['BestBidQtty' + top].toLocaleString('en')
                                //     .substring(0, jsonObj['BestBidQtty' + top].toLocaleString('en').length - 1);
                                stock['buy_qtty_' + top].change = -1;
                                if (jsonObj['BestBidQtty' + top] === stock['buy_qtty_' + top].intVal)
                                    stock['buy_qtty_' + top].change = 0;
                                else if (jsonObj['BestBidQtty' + top] > stock['buy_qtty_' + top].intVal)
                                    stock['buy_qtty_' + top].change = 1;
                                // stock['buy_qtty_' + top].val = (jsonObj['BestBidQtty' + top] / 10).toLocaleString('en');
                                stock['buy_qtty_' + top].val = (Number((jsonObj['BestBidQtty' + top] / 1000).toFixed(1))).toLocaleString('en');
                                stock['buy_qtty_' + top].intVal = jsonObj['BestBidQtty' + top];
                                if (stock['buy_qtty_' + top].change !== 0)
                                    stock['buy_qtty_' + top].time = stock['buy_qtty_' + top].time === 1 ? 2 : 1;
                            } else {
                                stock['buy_qtty_' + top].val = '';
                                stock['buy_qtty_' + top].intVal = 0;
                            }

                            if (jsonObj['BestOfferPrice' + top] !== undefined && jsonObj['BestOfferPrice' + top] > 0) {
                                tmpVariable = jsonObj['BestOfferPrice' + top] / 1000;
                                stock['sell_' + top].change = -1;
                                if (tmpVariable === stock['sell_' + top].val)
                                    stock['sell_' + top].change = 0;
                                else if (tmpVariable > stock['sell_' + top].val)
                                    stock['sell_' + top].change = 1;
                                stock['sell_' + top].val = tmpVariable;
                                if (stock['sell_' + top].change !== 0) {
                                    stock.hasChange = 1;
                                    stock['sell_' + top].time = stock['sell_' + top].time === 1 ? 2 : 1;
                                }
                            } else {
                                stock['sell_' + top].val = '';
                            }
                            if (jsonObj['BestOfferQtty' + top] !== undefined && jsonObj['BestOfferQtty' + top] > 0) {
                                // tmpVariable = jsonObj['BestOfferQtty' + top].toLocaleString('en')
                                //     .substring(0, jsonObj['BestOfferQtty' + top].toLocaleString('en').length - 1);
                                stock['sell_qtty_' + top].change = -1;
                                if (jsonObj['BestOfferQtty' + top] === stock['sell_qtty_' + top].intVal)
                                    stock['sell_qtty_' + top].change = 0;
                                else if (jsonObj['BestOfferQtty' + top] > stock['sell_qtty_' + top].intVal)
                                    stock['sell_qtty_' + top].change = 1;
                                // stock['sell_qtty_' + 1].val = (jsonObj['BestOfferQtty' + top] / 10).toLocaleString('en');
                                stock['sell_qtty_' + top].val = (Number((jsonObj['BestOfferQtty' + top] / 1000).toFixed(1))).toLocaleString('en');
                                stock['sell_qtty_' + top].intVal = jsonObj['BestOfferQtty' + top];
                                if (stock['sell_qtty_' + top].change !== 0) {
                                    stock.hasChange = 1;
                                    stock['sell_qtty_' + top].time = stock['sell_qtty_' + top].time === 1 ? 2 : 1;
                                }
                            } else {
                                stock['sell_qtty_' + top].val = '';
                                stock['sell_qtty_' + top].intVal = 0;
                            }
                        }

                        if (stock.buy_qtty_1.val !== '' && stock.buy_1.val === '') {
                            stock.buy_1.val = 'ATC';
                        } else if (stock.buy_qtty_1.val === '') stock.buy_1.val = '';
                        if (stock.sell_qtty_1.val !== '' && stock.sell_1.val === '') {
                            stock.sell_1.val = 'ATC';
                        } else if (stock.sell_qtty_1.val === '') stock.sell_1.val = '';
                    }
                }
                if (stock.hasChange) tmpDataHNX.push(stock);
                lastDataHnx[jsonObj['Symbol']] = stock;
            }
        });

        //save lastPrice stock
        saveLastPriceStock();

        var data = tmpDataHNX;
        io.emit('onChangeStockForOnline', jo.pack(data));
        if (Object.values(last10MatchHistHNX).length) {
            io.emit('onChangeMatchHist', jo.pack(last10MatchHistHNX));
        }
    }
}

module.exports = {
    loadHistoryData,
    loadMatchHistData,
    initDataForOnline,
    hoseOnChangeHandler,
    hnxOnChangeHandler,
    pushMakerStockChangeEvent
}