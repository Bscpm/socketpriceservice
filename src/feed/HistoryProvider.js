const future = require('./FutureServerHandle');
const basis = require('./BasisServerHandle');
const dotenv = require('dotenv');
dotenv.config();
const rp = require('request-promise').defaults({ json: true })
const stockHelper = require('../utils/Stock');
const stringHelper = require('../utils/String');
const redis = require('../database/redis');

let basisStockData = {}, hoseStockData = {}, hnxStockData = {}, futureStockData = {},
    matchHistPSData = {}, matchHistCSData = {};
var countMatchHistCSRequest = 0, totalCountCS = 0,
    countMatchHistPSRequest = 0, totalCountPS = 0;
//get lastPrice stock
var lastPriceStock = {};
redis.commonDb.get("StockBoard:LastPriceStock", function (err, reply) {
    if (err) {
        errorLogger.info('[Redis - LastPriceStock] ' + err);
        res.send({ s: 'error', msg: 'error occurs!' });
        return;
    }
    if (reply !== null) {
        lastPriceStock = JSON.parse(reply);
    }
});

function initStockData() {
    rp({
        method: 'GET',
        url: process.env.API_URL + '/MarketData/GetCurrentStocksInfor',
    }).then(res => {
        if (res.s === 'ok') {
            totalCountCS = res.d.length + res.d1.length;
            res.d.forEach((el, index) => {
                initMatchHistCS(el.symbol, 2000 * index);
                if (el.market === "HASTC") el.market = 'hnx';
                else el.market = 'upcom';
                if (el.totalVolume % 10 > 4) el.totalVolume += 10;
                if (el.buyForeignQuantity % 10 > 4) el.buyForeignQuantity += 10;
                if (el.sellForeignQuantity % 10 > 4) el.sellForeignQuantity += 10;
                if (el.currentForeignRoom % 10 > 4) el.currentForeignRoom += 10;
                hnxStockData[el.symbol] = basisStockData[el.symbol] = {
                    id: el.symbol, symbol: el.symbol, company: el.companyName, ceiling: el.priceCeiling / 1000, floor: el.priceFloor / 1000, reference: el.priceBasic / 1000, 
                    buy_10: { val: el.priceBid10 <= 0 ? '' : el.priceBid10 / 1000, change: 0, time: 0 },
                    buy_qtty_10: {
                        val: el.quantityBid10 <= 0 ? '' : (el.quantityBid10 / 1000).toLocaleString('en'), intVal: el.quantityBid10 === undefined ? 0 : el.quantityBid10, change: 0, time: 0
                    },
                    buy_9: { val: el.priceBid9 <= 0 ? '' : el.priceBid9 / 1000, change: 0, time: 0 },
                    buy_qtty_9: {
                        val: el.quantityBid9 <= 0 ? '' : (el.quantityBid9 / 1000).toLocaleString('en'), intVal: el.quantityBid9 === undefined ? 0 : el.quantityBid9, change: 0, time: 0
                    },
                    buy_8: { val: el.priceBid8 <= 0 ? '' : el.priceBid8 / 1000, change: 0, time: 0 },
                    buy_qtty_8: {
                        val: el.quantityBid8 <= 0 ? '' : (el.quantityBid8 / 1000).toLocaleString('en'), intVal: el.quantityBid8 === undefined ? 0 : el.quantityBid8, change: 0, time: 0
                    },
                    buy_7: { val: el.priceBid7 <= 0 ? '' : el.priceBid7 / 1000, change: 0, time: 0 },
                    buy_qtty_7: {
                        val: el.quantityBid7 <= 0 ? '' : (el.quantityBid7 / 1000).toLocaleString('en'), intVal: el.quantityBid7 === undefined ? 0 : el.quantityBid7, change: 0, time: 0
                    },
                    buy_6: { val: el.priceBid6 <= 0 ? '' : el.priceBid6 / 1000, change: 0, time: 0 },
                    buy_qtty_6: {
                        val: el.quantityBid6 <= 0 ? '' : (el.quantityBid6 / 1000).toLocaleString('en'), intVal: el.quantityBid6 === undefined ? 0 : el.quantityBid6, change: 0, time: 0
                    },
                    buy_5: { val: el.priceBid5 <= 0 ? '' : el.priceBid5 / 1000, change: 0, time: 0 },
                    buy_qtty_5: {
                        val: el.quantityBid5 <= 0 ? '' : (el.quantityBid5 / 1000).toLocaleString('en'), intVal: el.quantityBid5 === undefined ? 0 : el.quantityBid5, change: 0, time: 0
                    },
                    buy_4: { val: el.priceBid4 <= 0 ? '' : el.priceBid4 / 1000, change: 0, time: 0 },
                    buy_qtty_4: {
                        val: el.quantityBid4 <= 0 ? '' : (el.quantityBid4 / 1000).toLocaleString('en'), intVal: el.quantityBid4 === undefined ? 0 : el.quantityBid4, change: 0, time: 0
                    },
                    buy_3: { val: el.priceBid3 <= 0 ? '' : el.priceBid3 / 1000, change: 0, time: 0 },
                    buy_qtty_3: {
                        val: el.quantityBid3 <= 0 ? '' : (el.quantityBid3 / 1000).toLocaleString('en'), intVal: el.quantityBid3 === undefined ? 0 : el.quantityBid3, change: 0, time: 0
                    },
                    buy_2: { val: el.priceBid2 <= 0 ? '' : el.priceBid2 / 1000, change: 0, time: 0 },
                    buy_qtty_2: {
                        val: el.quantityBid2 <= 0 ? '' : (el.quantityBid2 / 1000).toLocaleString('en'), intVal: el.quantityBid2 === undefined ? 0 : el.quantityBid2, change: 0, time: 0
                    },
                    buy_1: { val: el.priceBid1 <= 0 ? '' : el.priceBid1 / 1000, change: 0, time: 0 },
                    buy_qtty_1: {
                        val: el.quantityBid1 <= 0 ? '' : (el.quantityBid1 / 1000).toLocaleString('en'), intVal: el.quantityBid1 === undefined ? 0 : el.quantityBid1, change: 0, time: 0
                    },
                    match_price: { val: el.priceCurrent <= 0 ? '' : el.priceCurrent / 1000, change: 0, time: 0 },
                    match_qtty: {
                        val: el.volume <= 0 ? '' : (el.volume / 1000).toLocaleString('en'), intVal: el.volume, change: 0, time: 0
                    },
                    difference: el.priceChange === 0 ? '' : el.priceChange / 1000,
                    sell_1: { val: el.priceOffer1 <= 0 ? '' : el.priceOffer1 / 1000, change: 0, time: 0 },
                    sell_qtty_1: {
                        val: el.quantityOffer1 <= 0 ? '' : (el.quantityOffer1 / 1000).toLocaleString('en'), intVal: el.quantityOffer1 === undefined ? 0 : el.quantityOffer1, change: 0, time: 0
                    },
                    sell_2: { val: el.priceOffer2 <= 0 ? '' : el.priceOffer2 / 1000, change: 0, time: 0 },
                    sell_qtty_2: {
                        val: el.quantityOffer2 <= 0 ? '' : (el.quantityOffer2 / 1000).toLocaleString('en'), intVal: el.quantityOffer2 === undefined ? 0 : el.quantityOffer2, change: 0, time: 0
                    },
                    sell_3: { val: el.priceOffer3 <= 0 ? '' : el.priceOffer3 / 1000, change: 0, time: 0 },
                    sell_qtty_3: {
                        val: el.quantityOffer3 <= 0 ? '' : (el.quantityOffer3 / 1000).toLocaleString('en'), intVal: el.quantityOffer3 === undefined ? 0 : el.quantityOffer3, change: 0, time: 0
                    },
                    sell_4: { val: el.priceOffer4 <= 0 ? '' : el.priceOffer4 / 1000, change: 0, time: 0 },
                    sell_qtty_4: {
                        val: el.quantityOffer4 <= 0 ? '' : (el.quantityOffer4 / 1000).toLocaleString('en'), intVal: el.quantityOffer4 === undefined ? 0 : el.quantityOffer4, change: 0, time: 0
                    },
                    sell_5: { val: el.priceOffer5 <= 0 ? '' : el.priceOffer5 / 1000, change: 0, time: 0 },
                    sell_qtty_5: {
                        val: el.quantityOffer5 <= 0 ? '' : (el.quantityOffer5 / 1000).toLocaleString('en'), intVal: el.quantityOffer5 === undefined ? 0 : el.quantityOffer5, change: 0, time: 0
                    },
                    sell_6: { val: el.priceOffer6 <= 0 ? '' : el.priceOffer6 / 1000, change: 0, time: 0 },
                    sell_qtty_6: {
                        val: el.quantityOffer6 <= 0 ? '' : (el.quantityOffer6 / 1000).toLocaleString('en'), intVal: el.quantityOffer6 === undefined ? 0 : el.quantityOffer6, change: 0, time: 0
                    },
                    sell_7: { val: el.priceOffer7 <= 0 ? '' : el.priceOffer7 / 1000, change: 0, time: 0 },
                    sell_qtty_7: {
                        val: el.quantityOffer7 <= 0 ? '' : (el.quantityOffer7 / 1000).toLocaleString('en'), intVal: el.quantityOffer7 === undefined ? 0 : el.quantityOffer7, change: 0, time: 0
                    },
                    sell_8: { val: el.priceOffer8 <= 0 ? '' : el.priceOffer8 / 1000, change: 0, time: 0 },
                    sell_qtty_8: {
                        val: el.quantityOffer8 <= 0 ? '' : (el.quantityOffer8 / 1000).toLocaleString('en'), intVal: el.quantityOffer8 === undefined ? 0 : el.quantityOffer8, change: 0, time: 0
                    },
                    sell_9: { val: el.priceOffer9 <= 0 ? '' : el.priceOffer9 / 1000, change: 0, time: 0 },
                    sell_qtty_9: {
                        val: el.quantityOffer9 <= 0 ? '' : (el.quantityOffer9 / 1000).toLocaleString('en'), intVal: el.quantityOffer9 === undefined ? 0 : el.quantityOffer9, change: 0, time: 0
                    },
                    sell_10: { val: el.priceOffer10 <= 0 ? '' : el.priceOffer10 / 1000, change: 0, time: 0 },
                    sell_qtty_10: {
                        val: el.quantityOffer10 <= 0 ? '' : (el.quantityOffer10 / 1000).toLocaleString('en'), intVal: el.quantityOffer10 === undefined ? 0 : el.quantityOffer10, change: 0, time: 0
                    },
                    total_vol: {
                        val: el.totalVolume <= 0 ? '' : (Number((el.totalVolume / 1000).toFixed(1))).toLocaleString('en'), intVal: el.totalVolume, change: 0, time: 0
                    },
                    total_value: {
                        val: Math.round(el.totalValue / 1000000).toLocaleString('en') + ' tr', intVal: el.totalValue, change: 0, time: 0
                    },
                    open_price: el.priceOpen <= 0 ? '' : el.priceOpen / 1000,
                    high_price: { val: el.priceHigh <= 0 ? '' : el.priceHigh / 1000, change: 0, time: 0 },
                    low_price: { val: el.priceLow <= 0 ? '' : el.priceLow / 1000, change: 0, time: 0 },
                    nn_buy: {
                        val: el.buyForeignQuantity <= 0 ? '' : (Number((el.buyForeignQuantity / 1000).toFixed(1))).toLocaleString('en'), change: 0, time: 0
                    },
                    nn_sell: {
                        val: el.sellForeignQuantity <= 0 ? '' : (Number((el.sellForeignQuantity / 1000).toFixed(1))).toLocaleString('en'), change: 0, time: 0
                    },
                    nn_room: {
                        val: el.currentForeignRoom <= 0 ? '' : (Number((el.currentForeignRoom / 1000).toFixed(1))).toLocaleString('en'), change: 0, time: 0
                    }, trading_session: '', stock_type: '',
                    market: el.market, hasChange: 0
                };
            });

            res.d1.sort((a, b) => (a.stocktype > b.stocktype) ? 1 : ((b.stocktype > a.stocktype) ? -1 : 0));
            res.d1.forEach((el, index) => {
                initMatchHistCS(el.symbol, 2000 * index);
                if (el.lasttradingdate === null || el.lasttradingdate.trim() === ''
                    || !stringHelper.compareDateString(stringHelper.convertDate(el.lasttradingdate.trim(), 'yyyyMMdd', '/'), stringHelper.curDate2String('/'), '/')) {
                    el.totaltrading = el.totaltrading !== null ? parseInt(el.totaltrading) : 0
                    if (el.totaltrading % 10 > 4) el.totaltrading += 10;
                    el.foreignbuy = el.foreignbuy !== null ? parseInt(el.foreignbuy) : 0
                    if (el.foreignbuy % 10 > 4) el.foreignbuy += 10;
                    el.foreignsell = el.foreignsell !== null ? parseInt(el.foreignsell) : 0
                    if (el.foreignsell % 10 > 4) el.foreignsell += 10;
                    el.foreignremain = el.foreignremain !== null ? parseInt(el.foreignremain) : 0
                    if (el.foreignremain % 10 > 4) el.foreignremain += 10;
                    var underlying = {
                        id: 0, symbol: '', price: { val: '', change: 0, time: 0 }, color: 0,
                        buy_1: 0, buy_2: 0, buy_3: 0, sell_1: 0, sell_2: 0, sell_3: 0
                    };
                    if (el.stocktype === '4') {
                        if (el.lasttradingdate === null || el.lasttradingdate.trim() === '') return;
                        el.stocktype = 'W';
                        var tmpSymbol = el.underlyingsymbol === null ? '' : el.underlyingsymbol.trim();
                        var tmpStock = res.d1.find(el => el.symbol === tmpSymbol);
                        var colorStyle = -1;
                        if (tmpStock !== undefined) {
                            if (tmpStock.closeprice === tmpStock.reference || tmpStock.closeprice === null) colorStyle = 0;
                            else if (tmpStock.closeprice === tmpStock.ceiling) colorStyle = 2;
                            else if (tmpStock.closeprice === tmpStock.floor) colorStyle = -2;
                            else if (tmpStock.closeprice > tmpStock.reference) colorStyle = 1;
                            underlying = {
                                id: tmpStock.stockid,
                                symbol: tmpSymbol,
                                price: { val: tmpStock.closeprice !== null ? parseInt(tmpStock.closeprice) / 1000 : '', change: 0, time: 0 },
                                color: colorStyle,
                                buy_1: tmpStock.bidpricE1 !== null ? parseInt(tmpStock.bidpricE1) / 1000 : 0,
                                buy_2: tmpStock.bidpricE2 !== null ? parseInt(tmpStock.bidpricE2) / 1000 : 0,
                                buy_3: tmpStock.bidpricE3 !== null ? parseInt(tmpStock.bidpricE3) / 1000 : 0,
                                sell_1: tmpStock.offerpricE1 !== null ? parseInt(tmpStock.offerpricE1) / 1000 : 0,
                                sell_2: tmpStock.offerpricE2 !== null ? parseInt(tmpStock.offerpricE2) / 1000 : 0,
                                sell_3: tmpStock.offerpricE3 !== null ? parseInt(tmpStock.offerpricE3) / 1000 : 0
                            };
                            // underlying = {
                            //     id: tmpStock.stockid,
                            //     symbol: tmpSymbol,
                            //     price: { val: '', change: 0, time: 0 },
                            //     color: 0,
                            //     buy_1: 0,
                            //     buy_2: 0,
                            //     buy_3: 0,
                            //     sell_1: 0,
                            //     sell_2: 0,
                            //     sell_3: 0
                            // };
                            hoseStockData[underlying.id].covered_warrant_id.push(el.stockid);
                        }
                    } else el.stocktype = 'S';
                    hoseStockData[el.stockid] = basisStockData[el.stockid] = {
                        id: el.stockid, symbol: el.symbol, company: el.fullname, ceiling: parseInt(el.ceiling) / 1000,
                        floor: parseInt(el.floor) / 1000, reference: parseInt(el.reference) / 1000,
                        buy_3: { val: el.bidpricE3 !== null ? parseInt(el.bidpricE3) / 1000 : '', change: 0, time: 0 },
                        buy_qtty_3: {
                            val: el.bidvoL3 !== null ? (Number(el.bidvoL3) / 1000).toLocaleString('en') : '', intVal: parseInt(el.bidvoL3), change: 0, time: 0
                        },
                        buy_2: { val: el.bidpricE2 !== null ? parseInt(el.bidpricE2) / 1000 : '', change: 0, time: 0 },
                        buy_qtty_2: {
                            val: el.bidvoL2 !== null ? (Number(el.bidvoL2) / 1000).toLocaleString('en') : '', intVal: parseInt(el.bidvoL2), change: 0, time: 0
                        },
                        buy_1: { val: el.bidpricE1 !== null ? parseInt(el.bidpricE1) / 1000 : '', change: 0, time: 0 },
                        buy_qtty_1: {
                            val: el.bidvoL1 !== null ? (Number(el.bidvoL1) / 1000).toLocaleString('en') : '', intVal: parseInt(el.bidvoL1), change: 0, time: 0
                        },
                        match_price: { val: el.closeprice !== null ? parseInt(el.closeprice) / 1000 : '', change: 0, time: 0 },
                        match_qtty: {
                            val: el.closevol !== null ? (Number(el.closevol) / 1000).toLocaleString('en') : '', intVal: parseInt(el.closevol), change: 0, time: 0
                        },
                        difference: el.change !== null && el.closeprice !== el.reference ? parseInt(el.change) / 1000 : '',
                        sell_1: { val: el.offerpricE1 !== null ? parseInt(el.offerpricE1) / 1000 : '', change: 0, time: 0 },
                        sell_qtty_1: {
                            val: el.offervoL1 !== null ? (Number(el.offervoL1) / 1000).toLocaleString('en') : '', intVal: parseInt(el.offervoL1), change: 0, time: 0
                        },
                        sell_2: { val: el.offerpricE2 !== null ? parseInt(el.offerpricE2) / 1000 : '', change: 0, time: 0 },
                        sell_qtty_2: {
                            val: el.offervoL2 !== null ? (Number(el.offervoL2) / 1000).toLocaleString('en') : '', intVal: parseInt(el.offervoL2), change: 0, time: 0
                        },
                        sell_3: { val: el.offerpricE3 !== null ? parseInt(el.offerpricE3) / 1000 : '', change: 0, time: 0 },
                        sell_qtty_3: {
                            val: el.offervoL3 !== null ? (Number(el.offervoL3) / 1000).toLocaleString('en') : '', intVal: parseInt(el.offervoL3), change: 0, time: 0
                        },
                        total_vol: {
                            val: el.totaltrading === 0 ? '' : (Number((el.totaltrading / 1000).toFixed(1))).toLocaleString('en'), intVal: el.totaltrading, change: 0, time: 0
                        },
                        total_value: {
                            val: Math.round(Number(el.totaltradingvalue) / 1000000).toLocaleString('en') + ' tr', intVal: Number(el.totaltradingvalue), change: 0, time: 0
                        },
                        open_price: el.open !== null ? parseInt(el.open) / 1000 : '',
                        high_price: { val: el.high !== null ? parseInt(el.high) / 1000 : '', change: 0, time: 0 },
                        low_price: { val: el.low !== null ? parseInt(el.low) / 1000 : '', change: 0, time: 0 },
                        nn_buy: {
                            val: el.foreignbuy === 0 ? '' : (Number((el.foreignbuy / 1000).toFixed(1))).toLocaleString('en'), change: 0, time: 0
                        },
                        nn_sell: {
                            val: el.foreignsell === 0 ? '' : (Number((el.foreignsell / 1000).toFixed(1))).toLocaleString('en'), change: 0, time: 0
                        },
                        nn_room: {
                            val: (Number((el.foreignremain / 1000).toFixed(1))).toLocaleString('en'), change: 0, time: 0
                        },
                        trading_session: process.env.HOSE_SESSION, stock_type: el.stocktype,
                        issuer_name: el.issuername === null ? '' : (el.issuername.trim() === 'VNDIRECT' ? 'VNDS' : el.issuername.trim()),
                        maturity_date: el.maturitydate === null ? '' : stringHelper.convertDate(el.maturitydate, 'yyyyMMdd', '/'),
                        last_trading_date: el.lasttradingdate === null ? '' : stringHelper.convertDate(el.lasttradingdate, 'yyyyMMdd', '/'),
                        exercise_price: el.exerciseprice === null || el.exerciseprice === '0' ? '' : (Number(el.exerciseprice) / 1000).toFixed(2),
                        exercise_ratio: el.exerciseratio === null ? '' : el.exerciseratio.trim(),
                        underlying: underlying,
                        covered_warrant_id: [],
                        market: 'hose', hasChange: 0
                    };

                    // hoseStockData[el.stockid] = basisStockData[el.stockid] = {
                    //     id: el.stockid, symbol: el.symbol, company: el.fullname, ceiling: parseInt(el.ceiling) / 1000,
                    //     floor: parseInt(el.floor) / 1000, reference: parseInt(el.reference) / 1000,
                    //     buy_3: { val: '', change: 0, time: 0 },
                    //     buy_qtty_3: {
                    //         val: '', intVal: 0, change: 0, time: 0
                    //     },
                    //     buy_2: { val: '', change: 0, time: 0 },
                    //     buy_qtty_2: {
                    //         val: '', intVal: 0, change: 0, time: 0
                    //     },
                    //     buy_1: { val: '', change: 0, time: 0 },
                    //     buy_qtty_1: {
                    //         val: '', intVal: 0, change: 0, time: 0
                    //     },
                    //     match_price: { val: '', change: 0, time: 0 },
                    //     match_qtty: {
                    //         val: '', intVal: 0, change: 0, time: 0
                    //     },
                    //     difference: '',
                    //     sell_1: { val: '', change: 0, time: 0 },
                    //     sell_qtty_1: {
                    //         val: '', intVal: 0, change: 0, time: 0
                    //     },
                    //     sell_2: { val: '', change: 0, time: 0 },
                    //     sell_qtty_2: {
                    //         val: '', intVal: 0, change: 0, time: 0
                    //     },
                    //     sell_3: { val: '', change: 0, time: 0 },
                    //     sell_qtty_3: {
                    //         val: '', intVal: 0, change: 0, time: 0
                    //     },
                    //     total_vol: {
                    //         val: '', intVal: 0, change: 0, time: 0
                    //     },
                    //     total_value: {
                    //         val: '', intVal: 0, change: 0, time: 0
                    //     },
                    //     open_price: '',
                    //     high_price: { val: '', change: 0, time: 0 },
                    //     low_price: { val: '', change: 0, time: 0 },
                    //     nn_buy: {
                    //         val: '', change: 0, time: 0
                    //     },
                    //     nn_sell: {
                    //         val: '', change: 0, time: 0
                    //     },
                    //     nn_room: {
                    //         val: '', change: 0, time: 0
                    //     },
                    //     trading_session: process.env.HOSE_SESSION, stock_type: el.stocktype,
                    //     issuer_name: el.exerciseratio === null ? '' : (el.exerciseratio.trim() === 'VNDIRECT' ? 'VNDS' : el.exerciseratio.trim()),
                    //     maturity_date: el.maturitydate === null ? '' : stringHelper.convertDate(el.maturitydate, 'yyyyMMdd', '/'),
                    //     last_trading_date: el.lasttradingdate === null ? '' : stringHelper.convertDate(el.lasttradingdate, 'yyyyMMdd', '/'),
                    //     exercise_price: el.openinterest === null || el.openinterest === '0' ? '' : (parseInt(el.openinterest) / 10000).toFixed(2),
                    //     exercise_ratio: el.openinterestchange === null ? '' : el.openinterestchange.trim(),
                    //     underlying: underlying,
                    //     covered_warrant_id: [],
                    //     market: 'hose', hasChange: 0
                    // };
                }
            });
            // hoseStockData['94'] = basisStockData['94'] = {
            //     id: '94', symbol: 'CREE3333', company: 'CREE3333', ceiling: 7.8,
            //     floor: 0.01, reference: 3.5,
            //     buy_3: { val: '', change: 0, time: 0 },
            //     buy_qtty_3: {
            //         val: '', intVal: 0, change: 0, time: 0
            //     },
            //     buy_2: { val: '', change: 0, time: 0 },
            //     buy_qtty_2: {
            //         val: '', intVal: 0, change: 0, time: 0
            //     },
            //     buy_1: { val: '', change: 0, time: 0 },
            //     buy_qtty_1: {
            //         val: '', intVal: 0, change: 0, time: 0
            //     },
            //     match_price: { val: '', change: 0, time: 0 },
            //     match_qtty: {
            //         val: '', intVal: 0, change: 0, time: 0
            //     },
            //     difference: '',
            //     sell_1: { val: '', change: 0, time: 0 },
            //     sell_qtty_1: {
            //         val: '', intVal: 0, change: 0, time: 0
            //     },
            //     sell_2: { val: '', change: 0, time: 0 },
            //     sell_qtty_2: {
            //         val: '', intVal: 0, change: 0, time: 0
            //     },
            //     sell_3: { val: '', change: 0, time: 0 },
            //     sell_qtty_3: {
            //         val: '', intVal: 0, change: 0, time: 0
            //     },
            //     total_vol: {
            //         val: '', intVal: 0, change: 0, time: 0
            //     },
            //     total_value: {
            //         val: '', intVal: 0, change: 0, time: 0
            //     },
            //     open_price: '',
            //     high_price: { val: '', change: 0, time: 0 },
            //     low_price: { val: '', change: 0, time: 0 },
            //     nn_buy: {
            //         val: '', change: 0, time: 0
            //     },
            //     nn_sell: {
            //         val: '', change: 0, time: 0
            //     },
            //     nn_room: {
            //         val: '', change: 0, time: 0
            //     },
            //     trading_session: process.env.HOSE_SESSION, stock_type: 'W',
            //     issuer_name: 'BSC',
            //     maturity_date: '12/01/2021',
            //     last_trading_date: '10/01/2021',
            //     exercise_price: '60.00',
            //     exercise_ratio: '1.0000:1',
            //     underlying: {
            //         id: '2735', symbol: 'REE', price: {
            //             val: '',
            //             change: 0, time: 0
            //         }, color: 0,
            //         buy_1: 0, buy_2: 0,
            //         buy_3: 0, sell_1:0,
            //         sell_2: 0, sell_3: 0
            //     },
            //     covered_warrant_id: [],
            //     market: 'hose', hasChange: 0
            // };
            // hoseStockData['2735'].ceiling = basisStockData['2735'].ceiling = 66.3;
            // hoseStockData['2735'].floor = basisStockData['2735'].floor = 57.7;
            // hoseStockData['2735'].reference = basisStockData['2735'].reference = 62;
            // hoseStockData['2735'].covered_warrant_id = basisStockData['2735'].covered_warrant_id = ['94'];
            // hoseStockData['2735'].match_price.val = basisStockData['2735'].match_price.val = 62;

            // hoseStockData['1354'].ceiling = basisStockData['1354'].ceiling = 58.8;
            // hoseStockData['1354'].floor = basisStockData['1354'].floor = 51.2;
            // hoseStockData['1354'].reference = basisStockData['1354'].reference = 55;

            // hoseStockData['217'].ceiling = basisStockData['217'].ceiling = 45.5;
            // hoseStockData['217'].floor = basisStockData['217'].floor = 39.6;
            // hoseStockData['217'].reference = basisStockData['217'].reference = 42.55;

            // hoseStockData['580'].ceiling = basisStockData['580'].ceiling = 29.05;
            // hoseStockData['580'].floor = basisStockData['580'].floor = 25.25;
            // hoseStockData['580'].reference = basisStockData['580'].reference = 27.15;

            basis.loadHistoryData(hoseStockData, hnxStockData, lastPriceStock);
        }
    })
}

function initFutureData() {
    rp({
        method: 'GET',
        url: process.env.API_URL + '/MarketData/GetCurrentStockPSInfor'
    }).then(res => {
        if (res.s === 'ok') {
            totalCountPS = res.d.length;
            res.d.forEach(el => {
                // Init match hist data
                initMatchHistPS(el.symbol);
                // Get TopPrice
                var buy1 = 0, buy2 = 0, buy3 = 0, buy4 = 0, buy5 = 0, buy6 = 0, buy7 = 0, buy8 = 0, buy9 = 0, buy10 = 0,
                    sell1 = 0, sell2 = 0, sell3 = 0, sell4 = 0, sell5 = 0, sell6 = 0, sell7 = 0, sell8 = 0, sell9 = 0, sell10 = 0,
                    buyqtty1 = 0, buyqtty2 = 0, buyqtty3 = 0, buyqtty4 = 0, buyqtty5 = 0, buyqtty6 = 0, buyqtty7 = 0, buyqtty8 = 0, buyqtty9 = 0, buyqtty10 = 0,
                    sellqtty1 = 0, sellqtty2 = 0, sellqtty3 = 0, sellqtty4 = 0, sellqtty5 = 0, sellqtty6 = 0, sellqtty7 = 0, sellqtty8 = 0, sellqtty9 = 0, sellqtty10 = 0
                var underlying = 'VN30';
                if (el.symbol.indexOf('GB05') === 0) underlying = 'VGB5';
                if (el.symbol.indexOf('GB10') === 0) underlying = 'VGB10';
                rp({
                    method: 'POST',
                    url: process.env.API_URL + '/MarketData/GetTopPricePS',
                    form: { symbol: el.symbol }
                }).then(res2 => {
                    if (res2.s === 'ok') {
                        if (res2.d[0] !== undefined) {
                            buy1 = res2.d[0].buyprice === 'ATO' || res2.d[0].buyprice === 'ATC' || res2.d[0].buyprice === null
                                ? 0 : parseFloat(res2.d[0].buyprice)
                            buyqtty1 = res2.d[0].buyquantity === null ? 0 : parseInt(res2.d[0].buyquantity, 10)
                            sell1 = res2.d[0].sellprice === 'ATO' || res2.d[0].sellprice === 'ATC' || res2.d[0].sellprice === null
                                ? 0 : parseFloat(res2.d[0].sellprice)
                            sellqtty1 = res2.d[0].sellquantity === null ? 0 : parseInt(res2.d[0].sellquantity, 10)
                        }
                        if (res2.d[1] !== undefined) {
                            buy2 = parseFloat(res2.d[1].buyprice)
                            buyqtty2 = parseInt(res2.d[1].buyquantity, 10)
                            sell2 = parseFloat(res2.d[1].sellprice)
                            sellqtty2 = parseInt(res2.d[1].sellquantity, 10)
                        }
                        if (res2.d[2] !== undefined) {
                            buy3 = parseFloat(res2.d[2].buyprice)
                            buyqtty3 = parseInt(res2.d[2].buyquantity, 10)
                            sell3 = parseFloat(res2.d[2].sellprice)
                            sellqtty3 = parseInt(res2.d[2].sellquantity, 10)
                        }
                        if (res2.d[3] !== undefined) {
                            buy4 = parseFloat(res2.d[3].buyprice)
                            buyqtty4 = parseInt(res2.d[3].buyquantity, 10)
                            sell4 = parseFloat(res2.d[3].sellprice)
                            sellqtty4 = parseInt(res2.d[3].sellquantity, 10)
                        }
                        if (res2.d[4] !== undefined) {
                            buy5 = parseFloat(res2.d[4].buyprice)
                            buyqtty5 = parseInt(res2.d[4].buyquantity, 10)
                            sell5 = parseFloat(res2.d[4].sellprice)
                            sellqtty5 = parseInt(res2.d[4].sellquantity, 10)
                        }
                        if (res2.d[5] !== undefined) {
                            buy6 = parseFloat(res2.d[5].buyprice)
                            buyqtty6 = parseInt(res2.d[5].buyquantity, 10)
                            sell6 = parseFloat(res2.d[5].sellprice)
                            sellqtty6 = parseInt(res2.d[5].sellquantity, 10)
                        }
                        if (res2.d[6] !== undefined) {
                            buy7 = parseFloat(res2.d[6].buyprice)
                            buyqtty7 = parseInt(res2.d[6].buyquantity, 10)
                            sell7 = parseFloat(res2.d[6].sellprice)
                            sellqtty7 = parseInt(res2.d[6].sellquantity, 10)
                        }
                        if (res2.d[7] !== undefined) {
                            buy8 = parseFloat(res2.d[7].buyprice)
                            buyqtty8 = parseInt(res2.d[7].buyquantity, 10)
                            sell8 = parseFloat(res2.d[7].sellprice)
                            sellqtty8 = parseInt(res2.d[7].sellquantity, 10)
                        }
                        if (res2.d[8] !== undefined) {
                            buy9 = parseFloat(res2.d[8].buyprice)
                            buyqtty9 = parseInt(res2.d[8].buyquantity, 10)
                            sell9 = parseFloat(res2.d[8].sellprice)
                            sellqtty9 = parseInt(res2.d[8].sellquantity, 10)
                        }
                        if (res2.d[9] !== undefined) {
                            buy10 = parseFloat(res2.d[9].buyprice)
                            buyqtty10 = parseInt(res2.d[9].buyquantity, 10)
                            sell10 = parseFloat(res2.d[9].sellprice)
                            sellqtty10 = parseInt(res2.d[9].sellquantity, 10)
                        }

                        futureStockData[el.symbol] = {
                            symbol: el.symbol, maturity_date: el.lasttradingdate, ceiling: { val: parseFloat(el.ceiling), yield: 0 },
                            floor: { val: parseFloat(el.floor), yield: 0 },
                            reference: { val: parseFloat(el.reference), yield: 0 }, open_interest: parseInt(el.openinterest, 10),
                            buy_3: { val: buy3, yield: 0, change: 0, time: 0 }, buy_qtty_3: { val: buyqtty3, change: 0, time: 0 },
                            buy_2: { val: buy2, yield: 0, change: 0, time: 0 }, buy_qtty_2: { val: buyqtty2, change: 0, time: 0 },
                            buy_1: { val: buy1, yield: 0, change: 0, time: 0 }, buy_qtty_1: { val: buyqtty1, change: 0, time: 0 },
                            buy_4: { val: buy4, yield: 0, change: 0, time: 0 }, buy_qtty_4: { val: buyqtty4, change: 0, time: 0 },
                            buy_5: { val: buy5, yield: 0, change: 0, time: 0 }, buy_qtty_5: { val: buyqtty5, change: 0, time: 0 },
                            buy_6: { val: buy6, yield: 0, change: 0, time: 0 }, buy_qtty_6: { val: buyqtty6, change: 0, time: 0 },
                            buy_7: { val: buy7, yield: 0, change: 0, time: 0 }, buy_qtty_7: { val: buyqtty7, change: 0, time: 0 },
                            buy_8: { val: buy8, yield: 0, change: 0, time: 0 }, buy_qtty_8: { val: buyqtty8, change: 0, time: 0 },
                            buy_9: { val: buy9, yield: 0, change: 0, time: 0 }, buy_qtty_9: { val: buyqtty9, change: 0, time: 0 },
                            buy_10: { val: buy10, yield: 0, change: 0, time: 0 }, buy_qtty_10: { val: buyqtty10, change: 0, time: 0 },
                            match_price: { val: el.closeprice === null ? 0 : parseFloat(el.closeprice), yield: 0, change: 0, time: 0 },
                            match_qtty: { val: el.closevol === null ? 0 : parseInt(el.closevol, 10), change: 0, time: 0 },
                            difference: el.change === null ? 0 : parseFloat(el.change),
                            sell_1: { val: sell1, yield: 0, change: 0, time: 0 }, sell_qtty_1: { val: sellqtty1, change: 0, time: 0 },
                            sell_2: { val: sell2, yield: 0, change: 0, time: 0 }, sell_qtty_2: { val: sellqtty2, change: 0, time: 0 },
                            sell_3: { val: sell3, yield: 0, change: 0, time: 0 }, sell_qtty_3: { val: sellqtty3, change: 0, time: 0 },
                            sell_4: { val: sell4, yield: 0, change: 0, time: 0 }, sell_qtty_4: { val: sellqtty4, change: 0, time: 0 },
                            sell_5: { val: sell5, yield: 0, change: 0, time: 0 }, sell_qtty_5: { val: sellqtty5, change: 0, time: 0 },
                            sell_6: { val: sell6, yield: 0, change: 0, time: 0 }, sell_qtty_6: { val: sellqtty6, change: 0, time: 0 },
                            sell_7: { val: sell7, yield: 0, change: 0, time: 0 }, sell_qtty_7: { val: sellqtty7, change: 0, time: 0 },
                            sell_8: { val: sell8, yield: 0, change: 0, time: 0 }, sell_qtty_8: { val: sellqtty8, change: 0, time: 0 },
                            sell_9: { val: sell9, yield: 0, change: 0, time: 0 }, sell_qtty_9: { val: sellqtty9, change: 0, time: 0 },
                            sell_10: { val: sell10, yield: 0, change: 0, time: 0 }, sell_qtty_10: { val: sellqtty10, change: 0, time: 0 },
                            total_vol: { val: el.totaltrading === null ? 0 : parseInt(el.totaltrading, 10), change: 0, time: 0 },
                            total_value: { val: Math.round(Number(el.totaltradingvalue) / 1000000).toLocaleString('en'), intVal: el.totaltradingvalue === null ? 0 : Number(el.totaltradingvalue), change: 0, time: 0 },
                            open_price: { val: el.open === null ? 0 : parseFloat(el.open), yield: 0 },
                            high_price: { val: el.high === null ? 0 : parseFloat(el.high), yield: 0, change: 0, time: 0 },
                            low_price: { val: el.low === null ? 0 : parseFloat(el.low), yield: 0, change: 0, time: 0 }, nn_buy: { val: parseInt(el.foreignbuy, 10), change: 0, time: 0 },
                            nn_sell: { val: parseInt(el.foreignsell, 10), change: 0, time: 0 }, underlying: underlying, market: 'deri', trading_session: '', stock_type: ''
                        }
                        if (underlying === 'VGB5' || underlying === 'VGB10') {
                            futureStockData[el.symbol].ceiling.yield = stockHelper.bondYield(underlying, futureStockData[el.symbol].ceiling.val);
                            futureStockData[el.symbol].floor.yield = stockHelper.bondYield(underlying, futureStockData[el.symbol].floor.val);
                            futureStockData[el.symbol].reference.yield = stockHelper.bondYield(underlying, futureStockData[el.symbol].reference.val);
                            futureStockData[el.symbol].buy_10.yield = stockHelper.bondYield(underlying, futureStockData[el.symbol].buy_10.val);
                            futureStockData[el.symbol].buy_9.yield = stockHelper.bondYield(underlying, futureStockData[el.symbol].buy_9.val);
                            futureStockData[el.symbol].buy_8.yield = stockHelper.bondYield(underlying, futureStockData[el.symbol].buy_8.val);
                            futureStockData[el.symbol].buy_7.yield = stockHelper.bondYield(underlying, futureStockData[el.symbol].buy_7.val);
                            futureStockData[el.symbol].buy_6.yield = stockHelper.bondYield(underlying, futureStockData[el.symbol].buy_6.val);
                            futureStockData[el.symbol].buy_5.yield = stockHelper.bondYield(underlying, futureStockData[el.symbol].buy_5.val);
                            futureStockData[el.symbol].buy_4.yield = stockHelper.bondYield(underlying, futureStockData[el.symbol].buy_4.val);
                            futureStockData[el.symbol].buy_3.yield = stockHelper.bondYield(underlying, futureStockData[el.symbol].buy_3.val);
                            futureStockData[el.symbol].buy_2.yield = stockHelper.bondYield(underlying, futureStockData[el.symbol].buy_2.val);
                            futureStockData[el.symbol].buy_1.yield = stockHelper.bondYield(underlying, futureStockData[el.symbol].buy_1.val);
                            futureStockData[el.symbol].match_price.yield = stockHelper.bondYield(underlying, futureStockData[el.symbol].match_price.val);
                            futureStockData[el.symbol].sell_1.yield = stockHelper.bondYield(underlying, futureStockData[el.symbol].sell_1.val);
                            futureStockData[el.symbol].sell_2.yield = stockHelper.bondYield(underlying, futureStockData[el.symbol].sell_2.val);
                            futureStockData[el.symbol].sell_3.yield = stockHelper.bondYield(underlying, futureStockData[el.symbol].sell_3.val);
                            futureStockData[el.symbol].sell_4.yield = stockHelper.bondYield(underlying, futureStockData[el.symbol].sell_4.val);
                            futureStockData[el.symbol].sell_5.yield = stockHelper.bondYield(underlying, futureStockData[el.symbol].sell_5.val);
                            futureStockData[el.symbol].sell_6.yield = stockHelper.bondYield(underlying, futureStockData[el.symbol].sell_6.val);
                            futureStockData[el.symbol].sell_7.yield = stockHelper.bondYield(underlying, futureStockData[el.symbol].sell_7.val);
                            futureStockData[el.symbol].sell_8.yield = stockHelper.bondYield(underlying, futureStockData[el.symbol].sell_8.val);
                            futureStockData[el.symbol].sell_9.yield = stockHelper.bondYield(underlying, futureStockData[el.symbol].sell_9.val);
                            futureStockData[el.symbol].sell_10.yield = stockHelper.bondYield(underlying, futureStockData[el.symbol].sell_10.val);
                            futureStockData[el.symbol].open_price.yield = stockHelper.bondYield(underlying, futureStockData[el.symbol].open_price.val);
                            futureStockData[el.symbol].high_price.yield = stockHelper.bondYield(underlying, futureStockData[el.symbol].high_price.val);
                            futureStockData[el.symbol].low_price.yield = stockHelper.bondYield(underlying, futureStockData[el.symbol].low_price.val);
                        }
                        future.loadHistoryData(futureStockData, lastPriceStock);
                    }
                })
            });
        }
    })
}

function initMatchHistCS(symbol, timeout) {
    // setTimeout(() => {
    //     rp({
    //         method: 'POST',
    //         url: process.env.API_URL + '/MarketData/GetPriceOneSymbolIntraday',
    //         form: {
    //             symbol: symbol,
    //             id: 0
    //         }
    //     }).then(res => {
    //         countMatchHistCSRequest++;
    //         if (res.s === 'ok') {
    //             matchHistCSData[symbol] = res.d.map(el => {
    //                 return {
    //                     time: stringHelper.getTimeByDateTime(new Date(el.permdate)),
    //                     price: Number(el.price) / 1000,
    //                     volume: Number(el.volume),
    // change_value: Number(el.changevalue)/1000,
    //                     total_vol: Number(el.totalvolume)
    //                 };
    //             });
    //             // console.log(symbol + " done " + countMatchHistCSRequest + "/" + totalCountCS);
    //         }
    //         if(countMatchHistCSRequest === totalCountCS){
    //             basis.loadMatchHistData(matchHistCSData);
    //             console.log('load match hist done.')
    //         }
    //     }).catch(ex => {
    //         countMatchHistCSRequest++;
    //         if(countMatchHistCSRequest === totalCountCS){
    //             basis.loadMatchHistData(matchHistCSData);
    //             console.log('load match hist done.')
    //         }
    //     });
    // }, timeout);
}

function initMatchHistPS(symbol) {
    // var fromTime = new Date().setHours(8, 45, 0),
    //     toTime = new Date().setHours(15, 0, 0);
    // rp({
    //     method: 'POST',
    //     url: process.env.API_URL + '/MarketData/GetPricePhaiSinhHistoryMatch',
    //     form: {
    //         symbol: symbol,
    //         fromtime: fromTime,
    //         totime: toTime
    //     }
    // }).then(res => {
    //     countMatchHistPSRequest++;
    //     if (res.s === 'ok') {
    //         matchHistPSData[symbol] = res.d.map(el => {
    //             return {
    //                 time: stringHelper.getTimeByDateTime(new Date(el.time)),
    //                 price: Number(el.price),
    //                 volume: Number(el.volume),
    // change_value: Number(el.changevalue),
    //                 total_vol: Number(el.totalvolume)
    //             };
    //         });
    //     }
    //     if(countMatchHistPSRequest === totalCountPS){
    //         future.loadMatchHistData(matchHistPSData);
    //     }
    // }).catch(ex => {
    //     countMatchHistPSRequest++;
    //     if(countMatchHistPSRequest === totalCountPS){
    //         future.loadMatchHistData(matchHistPSData);
    //     }
    // });
}

module.exports = {
    basisStockData,
    futureStockData,
    initFutureData,
    initStockData
}