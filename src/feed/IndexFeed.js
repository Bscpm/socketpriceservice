var redis = require('../database/redis');
const dotenv = require('dotenv');
dotenv.config();
const rp = require('request-promise').defaults({ json: true });
const logger = require('../config/logger');
const errorLogger = logger.errorLogger;
var JsonOptimize = require('json-optimize'), jo = new JsonOptimize();

// Cac moc thoi gian trong ngay
var time8h = 0, time8h05 = 0, time8h45 = 0, time11h35 = 0, time12h55 = 0, time15h05 = 0,
    time9h = 0, time9h15 = 0, time11h30 = 0, time12h59,
    time13h = 0, time14h30 = 0, time14h45 = 0, time15h = 0, time23h59 = 0;
// Moc thoi gian cuoi ngay cua ngay hien tai
var lastTime = 0;
// Luu lai Index data
var VNIndexData = {}, VN30Data = {}, VNXAllData = {},
    HNXData = {}, HNX30Data = {}, UPCOMData = {};

var lastPriceIndex = {};
redis.commonDb.get("StockBoard:LastPriceIndex", function (err, reply) {
    if (err) {
        errorLogger.info('read [LastPriceIndex] error: ' + ex.message);
        return;
    }
    if (reply !== null) {
        lastPriceIndex = JSON.parse(reply);
    }
});

function getMarketStatusHOSE(tmpTimes) {
    // var tmpTimes = Date.parse(datetime);
    if (tmpTimes >= time15h) return 'Đóng cửa';
    else if (tmpTimes >= time14h45) return 'Thỏa thuận';
    else if (tmpTimes >= time14h30) return 'ATC';
    else if (tmpTimes >= time13h) return 'KL liên tục';
    else if (tmpTimes >= time11h30) return 'Tạm dừng';
    else if (tmpTimes >= time9h15) return 'KL liên tục';
    else if (tmpTimes >= time9h) return 'ATO';
}
function getMarketStatusHNX(tmpTimes) {
    // var tmpTimes = Date.parse(datetime);
    if (tmpTimes >= time15h) return 'Đóng cửa';
    else if (tmpTimes >= time14h45) return 'Thỏa thuận';
    else if (tmpTimes >= time14h30) return 'ATC';
    else if (tmpTimes >= time13h) return 'KL liên tục';
    else if (tmpTimes >= time11h30) return 'Tạm dừng';
    else if (tmpTimes >= time9h) return 'KL liên tục';
}
function getMarketStatusUPCOM(tmpTimes) {
    // var tmpTimes = Date.parse(datetime);
    if (tmpTimes >= time15h) return 'Đóng cửa';
    else if (tmpTimes >= time13h) return 'KL liên tục';
    else if (tmpTimes >= time11h30) return 'Tạm dừng';
    else if (tmpTimes >= time9h) return 'KL liên tục';
}

// init INDEX data
function initIndexData(io, clientId) {
    
    var date = new Date();
    if (date.getTime() > lastTime && date.getDay() !== 0 && date.getDay() !== 6) {
        // Thiet lap cac moc thoi gian
        time8h = new Date().setHours(8, 0, 0);
        time8h45 = new Date().setHours(8, 45, 0);
        lastTime = new Date().setHours(8, 0, 0) + 86400000;
        time11h35 = new Date().setHours(11, 35, 0);
        time12h55 = new Date().setHours(12, 55, 0);
        time15h05 = new Date().setHours(15, 5, 0);
        time23h59 = new Date().setHours(23, 59, 59);
        time9h = parseInt(new Date().setHours(9, 0, 0) / 1000) * 1000;
        time9h15 = parseInt(new Date().setHours(9, 15, 0) / 1000) * 1000;
        time11h30 = parseInt(new Date().setHours(11, 29, 0) / 1000) * 1000;
        time13h = parseInt(new Date().setHours(13, 0, 0) / 1000) * 1000;
        time14h30 = parseInt(new Date().setHours(14, 30, 0) / 1000) * 1000;
        time14h45 = parseInt(new Date().setHours(14, 45, 0) / 1000) * 1000;
        time15h = parseInt(new Date().setHours(15, 0, 0) / 1000) * 1000;
        time12h59 = parseInt(new Date().setHours(12, 59, 0) / 1000) * 1000;
        if (date.getTime() > time8h && date.getTime() < time8h45) {
            lastPriceIndex["vnindex"][0] = lastPriceIndex["vnindex"][1];
            lastPriceIndex["vn30"][0] = lastPriceIndex["vn30"][1];
            lastPriceIndex["xall"][0] = lastPriceIndex["xall"][1];
            lastPriceIndex["hnx"][0] = lastPriceIndex["hnx"][1];
            lastPriceIndex["hnx30"][0] = lastPriceIndex["hnx30"][1];
            lastPriceIndex["upcom"][0] = lastPriceIndex["upcom"][1];
        }
        console.log('init');
        rp({
            method: 'POST',
            url: process.env.API_URL + '/MarketData/GetAllIndexIntraday2',
            qs: {
                idhose: 0,
                idvn30: 0,
                idvnallshare: 0,
                idhnx: 0,
                idhnx30: 0,
                idupcom: 0
            }
        }).then(res => {
            console.log( process.env.API_URL );
            //console.log(res.hose);
            if (res.s === 'ok') {
                if (res.hose !== undefined) {
                    VNIndexData.priceBasic = lastPriceIndex["vnindex"][0], VNIndexData.data = {}, VNIndexData.lastId = 0;
                    // if (res.hose.d1.length && Number(res.hose.d1[0].indexbasic) > 0)
                    //     VNIndexData.priceBasic = Number(res.hose.d1[0].indexbasic);
                    if (res.hose.d.length) {
                        VNIndexData.lastId = res.hose.d[res.hose.d.length - 1].id;
                        // VNIndexData.marketStatus = getMarketStatusHOSE(res.hose.d[res.hose.d.length - 1].permdate);
                        VNIndexData.marketStatus = getMarketStatusHOSE(date.getTime());
                    }
                    res.hose.d.forEach((el, i) => {
                        el.permdate = Math.floor((Date.parse(el.permdate) / 1000) / 60) * 60000;
                        el.price = Number(el.price);
                        el.indexchange = Number(el.indexchange);
                        el.volume = parseInt(el.volume);
                        if (el.permdate > time9h)
                            VNIndexData.data[el.permdate] = el;
                        if (VNIndexData.data[time12h59] === undefined && el.permdate === time13h)
                            VNIndexData.data[time12h59] = el;
                    });
                }
                if (res.vN30 !== undefined) {
                    VN30Data.priceBasic = lastPriceIndex["vn30"][0], VN30Data.data = {}, VN30Data.lastId = 0;
                    // if (res.vN30.d1.length && Number(res.vN30.d1[0].indexbasic) > 0)
                    //     VN30Data.priceBasic = Number(res.vN30.d1[0].indexbasic);
                    if (res.vN30.d.length) {
                        VN30Data.lastId = res.vN30.d[res.vN30.d.length - 1].id;
                    }
                    res.vN30.d.forEach((el) => {
                        el.permdate = Math.floor((Date.parse(el.permdate) / 1000) / 60) * 60000;
                        el.price = Number(el.price);
                        el.indexchange = Number(el.indexchange);
                        el.volume = parseInt(el.volume);
                        if (el.permdate > time9h)
                            VN30Data.data[el.permdate] = el;
                        if (VN30Data.data[time12h59] === undefined && el.permdate === time13h)
                            VN30Data.data[time12h59] = el;
                    });
                }
                if (res.xall !== undefined) {
                    VNXAllData.priceBasic = lastPriceIndex["xall"][0], VNXAllData.data = {}, VNXAllData.lastId = 0;
                    // if (res.xall.d1.length && Number(res.xall.d1[0].indexbasic) > 0)
                    //     VNXAllData.priceBasic = Number(res.xall.d1[0].indexbasic);
                    if (res.xall.d.length) {
                        VNXAllData.lastId = res.xall.d[res.xall.d.length - 1].id;
                    }
                    res.xall.d.forEach((el, i) => {
                        el.permdate = Math.floor((Date.parse(el.permdate) / 1000) / 60) * 60000;
                        el.price = Number(el.price);
                        el.indexchange = Number(el.indexchange);
                        el.volume = parseInt(el.volume);
                        if (el.permdate > time9h)
                            VNXAllData.data[el.permdate] = el;
                        if (VNXAllData.data[time12h59] === undefined && el.permdate === time13h)
                            VNXAllData.data[time12h59] = el;
                    });
                }
                if (res.hnx !== undefined) {
                    HNXData.priceBasic = lastPriceIndex["hnx"][0], HNXData.data = {}, HNXData.lastId = 0;
                    // if (res.hnx.d1.length && Number(res.hnx.d1[0].indexbasic) > 0)
                    //     HNXData.priceBasic = Number(res.hnx.d1[0].indexbasic);
                    if (res.hnx.d.length) {
                        HNXData.lastId = res.hnx.d[res.hnx.d.length - 1].id;
                        // HNXData.marketStatus = getMarketStatusHNX(res.hnx.d[res.hnx.d.length - 1].permdate);
                        HNXData.marketStatus = getMarketStatusHNX(date.getTime());
                    }
                    res.hnx.d.forEach((el, i) => {
                        el.permdate = Math.floor((Date.parse(el.permdate) / 1000) / 60) * 60000;
                        el.price = Number(el.price);
                        el.indexchange = Number(el.indexchange);
                        el.volume = parseInt(el.volume);
                        if (el.permdate > time9h)
                            HNXData.data[el.permdate] = el;
                        if (HNXData.data[time12h59] === undefined && el.permdate === time13h)
                            HNXData.data[time12h59] = el;
                    });
                }
                if (res.hnX30 !== undefined) {
                    HNX30Data.priceBasic = lastPriceIndex["hnx30"][0], HNX30Data.data = {}, HNX30Data.lastId = 0;
                    // if (res.hnX30.d1.length && Number(res.hnX30.d1[0].indexbasic) > 0)
                    //     HNX30Data.priceBasic = Number(res.hnX30.d1[0].indexbasic);
                    if (res.hnX30.d.length) {
                        HNX30Data.lastId = res.hnX30.d[res.hnX30.d.length - 1].id;
                    }
                    res.hnX30.d.forEach((el, i) => {
                        el.permdate = Math.floor((Date.parse(el.permdate) / 1000) / 60) * 60000;
                        el.price = Number(el.price);
                        el.indexchange = Number(el.indexchange);
                        el.volume = parseInt(el.volume);
                        if (el.permdate > time9h)
                            HNX30Data.data[el.permdate] = el;
                        if (HNX30Data.data[time12h59] === undefined && el.permdate === time13h)
                            HNX30Data.data[time12h59] = el;
                    });
                }
                if (res.upcom !== undefined) {
                    UPCOMData.priceBasic = lastPriceIndex["upcom"][0], UPCOMData.data = {}, UPCOMData.lastId = 0;
                    // if (res.upcom.d1.length && Number(res.upcom.d1[0].indexbasic) > 0)
                    //     UPCOMData.priceBasic = Number(res.upcom.d1[0].indexbasic);
                    if (res.upcom.d.length) {
                        UPCOMData.lastId = res.upcom.d[res.upcom.d.length - 1].id;
                        // UPCOMData.marketStatus = getMarketStatusUPCOM(res.upcom.d[res.upcom.d.length - 1].permdate);
                        UPCOMData.marketStatus = getMarketStatusUPCOM(date.getTime());
                    }
                    res.upcom.d.forEach((el, i) => {
                        el.permdate = Math.floor((Date.parse(el.permdate) / 1000) / 60) * 60000;
                        el.price = Number(el.price);
                        el.indexchange = Number(el.indexchange);
                        el.volume = parseInt(el.volume);
                        if (el.permdate > time9h)
                            UPCOMData.data[el.permdate] = el;
                        if (UPCOMData.data[time12h59] === undefined && el.permdate === time13h)
                            UPCOMData.data[time12h59] = el;
                    });
                }
                io.to(`${clientId}`).emit('initialIndex', jo.pack({
                    marketStatusHOSE: VNIndexData.marketStatus, marketStatusHNX: HNXData.marketStatus,
                    marketStatusUPCOM: UPCOMData.marketStatus,
                    VNINDEX: VNIndexData, VN30: VN30Data, VNXALL: VNXAllData,
                    HNX: HNXData, HNX30: HNX30Data, UPCOM: UPCOMData
                }));
            }
        }).catch(err => {
            errorLogger.info(err);
        });
    }
    else {
        if (new Date().getTime() > time23h59) {
            time23h59 = new Date().setHours(23, 59, 59);
            var tmpData = [];
            if (VNIndexData.data !== undefined) {
                tmpData = Object.values(VNIndexData.data);
                VNIndexData.data = {};
                tmpData.forEach(itm => {
                    itm.permdate += 86400000;
                    VNIndexData.data[itm.permdate] = itm;
                });
            }
            if (VN30Data.data !== undefined) {
                tmpData = Object.values(VN30Data.data);
                VN30Data.data = {};
                tmpData.forEach(itm => {
                    itm.permdate += 86400000;
                    VN30Data.data[itm.permdate] = itm;
                });
            }
            if (VNXAllData.data !== undefined) {
                tmpData = Object.values(VNXAllData.data);
                VNXAllData.data = {};
                tmpData.forEach(itm => {
                    itm.permdate += 86400000;
                    VNXAllData.data[itm.permdate] = itm;
                });
            }
            if (HNXData.data !== undefined) {
                tmpData = Object.values(HNXData.data);
                HNXData.data = {};
                tmpData.forEach(itm => {
                    itm.permdate += 86400000;
                    HNXData.data[itm.permdate] = itm;
                });
            }
            if (HNX30Data.data !== undefined) {
                tmpData = Object.values(HNX30Data.data);
                HNX30Data.data = {};
                tmpData.forEach(itm => {
                    itm.permdate += 86400000;
                    HNX30Data.data[itm.permdate] = itm;
                });
            }
            if (UPCOMData.data !== undefined) {
                tmpData = Object.values(UPCOMData.data);
                UPCOMData.data = {};
                tmpData.forEach(itm => {
                    itm.permdate += 86400000;
                    UPCOMData.data[itm.permdate] = itm;
                });
            }
        }
        io.to(`${clientId}`).emit('initialIndex', jo.pack({
            marketStatusHOSE: VNIndexData.marketStatus, marketStatusHNX: HNXData.marketStatus,
            marketStatusUPCOM: UPCOMData.marketStatus,
            VNINDEX: VNIndexData, VN30: VN30Data, VNXALL: VNXAllData,
            HNX: HNXData, HNX30: HNX30Data, UPCOM: UPCOMData
        }));
    }
}

// stream INDEX data
function streamIndexData(io) {
        var indexInterval = setInterval(() => {
        var dateNow = new Date();
        if (dateNow.getDay() !== 0 && dateNow.getDay() !== 6) {
            var tsNow = dateNow.getTime();
            // Thiet lap cac moc thoi gian
            time8h = dateNow.setHours(8, 0, 0);
            time8h05 = dateNow.setHours(8, 5, 0);
            time8h45 = dateNow.setHours(8, 45, 0);
            time11h35 = dateNow.setHours(11, 35, 0);
            time12h55 = dateNow.setHours(12, 55, 0);
            time15h05 = dateNow.setHours(15, 5, 0);
            time23h59 = dateNow.setHours(23, 59, 59);
            time9h = parseInt(dateNow.setHours(9, 0, 0) / 1000) * 1000;
            time9h15 = parseInt(dateNow.setHours(9, 15, 0) / 1000) * 1000;
            time11h30 = parseInt(dateNow.setHours(11, 29, 0) / 1000) * 1000;
            time13h = parseInt(dateNow.setHours(13, 0, 0) / 1000) * 1000;
            time14h30 = parseInt(dateNow.setHours(14, 30, 0) / 1000) * 1000;
            time14h45 = parseInt(dateNow.setHours(14, 45, 0) / 1000) * 1000;
            time15h = parseInt(dateNow.setHours(15, 0, 0) / 1000) * 1000;
            time12h59 = parseInt(dateNow.setHours(12, 59, 0) / 1000) * 1000;

            if (tsNow > time8h && tsNow < time8h05) {
                lastPriceIndex["vnindex"][0] = lastPriceIndex["vnindex"][1];
                lastPriceIndex["vn30"][0] = lastPriceIndex["vn30"][1];
                lastPriceIndex["xall"][0] = lastPriceIndex["xall"][1];
                lastPriceIndex["hnx"][0] = lastPriceIndex["hnx"][1];
                lastPriceIndex["hnx30"][0] = lastPriceIndex["hnx30"][1];
                lastPriceIndex["upcom"][0] = lastPriceIndex["upcom"][1];
                redis.commonDb.set("StockBoard:LastPriceIndex", JSON.stringify(lastPriceIndex));
                VNIndexData.priceBasic = lastPriceIndex["vnindex"][1], VNIndexData.data = {}, VNIndexData.lastId = 0;
                VN30Data.priceBasic = lastPriceIndex["vn30"][1], VN30Data.data = {}, VN30Data.lastId = 0;
                VNXAllData.priceBasic = lastPriceIndex["xall"][1], VNXAllData.data = {}, VNXAllData.lastId = 0;
                HNXData.priceBasic = lastPriceIndex["hnx"][1], HNXData.data = {}, HNXData.lastId = 0;
                HNX30Data.priceBasic = lastPriceIndex["hnx30"][1], HNX30Data.data = {}, HNX30Data.lastId = 0;
                UPCOMData.priceBasic = lastPriceIndex["upcom"][1], UPCOMData.data = {}, UPCOMData.lastId = 0;
                io.emit('initialIndex', jo.pack({
                    marketStatusHOSE: VNIndexData.marketStatus, marketStatusHNX: HNXData.marketStatus,
                    marketStatusUPCOM: UPCOMData.marketStatus,
                    VNINDEX: VNIndexData, VN30: VN30Data, VNXALL: VNXAllData,
                    HNX: HNXData, HNX30: HNX30Data, UPCOM: UPCOMData
                }));
            }
            if ((tsNow > time8h45 && tsNow < time11h35) || (tsNow > time12h55 && tsNow < time15h05)) {
                rp({
                    method: 'POST',
                    url: process.env.API_URL + '/MarketData/GetAllIndexIntraday2',
                    qs: {
                        idhose: VNIndexData.lastId,
                        idvn30: VN30Data.lastId,
                        idvnallshare: VNXAllData.lastId,
                        idhnx: HNXData.lastId,
                        idhnx30: HNX30Data.lastId,
                        idupcom: UPCOMData.lastId
                    }
                }).then(res => {
                    if (res.s === 'ok') {
                        var tmpVNIndexData = {}, tmpVN30Data = {}, tmpVNXAllData = {},
                            tmpHNXData = {}, tmpHNX30Data = {}, tmpUPCOMData = {};
                        // if (res.hose.d1.length)
                        //     VNIndexData.priceBasic = res.hose.d1[0].indexbasic === ''
                        //         ? 0 : Number(res.hose.d1[0].indexbasic);
                        // if (res.vN30.d1.length)
                        //     VN30Data.priceBasic = res.vN30.d1[0].indexbasic === ''
                        //         ? 0 : Number(res.vN30.d1[0].indexbasic);
                        // if (res.xall.d1.length)
                        //     VNXAllData.priceBasic = res.xall.d1[0].indexbasic === ''
                        //         ? 0 : Number(res.xall.d1[0].indexbasic);
                        // if (res.hnx.d1.length)
                        //     HNXData.priceBasic = res.hnx.d1[0].indexbasic === ''
                        //         ? 0 : Number(res.hnx.d1[0].indexbasic);
                        // if (res.hnX30.d1.length)
                        //     HNX30Data.priceBasic = res.hnX30.d1[0].indexbasic === ''
                        //         ? 0 : Number(res.hnX30.d1[0].indexbasic);
                        // if (res.upcom.d1.length)
                        //     UPCOMData.priceBasic = res.upcom.d1[0].indexbasic === ''
                        //         ? 0 : Number(res.upcom.d1[0].indexbasic);
                        if (res.hose.d.length) {
                            lastPriceIndex['vnindex'][1] = Number(res.hose.d[res.hose.d.length - 1].price);
                            VNIndexData.lastId = res.hose.d[res.hose.d.length - 1].id;
                            // VNIndexData.marketStatus = getMarketStatusHOSE(res.hose.d[res.hose.d.length - 1].permdate);
                            VNIndexData.marketStatus = getMarketStatusHOSE(tsNow);
                            res.hose.d.forEach(el => {
                                el.permdate = Math.floor((Date.parse(el.permdate) / 1000) / 60) * 60000;
                                el.price = Number(el.price);
                                el.indexchange = Math.round(Number( (el.price) - VNIndexData.priceBasic) * 100) /100;
                                var tmpindexpercentchange= Number(el.indexchange / VNIndexData.priceBasic) * 100 ;
                                el.indexpercentchange = Math.round(tmpindexpercentchange * 100)/100 ;
                                el.totalvalue = Number(el.totalvalue);
                                el.volume = parseInt(el.volume);
                                
                                tmpVNIndexData[el.permdate] = el;
                                try {
                                    VNIndexData.data[el.permdate] = el;
                                } catch (error) {
                                    //console.log(el);
                                }
                                VNIndexData.data[el.permdate] = el;
                                if (VNIndexData.data[time12h59] === undefined && el.permdate === time13h) {
                                    tmpVNIndexData[time12h59] = el;
                                    VNIndexData.data[time12h59] = el;
                                }
                                
                            });
                        }
                       
                        if (res.vN30.d.length) {
                            lastPriceIndex['vn30'][1] = Number(res.vN30.d[res.vN30.d.length - 1].price);
                            VN30Data.lastId = res.vN30.d[res.vN30.d.length - 1].id;
                            res.vN30.d.forEach(el => {
                                el.permdate = Math.floor((Date.parse(el.permdate) / 1000) / 60) * 60000;
                                el.price = Number(el.price);
                                el.indexchange = Math.round(Number( (el.price) - VN30Data.priceBasic) * 100) /100; 
                                var tmpindexpercentchange= Number(el.indexchange / VN30Data.priceBasic) * 100 ;
                                el.indexpercentchange = Math.round(tmpindexpercentchange * 100)/100;
                                el.totalvalue = Number(el.totalvalue) ;
                                el.volume = parseInt(el.volume);
                                tmpVN30Data[el.permdate] = el;
                                VN30Data.data[el.permdate] = el;
                                if (VN30Data.data[time12h59] === undefined && el.permdate === time13h) {
                                    tmpVN30Data[time12h59] = el;
                                    VN30Data.data[time12h59] = el;
                                }
                              
                            });
                        }
                      
                        if (res.xall.d.length) {
                            lastPriceIndex['xall'][1] = Number(res.xall.d[res.xall.d.length - 1].price);
                            VNXAllData.lastId = res.xall.d[res.xall.d.length - 1].id;
                            res.xall.d.forEach(el => {
                                el.permdate = Math.floor((Date.parse(el.permdate) / 1000) / 60) * 60000;
                                el.price = Number(el.price);
                                el.indexchange = Math.round(Number( (el.price) - VNIndexData.priceBasic) * 100) /100;
                                var tmpindexpercentchange= Number(el.indexchange / VNXAllData.priceBasic) * 100 ;
                                el.indexpercentchange = Math.round(tmpindexpercentchange * 100)/100;
                                el.totalvalue = Number(el.totalvalue) ;
                                el.volume = parseInt(el.volume);
                                tmpVNXAllData[el.permdate] = el;
                                VNXAllData.data[el.permdate] = el;
                                if (VNXAllData.data[time12h59] === undefined && el.permdate === time13h) {
                                    tmpVNXAllData[time12h59] = el;
                                    VNXAllData.data[time12h59] = el;
                                }
                            });
                        }
                        if (res.hnx.d.length) {
                            lastPriceIndex['hnx'][1] = Number(res.hnx.d[res.hnx.d.length - 1].price);
                            var lastTimes = Date.parse(res.hnx.d[res.hnx.d.length - 1].permdate);
                            if (lastTimes < time14h30 || lastTimes > time14h45)
                                HNXData.lastId = res.hnx.d[res.hnx.d.length - 1].id;
                            // HNXData.marketStatus = getMarketStatusHNX(res.hnx.d[res.hnx.d.length - 1].permdate);
                            HNXData.marketStatus = getMarketStatusHNX(tsNow);
                            res.hnx.d.forEach(el => {
                                el.permdate = Math.floor((Date.parse(el.permdate) / 1000) / 60) * 60000;
                                el.price = Number(el.price);
                                el.indexchange = Number(el.indexchange);
                                el.volume = parseInt(el.volume);
                                tmpHNXData[el.permdate] = el;
                                HNXData.data[el.permdate] = el;
                                if (HNXData.data[time12h59] === undefined && el.permdate === time13h) {
                                    tmpHNXData[time12h59] = el;
                                    HNXData.data[time12h59] = el;
                                }
                            });
                        }
                        if (res.hnX30.d.length) {
                            lastPriceIndex['hnx30'][1] = Number(res.hnX30.d[res.hnX30.d.length - 1].price);
                            var lastTimes = Date.parse(res.hnX30.d[res.hnX30.d.length - 1].permdate);
                            if (lastTimes < time14h30 || lastTimes > time14h45)
                                HNX30Data.lastId = res.hnX30.d[res.hnX30.d.length - 1].id;
                            res.hnX30.d.forEach(el => {
                                el.permdate = Math.floor((Date.parse(el.permdate) / 1000) / 60) * 60000;
                                el.price = Number(el.price);
                                el.indexchange = Number(el.indexchange);
                                el.volume = parseInt(el.volume);
                                tmpHNX30Data[el.permdate] = el;
                                HNX30Data.data[el.permdate] = el;
                                if (HNX30Data.data[time12h59] === undefined && el.permdate === time13h) {
                                    tmpHNX30Data[time12h59] = el;
                                    HNX30Data.data[time12h59] = el;
                                }
                            });
                        }
                        if (res.upcom.d.length) {
                            lastPriceIndex['upcom'][1] = Number(res.upcom.d[res.upcom.d.length - 1].price);
                            UPCOMData.lastId = res.upcom.d[res.upcom.d.length - 1].id;
                            // UPCOMData.marketStatus = getMarketStatusUPCOM(res.upcom.d[res.upcom.d.length - 1].permdate);
                            UPCOMData.marketStatus = getMarketStatusUPCOM(tsNow);
                            res.upcom.d.forEach(el => {
                                el.permdate = Math.floor((Date.parse(el.permdate) / 1000) / 60) * 60000;
                                el.price = Number(el.price);
                                el.indexchange = Number(el.indexchange);
                                el.volume = parseInt(el.volume);
                                tmpUPCOMData[el.permdate] = el;
                                UPCOMData.data[el.permdate] = el;
                                if (UPCOMData.data[time12h59] === undefined && el.permdate === time13h) {
                                    tmpUPCOMData[time12h59] = el;
                                    UPCOMData.data[time12h59] = el;
                                }
                            });
                        }
                        
                        if (tsNow > time15h) {
                            VNIndexData.marketStatus = 'Đóng cửa';
                            HNXData.marketStatus = 'Đóng cửa';
                            UPCOMData.marketStatus = 'Đóng cửa';
                            try {
                                redis.commonDb.set("StockBoard:LastPriceIndex", JSON.stringify(lastPriceIndex));
                            } catch (error) {
                                console.log(error);
                            }
                            
                        }

                        io.emit('onChangeIndex', jo.pack({
                            marketStatusHOSE: VNIndexData.marketStatus, marketStatusHNX: HNXData.marketStatus,
                            marketStatusUPCOM: UPCOMData.marketStatus,
                            VNINDEX: { priceBasic: VNIndexData.priceBasic, data: tmpVNIndexData },
                            VN30: { priceBasic: VN30Data.priceBasic, data: tmpVN30Data },
                            VNXALL: { priceBasic: VNXAllData.priceBasic, data: tmpVNXAllData },
                            HNX: { priceBasic: HNXData.priceBasic, data: tmpHNXData },
                            HNX30: { priceBasic: HNX30Data.priceBasic, data: tmpHNX30Data },
                            UPCOM: { priceBasic: UPCOMData.priceBasic, data: tmpUPCOMData }
                        }));
                    }
                }).catch(err => {
                    errorLogger.info(err);
                    clearInterval(indexInterval);
                });
            }
        }
    }, Number(process.env.INDEX_INTERVAL));
}

module.exports = {
    initIndexData,
    streamIndexData
}