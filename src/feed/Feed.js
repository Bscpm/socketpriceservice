const stompit = require('stompit')
const dotenv = require('dotenv');
dotenv.config();
const errorLogger = require('../config/logger').errorLogger;

const connectOptions = {
    host: process.env.MQ_SERVER_ADDRESS,
    port: process.env.MQ_SERVER_PORT,
    timeout: 86400000,
    connectHeaders: {
        'host': process.env.MQ_SERVER_ADDRESS,
        'login': '',
        'passcode': '',
        'heart-beat': '5000,5000'
    }
};

var hasError = false;

function mqConnectionCallback(error, client, io, hoseOnChangeHandler, hnxOnChangeHandler, futureOnChangeHandler) {
    if (error) {
        errorLogger.info('connect mq error ' + error.message);
        return;
    }
    client.on('error', (error) => {
        hasError = true;
        errorLogger.info(error.message);
    });

    hasError = false;

    if (hoseOnChangeHandler !== null) {
        var hoseheaders = {
            'destination': '/topic/' + process.env.MQ_SERVER_HOSE_TOPIC,
            'ack': 'client-individual'
        };
        client.subscribe(hoseheaders, function (error, message) {
            if (error) {
                errorLogger.info('subscribe hose mq error ' + error.message);
                return;
            }
            message.readString('utf-8', function (error, body) {
                if (error) {
                    errorLogger.info('read hose mq message error ' + error.message);
                    return;
                }
                
                hoseOnChangeHandler(io, body);
                client.ack(message);
                // client.disconnect();
            });
        });
    }
    if (hnxOnChangeHandler !== null) {
        var hnxheaders = {
            'destination': '/topic/' + process.env.MQ_SERVER_HNX_TOPIC,
            'ack': 'client-individual'
        };
        client.subscribe(hnxheaders, function (error, message) {
            if (error) {
                errorLogger.info('subscribe hnx mq error ' + error.message);
                return;
            }
            message.readString('utf-8', function (error, body) {
                if (error) {
                    errorLogger.info('read hnx mq message error ' + error.message);
                    return;
                }
                hnxOnChangeHandler(io, body);
                client.ack(message);
                // client.disconnect();
            });
        });
    }
    if (futureOnChangeHandler !== null) {
        var futureheaders = {
            'destination': '/topic/' + process.env.MQ_SERVER_FUTURE_TOPIC,
            'ack': 'client-individual'
        };
        client.subscribe(futureheaders, function (error, message) {
            if (error) {
                errorLogger.info('subscribe PS mq error ' + error.message);
                return;
            }
            message.readString('utf-8', function (error, body) {
                if (error) {
                    errorLogger.info('read PS mq message error ' + error.message);
                    return;
                }
                futureOnChangeHandler(io, body);
                client.ack(message);
                // client.disconnect();
            });
        });
    }
}

function mqConnection(io, hoseOnChangeHandler, hnxOnChangeHandler, futureOnChangeHandler) {
    stompit.connect(connectOptions, function (error, client) {
        mqConnectionCallback(error, client, io, hoseOnChangeHandler, hnxOnChangeHandler, futureOnChangeHandler)
    });
}

function mqReconnect(io, hoseOnChangeHandler, hnxOnChangeHandler, futureOnChangeHandler) {
    if (hasError) {
        stompit.connect(connectOptions, function (error, client) {
            errorLogger.info('mq reconnect');
            mqConnectionCallback(error, client, io, hoseOnChangeHandler, hnxOnChangeHandler, futureOnChangeHandler);
        });
    }
}

module.exports = {
    mqConnection,
    mqReconnect
}
