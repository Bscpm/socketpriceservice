const stringHelper = require('../utils/String');
const stockHelper = require('../utils/Stock');
var redis = require('../database/redis');
var JsonOptimize = require('json-optimize'), jo = new JsonOptimize();

var lastDataFuture = {}, matchHistData = {}, dealTransactionData = [];
var isYieldCalculated = false;
var lastPriceStock = {};

// Reset bien kiem tra su thay doi msg DI data
function resetDIDataChange(item) {
    item.total_vol.change = 0;
    item.total_value.change = 0;
    item.match_price.change = 0;
    item.match_qtty.change = 0;
    item.low_price.change = 0;
    item.high_price.change = 0;
    item.nn_buy.change = 0;
    item.nn_sell.change = 0;
}
// Reset bien kiem tra su thay doi msg TP data
function resetTPDataChange(item) {
    item.buy_1.change = 0;
    item.buy_2.change = 0;
    item.buy_3.change = 0;
    item.buy_4.change = 0;
    item.buy_5.change = 0;
    item.buy_6.change = 0;
    item.buy_7.change = 0;
    item.buy_8.change = 0;
    item.buy_9.change = 0;
    item.buy_10.change = 0;
    item.buy_qtty_1.change = 0;
    item.buy_qtty_2.change = 0;
    item.buy_qtty_3.change = 0;
    item.buy_qtty_4.change = 0;
    item.buy_qtty_5.change = 0;
    item.buy_qtty_6.change = 0;
    item.buy_qtty_7.change = 0;
    item.buy_qtty_8.change = 0;
    item.buy_qtty_9.change = 0;
    item.buy_qtty_10.change = 0;
    item.sell_1.change = 0;
    item.sell_2.change = 0;
    item.sell_3.change = 0;
    item.sell_4.change = 0;
    item.sell_5.change = 0;
    item.sell_6.change = 0;
    item.sell_7.change = 0;
    item.sell_8.change = 0;
    item.sell_9.change = 0;
    item.sell_10.change = 0;
    item.sell_qtty_1.change = 0;
    item.sell_qtty_2.change = 0;
    item.sell_qtty_3.change = 0;
    item.sell_qtty_4.change = 0;
    item.sell_qtty_5.change = 0;
    item.sell_qtty_6.change = 0;
    item.sell_qtty_7.change = 0;
    item.sell_qtty_8.change = 0;
    item.sell_qtty_9.change = 0;
    item.sell_qtty_10.change = 0;
}

function resetDataChange(data) {
    data.forEach(function (item) {
        resetTPDataChange(item);
        resetDIDataChange(item);
    });
    return data;
}

function resetData() {
    dataArr = Object.values(lastDataFuture);
    dataArr.forEach(function (item) {
        if (!stringHelper.isEmpty(item.maturity_date) && stringHelper.compareDateString(item.maturity_date, stringHelper.curDate2String('/'), '/')) {
            delete lastDataFuture[item.symbol];
        } else {
            item.open_interest = 0;
            item.buy_3.val = 0; item.buy_2.val = 0; item.buy_1.val = 0;
            item.buy_3.yield = 0; item.buy_2.yield = 0; item.buy_1.yield = 0;
            item.buy_4.val = 0; item.buy_5.val = 0; item.buy_6.val = 0;
            item.buy_7.val = 0; item.buy_8.val = 0; item.buy_9.val = 0; item.buy_10.val = 0;
            item.buy_qtty_3.val = 0; item.buy_qtty_2.val = 0; item.buy_qtty_1.val = 0;
            item.buy_qtty_4.val = 0; item.buy_qtty_5.val = 0; item.buy_qtty_6.val = 0;
            item.buy_qtty_7.val = 0; item.buy_qtty_8.val = 0; item.buy_qtty_9.val = 0; item.buy_qtty_10.val = 0;
            item.match_price.val = 0; item.match_price.yield = 0; item.match_qtty.val = 0; item.difference = 0;
            item.sell_1.val = 0; item.sell_2.val = 0; item.sell_3.val = 0;
            item.sell_1.yield = 0; item.sell_2.yield = 0; item.sell_3.yield = 0;
            item.sell_4.val = 0; item.sell_5.val = 0; item.sell_6.val = 0;
            item.sell_7.val = 0; item.sell_8.val = 0; item.sell_9.val = 0; item.sell_10.val = 0;
            item.sell_qtty_1.val = 0; item.sell_qtty_2.val = 0; item.sell_qtty_3.val = 0;
            item.sell_qtty_4.val = 0; item.sell_qtty_5.val = 0; item.sell_qtty_6.val = 0;
            item.sell_qtty_7.val = 0; item.sell_qtty_8.val = 0; item.sell_qtty_9.val = 0; item.sell_qtty_10.val = 0;
            item.total_vol.val = 0; item.total_value.val = '', item.total_value.intVal = 0;
            item.nn_buy.val = 0; item.nn_sell.val = 0;
            item.open_price.val = 0; item.high_price.val = 0; item.low_price.val = 0;
            item.open_price.yield = 0; item.high_price.yield = 0; item.low_price.yield = 0;
            // item.matchHistoryStream = {};
            dataObj[item.symbol] = item;
        }
    });
}

function loadHistoryData(historyData, historyLastPriceStock) {
    lastDataFuture = historyData;
    lastPriceStock = historyLastPriceStock;
}
function loadMatchHistData(_matchHistData) {
    matchHistData = _matchHistData;
    // console.log(matchHistData)
}

function saveLastPriceStock() {
    if (process.env.WRITE_LAST_PRICE_STOCK === 'Y') {
        redis.commonDb.set('StockBoard:LastPriceStock', JSON.stringify(lastPriceStock));
    }
}

function initDataForOnline(io, clientId) {
    data = resetDataChange(Object.values(lastDataFuture));
    io.to(`${clientId}`).emit('initialStocksForOnline', jo.pack({ stockTableId: 'future', data: data }));
}

function futureOnChangeHandler(io, string) {
    if (!string.includes("HeartBeat")) {
        var jsonArr = JSON.parse(string);
        var last10MatchHist = {};
        for (var jsonObj of jsonArr) {
            if (jsonObj['MsgType'] === 'INIT_DAY') {
                resetData();
                matchHistData = {};
                dealTransactionData = [];
                isYieldCalculated = false;
                break;
            }
            var stock = lastDataFuture[jsonObj['Symbol']];
            if (stock === undefined) {
                lastDataFuture[jsonObj['Symbol']] = {
                    symbol: jsonObj['Symbol'], maturity_date: '', ceiling: { val: 0, yield: 0 }, floor: { val: 0, yield: 0 },
                    reference: { val: 0, yield: 0 }, open_interest: 0,
                    buy_3: { val: 0, yield: 0, change: 0, time: 0 }, buy_qtty_3: { val: 0, change: 0, time: 0 },
                    buy_2: { val: 0, yield: 0, change: 0, time: 0 }, buy_qtty_2: { val: 0, change: 0, time: 0 },
                    buy_1: { val: 0, yield: 0, change: 0, time: 0 }, buy_qtty_1: { val: 0, change: 0, time: 0 },
                    buy_4: { val: 0, change: 0, time: 0 }, buy_qtty_4: { val: 0, change: 0, time: 0 },
                    buy_5: { val: 0, change: 0, time: 0 }, buy_qtty_5: { val: 0, change: 0, time: 0 },
                    buy_6: { val: 0, change: 0, time: 0 }, buy_qtty_6: { val: 0, change: 0, time: 0 },
                    buy_7: { val: 0, change: 0, time: 0 }, buy_qtty_7: { val: 0, change: 0, time: 0 },
                    buy_8: { val: 0, change: 0, time: 0 }, buy_qtty_8: { val: 0, change: 0, time: 0 },
                    buy_9: { val: 0, change: 0, time: 0 }, buy_qtty_9: { val: 0, change: 0, time: 0 },
                    buy_10: { val: 0, change: 0, time: 0 }, buy_qtty_10: { val: 0, change: 0, time: 0 },
                    match_price: { val: 0, yield: 0, change: 0, time: 0 }, match_qtty: { val: 0, change: 0, time: 0 }, difference: 0,
                    sell_1: { val: 0, yield: 0, change: 0, time: 0 }, sell_qtty_1: { val: 0, change: 0, time: 0 },
                    sell_2: { val: 0, yield: 0, change: 0, time: 0 }, sell_qtty_2: { val: 0, change: 0, time: 0 },
                    sell_3: { val: 0, yield: 0, change: 0, time: 0 }, sell_qtty_3: { val: 0, change: 0, time: 0 },
                    sell_4: { val: 0, change: 0, time: 0 }, sell_qtty_4: { val: 0, change: 0, time: 0 },
                    sell_5: { val: 0, change: 0, time: 0 }, sell_qtty_5: { val: 0, change: 0, time: 0 },
                    sell_6: { val: 0, change: 0, time: 0 }, sell_qtty_6: { val: 0, change: 0, time: 0 },
                    sell_7: { val: 0, change: 0, time: 0 }, sell_qtty_7: { val: 0, change: 0, time: 0 },
                    sell_8: { val: 0, change: 0, time: 0 }, sell_qtty_8: { val: 0, change: 0, time: 0 },
                    sell_9: { val: 0, change: 0, time: 0 }, sell_qtty_9: { val: 0, change: 0, time: 0 },
                    sell_10: { val: 0, change: 0, time: 0 }, sell_qtty_10: { val: 0, change: 0, time: 0 },
                    total_vol: { val: 0, change: 0, time: 0 }, total_value: { val: '', intVal: 0, change: 0, time: 0 }, open_price: { val: 0, yield: 0 },
                    high_price: { val: 0, yield: 0, change: 0, time: 0 }, low_price: { val: 0, yield: 0, change: 0, time: 0 },
                    nn_buy: { val: 0, change: 0, time: 0 }, nn_sell: { val: 0, change: 0, time: 0 },
                    underlying: '', market: 'deri', trading_session: ''
                };
                stock = lastDataFuture[jsonObj['Symbol']];
            }

            // if (!stringHelper.isEmpty(stock.maturity_date) && stringHelper.compareDateString(stock.maturity_date, stringHelper.curDate2String('/'), '/')) {
            //     delete lastDataFuture[stock.symbol];
            // }

            // Xu ly message DI
            if (jsonObj['MsgType'] === 'DI') {
                resetDIDataChange(stock);
                // lastDataFuture[jsonObj['Symbol']].lastMatchHistory = null;
                stock.symbol = jsonObj['Symbol'];
                stock.underlying = jsonObj['Underlying'];
                stock.trading_session = jsonObj['TradingSessionID'];
                stock.stock_type = jsonObj['SecurityType'];
                if (stock.trading_session === 'AVAILABLE' && jsonObj['Time'].split(':')[0] >= '11')
                    stock.trading_session = 'BREAK';
                stock.maturity_date = jsonObj['LastTradingDate'];
                stock.ceiling.val = parseFloat(jsonObj['CeilingPrice']);
                stock.floor.val = parseFloat(jsonObj['FloorPrice']);
                stock.reference.val = parseFloat(jsonObj['BasicPrice']);
                if (jsonObj['OpenInterest'] !== undefined) {
                    stock.open_interest = parseInt(jsonObj['OpenInterest']);
                }
                stock.open_price.val = jsonObj['OpenPrice'] === undefined ? lastDataFuture[stock.symbol].open_price.val : parseFloat(jsonObj['OpenPrice']);

                if (jsonObj['HighestPrice'] !== undefined) {
                    highestPrice = parseFloat(jsonObj['HighestPrice']);
                    if (highestPrice === lastDataFuture[stock.symbol].high_price.val)
                        stock.high_price.change = 0;
                    else {
                        stock.high_price.change = 1;
                        stock.high_price.time = lastDataFuture[stock.symbol].high_price.time === 1 ? 2 : 1;
                    }
                    stock.high_price.val = highestPrice;
                }
                if (jsonObj['LowestPrice'] !== undefined) {
                    lowestPrice = parseFloat(jsonObj['LowestPrice']);
                    if (lowestPrice === lastDataFuture[stock.symbol].low_price.val)
                        stock.low_price.change = 0;
                    else {
                        stock.low_price.change = 1;
                        stock.low_price.time = lastDataFuture[stock.symbol].low_price.time === 1 ? 2 : 1;
                    }
                    stock.low_price.val = lowestPrice;
                }
                if (jsonObj['BuyForeignQtty'] !== undefined) {
                    buyForeignQtty = parseInt(jsonObj['BuyForeignQtty']);
                    if (buyForeignQtty === lastDataFuture[stock.symbol].nn_buy.val)
                        stock.nn_buy.change = 0;
                    else {
                        stock.nn_buy.change = 1;
                        stock.nn_buy.time = lastDataFuture[stock.symbol].nn_buy.time === 1 ? 2 : 1;
                    }
                    stock.nn_buy.val = buyForeignQtty;
                }
                if (jsonObj['SellForeignQtty'] !== undefined) {
                    sellForeignQtty = parseInt(jsonObj['SellForeignQtty']);
                    if (sellForeignQtty === lastDataFuture[stock.symbol].nn_sell.val)
                        stock.nn_sell.change = 0;
                    else {
                        stock.nn_sell.change = 1;
                        stock.nn_sell.time = lastDataFuture[stock.symbol].nn_sell.time === 1 ? 2 : 1;
                    }
                    stock.nn_sell.val = sellForeignQtty;
                }
                if (stock.trading_session === 'CALL_AUCTION_OPENING' || stock.trading_session === 'CALL_AUCTION_CLOSING') {
                    if (jsonObj['CurrentPrice'] === undefined) {
                        stock.match_price.val = 0;
                    } else {
                        currentPrice = parseFloat(jsonObj['CurrentPrice']);
                        if (currentPrice === lastDataFuture[stock.symbol].match_price.val)
                            stock.match_price.change = 0;
                        else {
                            stock.match_price.change = 1;
                            stock.match_price.time = lastDataFuture[stock.symbol].match_price.time === 1 ? 2 : 1;
                            // stockHelper.writeLastPriceStock('future', stock.symbol, currentPrice);
                        }
                        stock.match_price.val = currentPrice;
                        if (currentPrice > 0) {
                            lastPriceStock[stock.symbol] = {
                                symbol: stock.symbol,
                                floor: stock.market.toUpperCase(),
                                stocktype: stock.stock_type,
                                price: currentPrice
                            };
                        }
                    }
                    if (jsonObj['CurrentQtty'] === undefined) {
                        stock.match_qtty.val = 0;
                    } else {
                        currentQtty = parseInt(jsonObj['CurrentQtty']);
                        if (currentQtty === lastDataFuture[stock.symbol].match_qtty.val)
                            stock.match_qtty.change = 0;
                        else {
                            stock.match_qtty.change = 1;
                            stock.match_qtty.time = lastDataFuture[stock.symbol].match_qtty.time === 1 ? 2 : 1;
                        }
                        stock.match_qtty.val = currentQtty;
                    }
                } else {
                    if (jsonObj['MatchPrice'] !== undefined) {
                        matchPrice = parseFloat(jsonObj['MatchPrice']);
                        if (matchPrice === lastDataFuture[stock.symbol].match_price.val)
                            stock.match_price.change = 0;
                        else {
                            stock.match_price.change = 1;
                            stock.match_price.time = lastDataFuture[stock.symbol].match_price.time === 1 ? 2 : 1;
                            // stockHelper.writeLastPriceStock('future', stock.symbol, matchPrice);
                        }
                        stock.match_price.val = matchPrice;
                        if (matchPrice > 0) {
                            lastPriceStock[stock.symbol] = {
                                symbol: stock.symbol,
                                floor: stock.market.toUpperCase(),
                                stocktype: stock.stock_type,
                                price: matchPrice
                            };
                        }
                    }
                    if (jsonObj['MatchQtty'] !== undefined) {
                        matchQtty = parseInt(jsonObj['MatchQtty']);
                        if (matchQtty === lastDataFuture[stock.symbol].match_qtty.val)
                            stock.match_qtty.change = 0;
                        else {
                            stock.match_qtty.change = 1;
                            stock.match_qtty.time = lastDataFuture[stock.symbol].match_qtty.time === 1 ? 2 : 1;
                        }
                        stock.match_qtty.val = matchQtty;
                    }
                }
                if (stock.match_price.val !== 0) {
                    stock.difference = Number((stock.match_price.val - stock.reference.val).toFixed(1));
                } else {
                    stock.difference = lastDataFuture[stock.symbol].difference;
                    if (stock.trading_session === 'CALL_AUCTION_OPENING' || stock.trading_session === 'CALL_AUCTION_CLOSING')
                        stock.difference = 0;
                }
                if (jsonObj['NM_TotalTradedQtty'] !== undefined) {
                    totalVolumeTraded = parseInt(jsonObj['NM_TotalTradedQtty']);
                    if (totalVolumeTraded === lastDataFuture[stock.symbol].total_vol.val) {
                        stock.total_vol.change = 0;
                    }
                    else {
                        stock.total_vol.change = 1;
                        stock.total_vol.time = lastDataFuture[stock.symbol].total_vol.time === 1 ? 2 : 1;
                        if (matchHistData[stock.symbol] === undefined) {
                            matchHistData[stock.symbol] = [{
                                time: jsonObj['Time'],
                                price: stock.match_price.val,
                                volume: (totalVolumeTraded - lastDataFuture[stock.symbol].total_vol.val),
                                total_vol: totalVolumeTraded
                            }];
                            last10MatchHist[stock.symbol] = matchHistData[stock.symbol];
                        } else {
                            matchHistData[stock.symbol].push({
                                time: jsonObj['Time'],
                                price: stock.match_price.val,
                                volume: (totalVolumeTraded - lastDataFuture[stock.symbol].total_vol.val),
                                change_value: Number((stock.match_price.val - stock.reference).toFixed(2)),
                                total_vol: totalVolumeTraded
                            });
                            last10MatchHist[stock.symbol] = matchHistData[stock.symbol].slice(matchHistData[stock.symbol].length - 10);
                        }
                        // stock.matchHistoryStream[jsonObj['Time']] = {
                        //     time: jsonObj['Time'],
                        //     price: stock.match_price.val,
                        //     volume: (totalVolumeTraded - lastDataFuture[stock.symbol].total_vol.val),
                        //     total_vol: totalVolumeTraded
                        // }
                    }
                    stock.total_vol.val = totalVolumeTraded;
                }
                if (jsonObj['NM_TotalTradedValue'] !== undefined) {
                    totalTradedValue = parseInt(jsonObj['NM_TotalTradedValue']);
                    if (totalTradedValue === lastDataFuture[stock.symbol].total_value.val) {
                        stock.total_value.change = 0;
                    }
                    else {
                        stock.total_value.change = 1;
                        stock.total_value.time = lastDataFuture[stock.symbol].total_value.time === 1 ? 2 : 1;
                    }
                    stock.total_value.val = Math.round(totalTradedValue / 1000000).toLocaleString('en');
                    stock.total_value.intVal = totalTradedValue;
                }
                if (jsonObj['PT_MatchPrice'] !== undefined && jsonObj['PT_MatchQtty'] !== undefined) {
                    var iTotalVol = parseInt(jsonObj['PT_TotalTradedQtty']);
                    var dealStocks = dealTransactionData.filter(s => s.symbol === stock.symbol);
                    if (dealStocks.length && iTotalVol > dealStocks[dealStocks.length].totalVolume) {
                        var fPrice = parseFloat(jsonObj['PT_MatchPrice']),
                            iVolume = parseInt(jsonObj['PT_MatchQtty']);
                        // -2: bang gia san, -1: nho hon gia TC, 0: bang gia TC, 1: lon hon gia TC, 2: bang gia tran
                        var diff = -1;
                        if (iPrice === stock.floor) diff = -2;
                        else if (iPrice === stock.ceiling) diff = 2;
                        else if (iPrice === stock.reference) diff = 0;
                        else if (iPrice > stock.reference) diff = 1;

                        dealTransactionData.push({
                            symbol: stock.symbol,
                            price: fPrice,
                            yield: stockHelper.bondYield(stock.underlying, stock.underlying, fPrice),
                            difference: diff,
                            volume: iVolume,
                            value: (fPrice * iVolume).toFixed(2),
                            totalVolume: iTotalVol,
                            totalValue: parseFloat(jsonObj['PT_TotalTradedValue']),
                            time: jsonObj['Time']
                        });
                        io.to('future').emit('onChangeDealTransaction', jo.pack(dealTransactionData));
                    }
                }
                // if (stock.underlying === 'VGB5') {
                if (!isYieldCalculated) {
                    stock.ceiling.yield = stockHelper.bondYield(stock.underlying, stock.ceiling.val);
                    stock.floor.yield = stockHelper.bondYield(stock.underlying, stock.floor.val);
                    stock.reference.yield = stockHelper.bondYield(stock.underlying, stock.reference.val);
                }
                if (stock.match_price.val > 0 && stock.match_price.change !== 0)
                    stock.match_price.yield = stockHelper.bondYield(stock.underlying, stock.match_price.val);
                if (stock.open_price.val > 0 && stock.open_price.change !== 0)
                    stock.open_price.yield = stockHelper.bondYield(stock.underlying, stock.open_price.val);
                if (jsonObj['HighestPrice'] !== undefined && stock.high_price.val > 0
                    && stock.high_price.change !== 0)
                    stock.high_price.yield = stockHelper.bondYield(stock.underlying, stock.high_price.val);
                if (jsonObj['LowestPrice'] !== undefined && stock.low_price.val > 0
                    && stock.low_price.change !== 0)
                    stock.low_price.yield = stockHelper.bondYield(stock.underlying, stock.low_price.val);
                // }
            }
            // Xu ly message TP
            else if (jsonObj['MsgType'] === 'TP') {
                resetTPDataChange(stock);
                stock.symbol = jsonObj['Symbol'];
                if (jsonObj['BestBidPrice4'] !== undefined) {
                    bestBidPrice4 = parseFloat(jsonObj['BestBidPrice4']);
                    stock.buy_4.change = -1;
                    if (bestBidPrice4 === stock.buy_4.val)
                        stock.buy_4.change = 0;
                    else if (bestBidPrice4 > stock.buy_4.val)
                        stock.buy_4.change = 1;
                    stock.buy_4.val = bestBidPrice4;
                    if (stock.buy_4.change !== 0)
                        stock.buy_4.time = stock.buy_4.time === 1 ? 2 : 1;
                } else {
                    stock.buy_4.change = 0;
                    stock.buy_4.val = 0;
                }
                if (jsonObj['BestBidQtty4'] !== undefined) {
                    bestBidQtty4 = parseInt(jsonObj['BestBidQtty4']);
                    stock.buy_qtty_4.change = -1;
                    if (bestBidQtty4 === stock.buy_qtty_4.val)
                        stock.buy_qtty_4.change = 0;
                    else if (bestBidQtty4 > stock.buy_qtty_4.val)
                        stock.buy_qtty_4.change = 1;
                    stock.buy_qtty_4.val = bestBidQtty4;
                    if (stock.buy_qtty_4.change !== 0)
                        stock.buy_qtty_4.time = stock.buy_qtty_4.time === 1 ? 2 : 1;
                } else {
                    stock.buy_qtty_4.change = 0;
                    stock.buy_qtty_4.val = 0;
                }
                if (jsonObj['BestBidPrice5'] !== undefined) {
                    bestBidPrice5 = parseFloat(jsonObj['BestBidPrice5']);
                    stock.buy_5.change = -1;
                    if (bestBidPrice5 === stock.buy_5.val)
                        stock.buy_5.change = 0;
                    else if (bestBidPrice5 > stock.buy_5.val)
                        stock.buy_5.change = 1;
                    stock.buy_5.val = bestBidPrice5;
                    if (stock.buy_5.change !== 0)
                        stock.buy_5.time = stock.buy_5.time === 1 ? 2 : 1;
                } else {
                    stock.buy_5.change = 0;
                    stock.buy_5.val = 0;
                }
                if (jsonObj['BestBidQtty5'] !== undefined) {
                    bestBidQtty5 = parseInt(jsonObj['BestBidQtty5']);
                    stock.buy_qtty_5.change = -1;
                    if (bestBidQtty5 === stock.buy_qtty_5.val)
                        stock.buy_qtty_5.change = 0;
                    else if (bestBidQtty5 > stock.buy_qtty_5.val)
                        stock.buy_qtty_5.change = 1;
                    stock.buy_qtty_5.val = bestBidQtty5;
                    if (stock.buy_qtty_5.change !== 0)
                        stock.buy_qtty_5.time = stock.buy_qtty_5.time === 1 ? 2 : 1;
                } else {
                    stock.buy_qtty_5.change = 0;
                    stock.buy_qtty_5.val = 0;
                }
                if (jsonObj['BestBidPrice6'] !== undefined) {
                    bestBidPrice6 = parseFloat(jsonObj['BestBidPrice6']);
                    stock.buy_6.change = -1;
                    if (bestBidPrice6 === stock.buy_6.val)
                        stock.buy_6.change = 0;
                    else if (bestBidPrice6 > stock.buy_6.val)
                        stock.buy_6.change = 1;
                    stock.buy_6.val = bestBidPrice6;
                    if (stock.buy_6.change !== 0)
                        stock.buy_6.time = stock.buy_6.time === 1 ? 2 : 1;
                } else {
                    stock.buy_6.change = 0;
                    stock.buy_6.val = 0;
                }
                if (jsonObj['BestBidQtty6'] !== undefined) {
                    bestBidQtty6 = parseInt(jsonObj['BestBidQtty6']);
                    stock.buy_qtty_6.change = -1;
                    if (bestBidQtty6 === stock.buy_qtty_6.val)
                        stock.buy_qtty_6.change = 0;
                    else if (bestBidQtty6 > stock.buy_qtty_6.val)
                        stock.buy_qtty_6.change = 1;
                    stock.buy_qtty_6.val = bestBidQtty6;
                    if (stock.buy_qtty_6.change !== 0)
                        stock.buy_qtty_6.time = stock.buy_qtty_6.time === 1 ? 2 : 1;
                } else {
                    stock.buy_qtty_6.change = 0;
                    stock.buy_qtty_6.val = 0;
                }
                if (jsonObj['BestBidPrice7'] !== undefined) {
                    bestBidPrice7 = parseFloat(jsonObj['BestBidPrice7']);
                    stock.buy_7.change = -1;
                    if (bestBidPrice7 === stock.buy_7.val)
                        stock.buy_7.change = 0;
                    else if (bestBidPrice7 > stock.buy_7.val)
                        stock.buy_7.change = 1;
                    stock.buy_7.val = bestBidPrice7;
                    if (stock.buy_7.change !== 0)
                        stock.buy_7.time = stock.buy_7.time === 1 ? 2 : 1;
                } else {
                    stock.buy_7.change = 0;
                    stock.buy_7.val = 0;
                }
                if (jsonObj['BestBidQtty7'] !== undefined) {
                    bestBidQtty7 = parseInt(jsonObj['BestBidQtty7']);
                    stock.buy_qtty_7.change = -1;
                    if (bestBidQtty7 === stock.buy_qtty_7.val)
                        stock.buy_qtty_7.change = 0;
                    else if (bestBidQtty7 > stock.buy_qtty_7.val)
                        stock.buy_qtty_7.change = 1;
                    stock.buy_qtty_7.val = bestBidQtty7;
                    if (stock.buy_qtty_7.change !== 0)
                        stock.buy_qtty_7.time = stock.buy_qtty_7.time === 1 ? 2 : 1;
                } else {
                    stock.buy_qtty_7.change = 0;
                    stock.buy_qtty_7.val = 0;
                }
                if (jsonObj['BestBidPrice8'] !== undefined) {
                    bestBidPrice8 = parseFloat(jsonObj['BestBidPrice8']);
                    stock.buy_8.change = -1;
                    if (bestBidPrice8 === stock.buy_8.val)
                        stock.buy_8.change = 0;
                    else if (bestBidPrice8 > stock.buy_8.val)
                        stock.buy_8.change = 1;
                    stock.buy_8.val = bestBidPrice8;
                    if (stock.buy_8.change !== 0)
                        stock.buy_8.time = stock.buy_8.time === 1 ? 2 : 1;
                } else {
                    stock.buy_8.change = 0;
                    stock.buy_8.val = 0;
                }
                if (jsonObj['BestBidQtty8'] !== undefined) {
                    bestBidQtty8 = parseInt(jsonObj['BestBidQtty8']);
                    stock.buy_qtty_8.change = -1;
                    if (bestBidQtty8 === stock.buy_qtty_8.val)
                        stock.buy_qtty_8.change = 0;
                    else if (bestBidQtty8 > stock.buy_qtty_8.val)
                        stock.buy_qtty_8.change = 1;
                    stock.buy_qtty_8.val = bestBidQtty8;
                    if (stock.buy_qtty_8.change !== 0)
                        stock.buy_qtty_8.time = stock.buy_qtty_8.time === 1 ? 2 : 1;
                } else {
                    stock.buy_qtty_8.change = 0;
                    stock.buy_qtty_8.val = 0;
                }
                if (jsonObj['BestBidPrice9'] !== undefined) {
                    bestBidPrice9 = parseFloat(jsonObj['BestBidPrice9']);
                    stock.buy_9.change = -1;
                    if (bestBidPrice9 === stock.buy_9.val)
                        stock.buy_9.change = 0;
                    else if (bestBidPrice9 > stock.buy_9.val)
                        stock.buy_9.change = 1;
                    stock.buy_9.val = bestBidPrice9;
                    if (stock.buy_9.change !== 0)
                        stock.buy_9.time = stock.buy_9.time === 1 ? 2 : 1;
                } else {
                    stock.buy_9.change = 0;
                    stock.buy_9.val = 0;
                }
                if (jsonObj['BestBidQtty9'] !== undefined) {
                    bestBidQtty9 = parseInt(jsonObj['BestBidQtty9']);
                    stock.buy_qtty_9.change = -1;
                    if (bestBidQtty9 === stock.buy_qtty_9.val)
                        stock.buy_qtty_9.change = 0;
                    else if (bestBidQtty9 > stock.buy_qtty_9.val)
                        stock.buy_qtty_9.change = 1;
                    stock.buy_qtty_9.val = bestBidQtty9;
                    if (stock.buy_qtty_9.change !== 0)
                        stock.buy_qtty_9.time = stock.buy_qtty_9.time === 1 ? 2 : 1;
                } else {
                    stock.buy_qtty_9.change = 0;
                    stock.buy_qtty_9.val = 0;
                }
                if (jsonObj['BestBidPrice10'] !== undefined) {
                    bestBidPrice10 = parseFloat(jsonObj['BestBidPrice10']);
                    stock.buy_10.change = -1;
                    if (bestBidPrice10 === stock.buy_10.val)
                        stock.buy_10.change = 0;
                    else if (bestBidPrice10 > stock.buy_10.val)
                        stock.buy_10.change = 1;
                    stock.buy_10.val = bestBidPrice10;
                    if (stock.buy_10.change !== 0)
                        stock.buy_10.time = stock.buy_10.time === 1 ? 2 : 1;
                } else {
                    stock.buy_10.change = 0;
                    stock.buy_10.val = 0;
                }
                if (jsonObj['BestBidQtty10'] !== undefined) {
                    bestBidQtty10 = parseInt(jsonObj['BestBidQtty10']);
                    stock.buy_qtty_10.change = -1;
                    if (bestBidQtty10 === stock.buy_qtty_10.val)
                        stock.buy_qtty_10.change = 0;
                    else if (bestBidQtty10 > stock.buy_qtty_10.val)
                        stock.buy_qtty_10.change = 1;
                    stock.buy_qtty_10.val = bestBidQtty10;
                    if (stock.buy_qtty_10.change !== 0)
                        stock.buy_qtty_10.time = stock.buy_qtty_10.time === 1 ? 2 : 1;
                } else {
                    stock.buy_qtty_10.change = 0;
                    stock.buy_qtty_10.val = 0;
                }
                if (jsonObj['BestBidPrice3'] !== undefined) {
                    bestBidPrice3 = parseFloat(jsonObj['BestBidPrice3']);
                    stock.buy_3.change = -1;
                    if (bestBidPrice3 === stock.buy_3.val)
                        stock.buy_3.change = 0;
                    else if (bestBidPrice3 > stock.buy_3.val)
                        stock.buy_3.change = 1;
                    stock.buy_3.val = bestBidPrice3;
                    if (stock.buy_3.change !== 0) {
                        stock.buy_3.time = stock.buy_3.time === 1 ? 2 : 1;
                        // if (stock.underlying === 'VGB5')
                        stock.buy_3.yield = stockHelper.bondYield(stock.underlying, stock.buy_3.val);
                    }
                } else {
                    stock.buy_3.change = 0;
                    stock.buy_3.val = 0;
                }
                if (jsonObj['BestBidQtty3'] !== undefined) {
                    bestBidQtty3 = parseInt(jsonObj['BestBidQtty3']);
                    stock.buy_qtty_3.change = -1;
                    if (bestBidQtty3 === stock.buy_qtty_3.val)
                        stock.buy_qtty_3.change = 0;
                    else if (bestBidQtty3 > stock.buy_qtty_3.val)
                        stock.buy_qtty_3.change = 1;
                    stock.buy_qtty_3.val = bestBidQtty3;
                    if (stock.buy_qtty_3.change !== 0)
                        stock.buy_qtty_3.time = stock.buy_qtty_3.time === 1 ? 2 : 1;
                } else {
                    stock.buy_qtty_3.change = 0;
                    stock.buy_qtty_3.val = 0;
                }
                if (jsonObj['BestBidPrice2'] !== undefined) {
                    bestBidPrice2 = parseFloat(jsonObj['BestBidPrice2']);
                    stock.buy_2.change = -1;
                    if (bestBidPrice2 === stock.buy_2.val)
                        stock.buy_2.change = 0;
                    else if (bestBidPrice2 > stock.buy_2.val)
                        stock.buy_2.change = 1;
                    stock.buy_2.val = bestBidPrice2;
                    if (stock.buy_2.change !== 0) {
                        stock.buy_2.time = stock.buy_2.time === 1 ? 2 : 1;
                        // if (stock.underlying === 'VGB5')
                        stock.buy_2.yield = stockHelper.bondYield(stock.underlying, stock.buy_2.val);
                    }
                } else {
                    stock.buy_2.change = 0;
                    stock.buy_2.val = 0;
                }
                if (jsonObj['BestBidQtty2'] !== undefined) {
                    bestBidQtty2 = parseInt(jsonObj['BestBidQtty2']);
                    stock.buy_qtty_2.change = -1;
                    if (bestBidQtty2 === stock.buy_qtty_2.val)
                        stock.buy_qtty_2.change = 0;
                    else if (bestBidQtty2 > stock.buy_qtty_2.val)
                        stock.buy_qtty_2.change = 1;
                    stock.buy_qtty_2.val = bestBidQtty2;
                    if (stock.buy_qtty_2.change !== 0)
                        stock.buy_qtty_2.time = stock.buy_qtty_2.time === 1 ? 2 : 1;
                } else {
                    stock.buy_qtty_2.change = 0;
                    stock.buy_qtty_2.val = 0;
                }
                if (jsonObj['BestBidPrice1'] === undefined) {
                    stock.buy_1.val = 0;
                    if (stock.trading_session === 'CALL_AUCTION_OPENING' || stock.trading_session === 'CALL_AUCTION_CLOSING') {
                        stock.buy_1.val = 0;
                        stock.buy_1.yield = ''
                    }
                } else {
                    bestBidPrice1 = parseFloat(jsonObj['BestBidPrice1']);
                    stock.buy_1.change = -1;
                    if (bestBidPrice1 === stock.buy_1.val)
                        stock.buy_1.change = 0;
                    else if (bestBidPrice1 > stock.buy_1.val)
                        stock.buy_1.change = 1;
                    stock.buy_1.val = bestBidPrice1;
                    if (stock.buy_1.change !== 0) {
                        stock.buy_1.time = stock.buy_1.time === 1 ? 2 : 1;
                        // if (stock.underlying === 'VGB5')
                        stock.buy_1.yield = stockHelper.bondYield(stock.underlying, stock.buy_1.val);
                    }
                }
                if (jsonObj['BestBidQtty1'] !== undefined) {
                    bestBidQtty1 = parseInt(jsonObj['BestBidQtty1']);
                    stock.buy_qtty_1.change = -1;
                    if (bestBidQtty1 === stock.buy_qtty_1.val)
                        stock.buy_qtty_1.change = 0;
                    else if (bestBidQtty1 > stock.buy_qtty_1.val)
                        stock.buy_qtty_1.change = 1;
                    stock.buy_qtty_1.val = bestBidQtty1;
                    if (stock.buy_qtty_1.change !== 0)
                        stock.buy_qtty_1.time = stock.buy_qtty_1.time === 1 ? 2 : 1;
                } else {
                    stock.buy_qtty_1.change = 0;
                    stock.buy_qtty_1.val = 0;
                }
                if (jsonObj['BestOfferPrice4'] !== undefined) {
                    bestOfferPrice4 = parseFloat(jsonObj['BestOfferPrice4']);
                    stock.sell_4.change = -1;
                    if (bestOfferPrice4 === stock.sell_4.val)
                        stock.sell_4.change = 0;
                    else if (bestOfferPrice4 > stock.sell_4.val)
                        stock.sell_4.change = 1;
                    stock.sell_4.val = bestOfferPrice4;
                    if (stock.sell_4.change !== 0)
                        stock.sell_4.time = stock.sell_4.time === 1 ? 2 : 1;
                } else {
                    stock.sell_4.change = 0;
                    stock.sell_4.val = 0;
                }
                if (jsonObj['BestOfferQtty4'] !== undefined) {
                    bestOfferQtty4 = parseInt(jsonObj['BestOfferQtty4']);
                    stock.sell_qtty_4.change = -1;
                    if (bestOfferQtty4 === stock.sell_qtty_4.val)
                        stock.sell_qtty_4.change = 0;
                    else if (bestOfferQtty4 > stock.sell_qtty_4.val)
                        stock.sell_qtty_4.change = 1;
                    stock.sell_qtty_4.val = bestOfferQtty4;
                    if (stock.sell_qtty_4.change !== 0)
                        stock.sell_qtty_4.time = stock.sell_qtty_4.time === 1 ? 2 : 1;
                } else {
                    stock.sell_qtty_4.change = 0;
                    stock.sell_qtty_4.val = 0;
                }
                if (jsonObj['BestOfferPrice5'] !== undefined) {
                    bestOfferPrice5 = parseFloat(jsonObj['BestOfferPrice5']);
                    stock.sell_5.change = -1;
                    if (bestOfferPrice5 === stock.sell_5.val)
                        stock.sell_5.change = 0;
                    else if (bestOfferPrice5 > stock.sell_5.val)
                        stock.sell_5.change = 1;
                    stock.sell_5.val = bestOfferPrice5;
                    if (stock.sell_5.change !== 0)
                        stock.sell_5.time = stock.sell_5.time === 1 ? 2 : 1;
                } else {
                    stock.sell_5.change = 0;
                    stock.sell_5.val = 0;
                }
                if (jsonObj['BestOfferQtty5'] !== undefined) {
                    bestOfferQtty5 = parseInt(jsonObj['BestOfferQtty5']);
                    stock.sell_qtty_5.change = -1;
                    if (bestOfferQtty5 === stock.sell_qtty_5.val)
                        stock.sell_qtty_5.change = 0;
                    else if (bestOfferQtty5 > stock.sell_qtty_5.val)
                        stock.sell_qtty_5.change = 1;
                    stock.sell_qtty_5.val = bestOfferQtty5;
                    if (stock.sell_qtty_5.change !== 0)
                        stock.sell_qtty_5.time = stock.sell_qtty_5.time === 1 ? 2 : 1;
                } else {
                    stock.sell_qtty_5.change = 0;
                    stock.sell_qtty_5.val = 0;
                }
                if (jsonObj['BestOfferPrice6'] !== undefined) {
                    bestOfferPrice6 = parseFloat(jsonObj['BestOfferPrice6']);
                    stock.sell_6.change = -1;
                    if (bestOfferPrice6 === stock.sell_6.val)
                        stock.sell_6.change = 0;
                    else if (bestOfferPrice6 > stock.sell_6.val)
                        stock.sell_6.change = 1;
                    stock.sell_6.val = bestOfferPrice6;
                    if (stock.sell_6.change !== 0)
                        stock.sell_6.time = stock.sell_6.time === 1 ? 2 : 1;
                } else {
                    stock.sell_6.change = 0;
                    stock.sell_6.val = 0;
                }
                if (jsonObj['BestOfferQtty6'] !== undefined) {
                    bestOfferQtty6 = parseInt(jsonObj['BestOfferQtty6']);
                    stock.sell_qtty_6.change = -1;
                    if (bestOfferQtty6 === stock.sell_qtty_6.val)
                        stock.sell_qtty_6.change = 0;
                    else if (bestOfferQtty6 > stock.sell_qtty_6.val)
                        stock.sell_qtty_6.change = 1;
                    stock.sell_qtty_6.val = bestOfferQtty6;
                    if (stock.sell_qtty_6.change !== 0)
                        stock.sell_qtty_6.time = stock.sell_qtty_6.time === 1 ? 2 : 1;
                } else {
                    stock.sell_qtty_6.change = 0;
                    stock.sell_qtty_6.val = 0;
                }
                if (jsonObj['BestOfferPrice7'] !== undefined) {
                    bestOfferPrice7 = parseFloat(jsonObj['BestOfferPrice7']);
                    stock.sell_7.change = -1;
                    if (bestOfferPrice7 === stock.sell_7.val)
                        stock.sell_7.change = 0;
                    else if (bestOfferPrice7 > stock.sell_7.val)
                        stock.sell_7.change = 1;
                    stock.sell_7.val = bestOfferPrice7;
                    if (stock.sell_7.change !== 0)
                        stock.sell_7.time = stock.sell_7.time === 1 ? 2 : 1;
                } else {
                    stock.sell_7.change = 0;
                    stock.sell_7.val = 0;
                }
                if (jsonObj['BestOfferQtty7'] !== undefined) {
                    bestOfferQtty7 = parseInt(jsonObj['BestOfferQtty7']);
                    stock.sell_qtty_7.change = -1;
                    if (bestOfferQtty7 === stock.sell_qtty_7.val)
                        stock.sell_qtty_7.change = 0;
                    else if (bestOfferQtty7 > stock.sell_qtty_7.val)
                        stock.sell_qtty_7.change = 1;
                    stock.sell_qtty_7.val = bestOfferQtty7;
                    if (stock.sell_qtty_7.change !== 0)
                        stock.sell_qtty_7.time = stock.sell_qtty_7.time === 1 ? 2 : 1;
                } else {
                    stock.sell_qtty_7.change = 0;
                    stock.sell_qtty_7.val = 0;
                }
                if (jsonObj['BestOfferPrice8'] !== undefined) {
                    bestOfferPrice8 = parseFloat(jsonObj['BestOfferPrice8']);
                    stock.sell_8.change = -1;
                    if (bestOfferPrice8 === stock.sell_8.val)
                        stock.sell_8.change = 0;
                    else if (bestOfferPrice8 > stock.sell_8.val)
                        stock.sell_8.change = 1;
                    stock.sell_8.val = bestOfferPrice8;
                    if (stock.sell_8.change !== 0)
                        stock.sell_8.time = stock.sell_8.time === 1 ? 2 : 1;
                } else {
                    stock.sell_8.change = 0;
                    stock.sell_8.val = 0;
                }
                if (jsonObj['BestOfferQtty8'] !== undefined) {
                    bestOfferQtty8 = parseInt(jsonObj['BestOfferQtty8']);
                    stock.sell_qtty_8.change = -1;
                    if (bestOfferQtty8 === stock.sell_qtty_8.val)
                        stock.sell_qtty_8.change = 0;
                    else if (bestOfferQtty8 > stock.sell_qtty_8.val)
                        stock.sell_qtty_8.change = 1;
                    stock.sell_qtty_8.val = bestOfferQtty8;
                    if (stock.sell_qtty_8.change !== 0)
                        stock.sell_qtty_8.time = stock.sell_qtty_8.time === 1 ? 2 : 1;
                } else {
                    stock.sell_qtty_8.change = 0;
                    stock.sell_qtty_8.val = 0;
                }
                if (jsonObj['BestOfferPrice9'] !== undefined) {
                    bestOfferPrice9 = parseFloat(jsonObj['BestOfferPrice9']);
                    stock.sell_9.change = -1;
                    if (bestOfferPrice9 === stock.sell_9.val)
                        stock.sell_9.change = 0;
                    else if (bestOfferPrice9 > stock.sell_9.val)
                        stock.sell_9.change = 1;
                    stock.sell_9.val = bestOfferPrice9;
                    if (stock.sell_9.change !== 0)
                        stock.sell_9.time = stock.sell_9.time === 1 ? 2 : 1;
                } else {
                    stock.sell_9.change = 0;
                    stock.sell_9.val = 0;
                }
                if (jsonObj['BestOfferQtty9'] !== undefined) {
                    bestOfferQtty9 = parseInt(jsonObj['BestOfferQtty9']);
                    stock.sell_qtty_9.change = -1;
                    if (bestOfferQtty9 === stock.sell_qtty_9.val)
                        stock.sell_qtty_9.change = 0;
                    else if (bestOfferQtty9 > stock.sell_qtty_9.val)
                        stock.sell_qtty_9.change = 1;
                    stock.sell_qtty_9.val = bestOfferQtty9;
                    if (stock.sell_qtty_9.change !== 0)
                        stock.sell_qtty_9.time = stock.sell_qtty_9.time === 1 ? 2 : 1;
                } else {
                    stock.sell_qtty_9.change = 0;
                    stock.sell_qtty_9.val = 0;
                }
                if (jsonObj['BestOfferPrice10'] !== undefined) {
                    bestOfferPrice10 = parseFloat(jsonObj['BestOfferPrice10']);
                    stock.sell_10.change = -1;
                    if (bestOfferPrice10 === stock.sell_10.val)
                        stock.sell_10.change = 0;
                    else if (bestOfferPrice10 > stock.sell_10.val)
                        stock.sell_10.change = 1;
                    stock.sell_10.val = bestOfferPrice10;
                    if (stock.sell_10.change !== 0)
                        stock.sell_10.time = stock.sell_10.time === 1 ? 2 : 1;
                } else {
                    stock.sell_10.change = 0;
                    stock.sell_10.val = 0;
                }
                if (jsonObj['BestOfferQtty10'] !== undefined) {
                    bestOfferQtty10 = parseInt(jsonObj['BestOfferQtty10']);
                    stock.sell_qtty_10.change = -1;
                    if (bestOfferQtty10 === stock.sell_qtty_10.val)
                        stock.sell_qtty_10.change = 0;
                    else if (bestOfferQtty10 > stock.sell_qtty_10.val)
                        stock.sell_qtty_10.change = 1;
                    stock.sell_qtty_10.val = bestOfferQtty10;
                    if (stock.sell_qtty_10.change !== 0)
                        stock.sell_qtty_10.time = stock.sell_qtty_10.time === 1 ? 2 : 1;
                } else {
                    stock.sell_qtty_10.change = 0;
                    stock.sell_qtty_10.val = 0;
                }
                if (jsonObj['BestOfferPrice3'] !== undefined) {
                    bestOfferPrice3 = parseFloat(jsonObj['BestOfferPrice3']);
                    stock.sell_3.change = -1;
                    if (bestOfferPrice3 === stock.sell_3.val)
                        stock.sell_3.change = 0;
                    else if (bestOfferPrice3 > stock.sell_3.val)
                        stock.sell_3.change = 1;
                    stock.sell_3.val = bestOfferPrice3;
                    if (stock.sell_3.change !== 0) {
                        stock.sell_3.time = stock.sell_3.time === 1 ? 2 : 1;
                        // if (stock.underlying === 'VGB5')
                        stock.sell_3.yield = stockHelper.bondYield(stock.underlying, stock.sell_3.val);
                    }
                } else {
                    stock.sell_3.change = 0;
                    stock.sell_3.val = 0;
                }
                if (jsonObj['BestOfferQtty3'] !== undefined) {
                    bestOfferQtty3 = parseInt(jsonObj['BestOfferQtty3']);
                    stock.sell_qtty_3.change = -1;
                    if (bestOfferQtty3 === stock.sell_qtty_3.val)
                        stock.sell_qtty_3.change = 0;
                    else if (bestOfferQtty3 > stock.sell_qtty_3.val)
                        stock.sell_qtty_3.change = 1;
                    stock.sell_qtty_3.val = bestOfferQtty3;
                    if (stock.sell_qtty_3.change !== 0)
                        stock.sell_qtty_3.time = stock.sell_qtty_3.time === 1 ? 2 : 1;
                } else {
                    stock.sell_qtty_3.change = 0;
                    stock.sell_qtty_3.val = 0;
                }
                if (jsonObj['BestOfferPrice2'] !== undefined) {
                    bestOfferPrice2 = parseFloat(jsonObj['BestOfferPrice2']);
                    stock.sell_2.change = -1;
                    if (bestOfferPrice2 === stock.sell_2.val)
                        stock.sell_2.change = 0;
                    else if (bestOfferPrice2 > stock.sell_2.val)
                        stock.sell_2.change = 1;
                    stock.sell_2.val = bestOfferPrice2;
                    if (stock.sell_2.change !== 0) {
                        stock.sell_2.time = stock.sell_2.time === 1 ? 2 : 1;
                        // if (stock.underlying === 'VGB5')
                        stock.sell_2.yield = stockHelper.bondYield(stock.underlying, stock.sell_2.val);
                    }
                } else {
                    stock.sell_2.change = 0;
                    stock.sell_2.val = 0;
                }
                if (jsonObj['BestOfferQtty2'] !== undefined) {
                    bestOfferQtty2 = parseInt(jsonObj['BestOfferQtty2']);
                    stock.sell_qtty_2.change = -1;
                    if (bestOfferQtty2 === stock.sell_qtty_2.val)
                        stock.sell_qtty_2.change = 0;
                    else if (bestOfferQtty2 > stock.sell_qtty_2.val)
                        stock.sell_qtty_2.change = 1;
                    stock.sell_qtty_2.val = bestOfferQtty2;
                    if (stock.sell_qtty_2.change !== 0)
                        stock.sell_qtty_2.time = stock.sell_qtty_2.time === 1 ? 2 : 1;
                } else {
                    stock.sell_qtty_2.change = 0;
                    stock.sell_qtty_2.val = 0;
                }
                if (jsonObj['BestOfferPrice1'] === undefined) {
                    stock.sell_1.val = 0;
                    if (stock.trading_session === 'CALL_AUCTION_OPENING' || stock.trading_session === 'CALL_AUCTION_CLOSING') {
                        stock.sell_1.val = 0;
                        stock.sell_1.yield = '';
                    }
                } else {
                    bestOfferPrice1 = parseFloat(jsonObj['BestOfferPrice1']);
                    stock.sell_1.change = -1;
                    if (bestOfferPrice1 === stock.sell_1.val)
                        stock.sell_1.change = 0;
                    else if (bestOfferPrice1 > stock.sell_1.val)
                        stock.sell_1.change = 1;
                    stock.sell_1.val = bestOfferPrice1;
                    if (stock.sell_1.change !== 0) {
                        stock.sell_1.time = stock.sell_1.time === 1 ? 2 : 1;
                        // if (stock.underlying === 'VGB5')
                        stock.sell_1.yield = stockHelper.bondYield(stock.underlying, stock.sell_1.val);
                    }
                }
                if (jsonObj['BestOfferQtty1'] !== undefined) {
                    bestOfferQtty1 = parseInt(jsonObj['BestOfferQtty1']);
                    stock.sell_qtty_1.change = -1;
                    if (bestOfferQtty1 === stock.sell_qtty_1.val)
                        stock.sell_qtty_1.change = 0;
                    else if (bestOfferQtty1 > stock.sell_qtty_1.val)
                        stock.sell_qtty_1.change = 1;
                    stock.sell_qtty_1.val = bestOfferQtty1;
                    if (stock.sell_qtty_1.change !== 0)
                        stock.sell_qtty_1.time = stock.sell_qtty_1.time === 1 ? 2 : 1;
                } else {
                    stock.sell_qtty_1.change = 0;
                    stock.sell_qtty_1.val = 0;
                }
            }
            lastDataFuture[stock.symbol] = stock;
        }

        saveLastPriceStock();
        var data = Object.values(lastDataFuture);
        // io.to('future').emit('onChangeStock', data);
        io.emit('onChangeStockForOnline', jo.pack(data));
        if (Object.values(last10MatchHist).length) {
            io.emit('onChangeMatchHist', jo.pack(last10MatchHist));
        }
    }
}

module.exports = {
    loadHistoryData,
    loadMatchHistData,
    initDataForOnline,
    futureOnChangeHandler
}