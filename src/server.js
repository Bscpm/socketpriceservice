const express = require('express');
const router = express.Router();
const cors = require('cors');
const bodyParser = require('body-parser');
const https = require('https');
const http = require('http');
const socketServer = require('socket.io');
let fs = require('fs');
const logger = require('./config/logger');
const index = require('./feed/IndexFeed');
const future = require('./feed/FutureServerHandle');
const basis = require('./feed/BasisServerHandle');
const feed = require('./feed/Feed');
const history = require('./feed/HistoryProvider');
const stringUtils = require('./utils/String');
var redis = require('./database/redis');

const dotenv = require('dotenv');
const { setMakerSimulator, clearTPData, hoseOnChangeHandler } = require('./feed/BasisServerHandle');
dotenv.config();
const errorLogger = logger.errorLogger;
const commonLogger = logger.commonLogger;

const app = express();
var countConnect = 0;

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
const corsOptions = {
  origin: process.env.APP_ORIGIN,
  credentials: true
}
app.use(cors())

router.get('/trading-view/change-symbol', async (req, res) => {
  try {
    var symbol = req.query.symbol;
    var symbolType = (/HOSE|HNX|UPCOM/).test(req.query.symbolType) ? 'BASIS' : req.query.symbolType;
    var clientId = req.query.clientId;
    if (symbol === undefined || symbol === '') throw new Error('data not valid');
    io.to(`${clientId}`).emit('tvSymbolChange', { symbol: symbol, symbolType: symbolType });
    res.status(200).send('Change symbol successfully');
  } catch (error) {
    errorLogger.info(JSON.stringify(error));
    res.status(400).send('Error occurs: ' + error.message);
  }
});

router.get('/trading-view/order', async (req, res) => {
  try {
    var side = req.query.side;
    var clientId = req.query.clientId;
    if (side === undefined || side === '' || clientId === undefined || clientId === '') throw new Error('data not valid');
    io.to(`${clientId}`).emit('tvOrderRequest', { side: side });
    res.status(200).send('request successfully');
  } catch (error) {
    errorLogger.info(JSON.stringify(error));
    res.status(400).send('Error occurs: ' + error.message);
  }
});

router.get('/getStockData', async (req, res) => {
  try {
    basis.pushMakerStockChangeEvent(null);
    res.send({ s: 'ok' });
  } catch (error) {
    errorLogger.info(JSON.stringify(error));
    res.status(400).send('Error occurs: ' + error.message);
  }
});

app.use(router)

var serve = http.createServer(app);
// var serve = https.createServer({
//     key: fs.readFileSync('/usr/local/ssl/certificate/private.key'),
//     cert: fs.readFileSync('/usr/local/ssl/certificate/iTrade.bsc.com.vn.crt'),
//     ca: fs.readFileSync('/usr/local/ssl/certificate/ca-bundle.crt')
// }, app);
var io = socketServer(serve);
// io.origins(process.env.WS_ORIGIN.split(';'));
serve.listen(process.env.SOCKET_PORT || 3000, process.env.SOCKET_DOMAIN, () => {
  console.log("Express Server with Socket Running on " + process.env.SOCKET_DOMAIN + ":" + process.env.SOCKET_PORT || 3000);
  commonLogger.info("Express Server with Socket Running on " + process.env.SOCKET_DOMAIN + ":" + process.env.SOCKET_PORT || 3000);
  // Load lich su gia
  history.initStockData();
  history.initFutureData();
  // stream index charts
  index.streamIndexData(io);
  mqReconnect();
});

io.on('connection', function (socket) {
  
  if (!stringUtils.validateOriginString(socket.handshake.headers.origin)
    && !stringUtils.validateOriginString(socket.handshake.address)) {
    socket.disconnect();
    return;
  }
  countConnect++;
  socket.on('disconnect', function () {
    countConnect--;
  });

  // Sync time
  socket.emit('timestamp', new Date().getTime());
  
  index.initIndexData(io, socket.id);
  basis.initDataForOnline(io, socket.id);
  future.initDataForOnline(io, socket.id);

});

function mqReconnect() {
  setInterval(() => {
    var date = new Date(),
      now = date.getTime(),
      beginMQReconnectTime = date.setHours(7, 30, 0),
      endMQReconnectTime = date.setHours(8, 30, 0);
    if (now > beginMQReconnectTime && now < endMQReconnectTime) {
      feed.mqReconnect(io, hoseOnChangeHandler, basis.hnxOnChangeHandler, future.futureOnChangeHandler);
    }
  }, 30000);
}

feed.mqConnection(io, hoseOnChangeHandler, basis.hnxOnChangeHandler, future.futureOnChangeHandler);

setInterval(() => {
  commonLogger.info("Active connection --> "+ (process.env.SOCKET_PORT || 3000) +": " + countConnect);
}, 300000);
